#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include "../include/cigma.h"
#include "../include/consoletools.h"
#include "../include/Cwing.h"

void initialize ();
void update ();
void render3D ();
void render2D ();
void windowResize();
void mouseMoved ();
void windowClose();

int main(int argc,char** argv){
	int largeurFenetre 	   = 600;
	int hauteurFenetre 	   = 600;
	int xfenetre 		   = 800;
	int yfenetre 		   = 600;
	char *nomFenetre 	   = "Cigma App";
	int limFPS 			   = 1000;
	void (*callbacks[7])() = {initialize,update,render2D,render3D,mouseMoved,windowResize,windowClose};

	setDebugMode(false); 		//Affichage des messages de debug ?
	setIgnoreError(false);		//Ignorer les erreurs ?

	startGFX(largeurFenetre,hauteurFenetre,xfenetre,yfenetre,nomFenetre,limFPS,callbacks,&argc,argv);
	return 0;
}

///////////////////////////////////////////////////////// FONCTIONS GFX CIGMA //////////////////////////////////////////////////////////////////////////


//fonction d'initialisation Cigma
void initialize (){

}

//fonction de rafraichissement Cigma
void update (){

}

void render3D (){
    
}

//fonction d'affichage Cigma
void render2D (){

}

//fonction de mouvement souris Cigma
void mouseMoved(){
	
}

//fonction de changement de taille de la fenetre Cigma
void windowResize(){

}

//fonction de fermeture de l'application Cigma
void windowClose(){
	
}