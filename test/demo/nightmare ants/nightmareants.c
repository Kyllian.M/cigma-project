#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include "../include/cigma.h"
#include "../include/consoletools.h"
#include "../include/Cwing.h"

void initialize ();
void update ();
void render3D ();
void render2D ();
void windowResize();
void mouseMoved ();
void windowClose();

int main(int argc,char** argv){
	int largeurFenetre 	   = 1300;
	int hauteurFenetre 	   = 500;
	int xfenetre 		   = 800;
	int yfenetre 		   = 600;
	char *nomFenetre 	   = "Nightmare Cosmic Ants";
	int limFPS 			   = 1000;
	void (*callbacks[7])() = {initialize,update,render2D,render3D,mouseMoved,windowResize,windowClose};

	setDebugMode(false); 		//Affichage des messages de debug ?
	setIgnoreError(false);		//Ignorer les erreurs ?

	startGFX(largeurFenetre,hauteurFenetre,xfenetre,yfenetre,nomFenetre,limFPS,callbacks,&argc,argv);
	return 0;
}


///////////////////////////////////////////////////////// FONCTIONS GFX CIGMA //////////////////////////////////////////////////////////////////////////

FontText* ft;
Textfield txtf;
Image* mage;
Image* space;
Video vid;
Sound spacemusic;
double t0;

//fonction d'initialisation Cigma
void initialize (){
	t0 = getTimeVal();
	ft = new_FontText("resources/fonts/PAPYRUS.TTF",100);
	txtf = new_Textfield(300,24,ft);
	mage = new_Image("resources/images/earth.jpg",true);
	space = new_Image("resources/images/space.jpg",true);
	vid = new_Video("resources/videos/antstexture.webm",600,300);
	spacemusic = new_Sound("resources/sounds/spacemusic.ogg");
	vid.play(&vid);
	spacemusic.play(&spacemusic);
	spacemusic.setPosition(&spacemusic,18);
}

//fonction de rafraichissement Cigma
void update (){
	txtf.update(&txtf);
	vid.update(&vid);
	if(libvlc_media_player_get_time(vid.mediaPlayer) >= libvlc_media_player_get_length(vid.mediaPlayer)*0.95){
		libvlc_media_player_set_position(vid.mediaPlayer,0);
	}
}

void render3D (){
    glEnable(GL_TEXTURE_2D);
	setColor4i(255,255,255, 255);
	//glTranslatef(0, sin(getTimeVal())*100, 0);
	glRotatef( ((int)((getTimeVal()-t0)*30))%360, 0,1,0);
	//glRotatef(90,0,1,0);
	//glDisable(GL_LIGHT0 | GL_LIGHTING);

    GLUquadric* monObj = gluNewQuadric();
	gluQuadricTexture(monObj, GLU_TRUE);
	gluQuadricDrawStyle(monObj, GLU_FILL);

	glBindTexture(GL_TEXTURE_2D,space->donneesRGB);
	gluSphere(monObj, 2000, 200, 200);
	glBindTexture(GL_TEXTURE_2D,mage->donneesRGB);
	glRotatef( 90 + 180, 1,0,0);
	gluSphere(monObj, 200, 20, 20);
	glTranslatef(0,380, 0);
	glRotatef( ((int)((getTimeVal()-t0)*30))%360*3, 1,1,0);
	glBindTexture(GL_TEXTURE_2D,vid.textureId);
	//gluSphere(monObj, 60, 20, 20);
	glutSolidTeapot(60);
	glTranslatef(30000,900,677);
	vid.show(&vid,0,0);

	gluQuadricTexture(monObj, GLU_FALSE);
	gluDeleteQuadric(monObj);
}

//fonction d'affichage Cigma
void render2D (){
	strokeRGB(COLOR_RED);
	if((int)(getTimeVal()-t0)%2 == 0)
		stroke(NO_STROKE);
    text(ft,200,200,90,"NIGHTMARE");
}

//fonction de mouvement souris Cigma
void mouseMoved(){
	
}

//fonction de changement de taille de la fenetre Cigma
void windowResize(){

}

//fonction de fermeture de l'application Cigma
void windowClose(){
	
}