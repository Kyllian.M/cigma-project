#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include "../include/cigma.h"
#include "../include/consoletools.h"
#include "../include/Cwing.h"

void initialize ();
void update ();
void render3D ();
void render2D ();
void windowResize();
void mouseMoved ();
void windowClose();

int main(int argc,char** argv){
	int largeurFenetre 	   = 600;
	int hauteurFenetre 	   = 600;
	int xfenetre 		   = 3000;
	int yfenetre 		   = 600;
	char *nomFenetre 	   = "Cigma App";
	int limFPS 			   = 1000;
	void (*callbacks[7])() = {initialize,update,render2D,render3D,mouseMoved,windowResize,windowClose};

	setDebugMode(false); 		//Affichage des messages de debug ?
	setIgnoreError(false);		//Ignorer les erreurs ?

	startGFX(largeurFenetre,hauteurFenetre,xfenetre,yfenetre,nomFenetre,limFPS,callbacks,&argc,argv);
	return 0;
}

int size = 100;



///////////////////////////////////////////////////////// FONCTIONS GFX CIGMA //////////////////////////////////////////////////////////////////////////

GLuint model;
CameraFPS cam3D;
FontText* font;
Image* img;


//fonction d'initialisation Cigma
void initialize (){
    font = new_FontText("resources/fonts/PAPYRUS.ttf");
    cam3D = new_CameraFPS();
	model = loadObj("resources/models/seahorse.obj");
    img = new_Image("resources/images/cigma.png",true);
}

#define WALK_SPEED 1

//fonction de rafraichissement Cigma
void update (){
    cam3D.update(&cam3D);
}

#define MAXNB 100

void render3D (){
    cam3D.apply(&cam3D);
	setColorRGB(COLOR_BLUE_VIOLET);
	glutSolidTeapot(1);
    setColorRGB(COLOR_GREEN);
    rectangle3D(0,-5.75,0,100,10,100);
    glTranslated(-4,3,0);
    drawModel(model,3);
    cam3D.reset(&cam3D);
}

//fonction d'affichage Cigma
void render2D (){
    imageCustom(img,0,0,200,100);
	displayFPS(3);
}

//fonction de mouvement souris Cigma
void mouseMoved(){
	
}

//fonction de changement de taille de la fenetre Cigma
void windowResize(){

}

//fonction de fermeture de l'application Cigma
void windowClose(){
	
}