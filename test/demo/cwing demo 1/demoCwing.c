#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#include "../lib/cigma.h"
#include "../lib/consoletools.h"
#include "../lib/Cwing.h"
#include "projet.h"
#include "../lib/KyllianToolsLib.h"

int main(int argc,char** argv){
	int largeurFenetre 	   = 500;
	int hauteurFenetre 	   = 500;
	int xfenetre 		   = 800;
	int yfenetre 		   = 600;
	char *nomFenetre 	   = "Cigma App";
	int limFPS 			   = 1000;
	void (*callbacks[6])() = {initialize,update,render,mouseMoved,windowResize,windowClose};

	setDebugMode(false); 		//Affichage des messages de debug ?
	setIgnoreError(false);		//Ignorer les erreurs ?

	startGFX(largeurFenetre,hauteurFenetre,xfenetre,yfenetre,nomFenetre,limFPS,callbacks,&argc,argv);
	return 0;
}


///////////////////////////////////////////////////////// FONCTIONS GFX CIGMA //////////////////////////////////////////////////////////////////////////

char* styles = "../ressources/style.css";
FontText *myFont;
FontText *Consolas;
int g = 0;
Sound music;
Image* cigmaimg;
Button btn;
Textfield txtf;
Slider slide;
Counter count;
DropMenu dm;
Overlay overlay;
int idWindow;
int subwindow;

void actionoftest(){
	printf("dab\n");
}

void actionofslide(){
	g = slide.value;
}

//fonction d'initialisation Cigma
void initialize (){
    myFont = new_FontText("../ressources/fonts/police.ttf", 100);
	Consolas = new_FontText("../ressources/fonts/consolas.ttf", 30);
	//music = new_Sound("valou.ogg");
	cigmaimg = new_Image("../ressources/cigma small.png",true);

	overlay = new_Overlay("../ressources/cigma32.png");
	overlay.activate(&overlay);
	
	btn = new_Button_CSS("Antet Depaj",styles,"buttons");
	btn.addEventListener(&btn,"click",actionoftest);

	txtf = new_Textfield_CSS(styles,"textfields");
	txtf.roundBorders(&txtf,0.7);

	slide = new_Slider_CSS(0,360,0,styles,"slide");
	slide.setCallback(&slide,actionofslide);

	count = new_Counter_CSS(0,10,20,styles,"counter");

	dm = new_DropMenuCSS(styles,"dropy");
	dm.add(&dm,"Yes");
	dm.add(&dm,"No");
	dm.add(&dm,"Maybe");
}

//fonction de rafraichissement Cigma
void update (){
	overlay.update(&overlay);
	txtf.update(&txtf);

    ifkeyup('i'){
        freeFontText(&myFont);
    }
	ifkeyup('v'){
		//music.play(&music);
	}

	btn.update(&btn);
	cigmaimg->color = new_RGBA(255,100,g%255,g%255);
	slide.update(&slide);
	count.update(&count);
	dm.update(&dm);

	if(!overlay.movewindow)
		updateGraphics();
}

//fonction d'affichage Cigma
void render (){
	selfRotate(g);
	clear(30,30,30);
	fillRGBA(COLOR_RGBA_MAROON);
	strokeRGBA(COLOR_RGBA_BLANCHE_DALMOND);
	strokeWidth(10);

	imageCustom(cigmaimg,0,0,LF,HF);

	circle(60,60,50,50);
	rectangle(130,10,100,50);
	square(250,10,90);
	triangle(370,10,440,20,390,90);
	roundedRectangle(10,120,50,100,0.8,30);
	ellipse(130 + 50,120 + 50,50,30);
	quads(250,120,350,120,325,220,275,220);
	line(10,240,110,340);
	dot(150,260);

    text(myFont,40,360,30,"MDR LULZ OwO");

	btn.show(&btn,20,20);
	txtf.show(&txtf,100,200);
	slide.show(&slide,20,300);
	count.show(&count,LF-100,5);
	dm.show(&dm,100,70);

	displayFPS(3);
	overlay.show(&overlay);
}

//fonction de mouvement souris Cigma
void mouseMoved(){
	
}

//fonction de changement de taille de la fenetre Cigma
void windowResize(){

}

//fonction de fermeture de l'application Cigma
void windowClose(){
	println("closy");
	//freeSound(&music);
}