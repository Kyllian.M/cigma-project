/*
APPS : 
sudo apt-get install xclip
sudo apt-get install vlc

LIBS :
sudo apt-get install libcsfml-audio2.3
sudo apt-get install libvlc-dev
sudo apt-get install libsdl1.2-dev
sudo apt-get install freeglut3-dev

JUST IN CASE :
sudo apt-get install libcsfml-dev
sudo apt-get install freeglut3

MAKEFILE : 
-lm -lSDL -lcsfml-audio -lcsfml-system -lvlc -lglut -lGLU -lGL -lX11 -g
*/

#ifndef CIGMA_LIB
#define CIGMA_LIB

#ifdef _WIN32
	#include <windows.h>
#endif
#ifdef __APPLE__
	#include <GLUT/glut.h>		// Header File For The GLut Library
#else
	#include <GL/glut.h>			// Header File For The GLut Library
	#ifdef _WIN32
		#include <GL/glext.h>
	#else
		#include <X11/Xlib.h>
		#include <GL/glx.h>
	#endif
#endif
#include <GL/glu.h>
#include <GL/gl.h>

#ifndef _TR1_STDBOOL_H
	#include <stdbool.h>
#endif
#include "../include/stb-master/stb_truetype.h"
#include <SFML/System/Time.h>
#include <SFML/Audio.h>
#include <SDL/SDL.h>
#include <vlc/vlc.h>
#include <vlc/libvlc.h>
#include <vlc/libvlc_media_player.h>


// Liste de toutes les couleurs du standard CSS 
// voir https://www.quackit.com/css/css_color_codes.cfm

//RED
#define COLOR_INDIAN_RED				new_RGB(205, 92, 92)
#define COLOR_LIGHT_CORAL				new_RGB(240,128,128)
#define COLOR_SALMON					new_RGB(250,128,114)
#define COLOR_DARK_SALMON				new_RGB(233,150,122)
#define COLOR_LIGHT_SALMON				new_RGB(255,160,122)
#define COLOR_CRIMSON					new_RGB(220, 20, 60)
#define COLOR_RED						new_RGB(255,  0,  0)
#define COLOR_FIRE_BRICK				new_RGB(178, 34, 34)
#define COLOR_DARK_RED					new_RGB(139,  0,  0)
//PINKS	
#define COLOR_PINK						new_RGB(255,192,203)
#define COLOR_LIGHT_PINK				new_RGB(255,182,193)
#define COLOR_HOT_PINK					new_RGB(255,105,180)
#define COLOR_DEEP_PINK					new_RGB(255, 20,147)
#define COLOR_MEDIUM_VIOLET_RED			new_RGB(199, 21,133)
#define COLOR_PALE_VIOLET_RED			new_RGB(219,112,147)
//ORANGES
#define COLOR_CORAL						new_RGB(255,127, 80)
#define COLOR_TOMATO					new_RGB(255, 99, 71)
#define COLOR_ORANGE_RED				new_RGB(255, 69,  0)
#define COLOR_DARK_ORANGE				new_RGB(255,140,  0)
#define COLOR_ORANGE					new_RGB(255,165,  0)
//YELLOWS
#define COLOR_GOLD						new_RGB(255,215,  0)
#define COLOR_YELLOW					new_RGB(255,255,  0)
#define COLOR_LIGHT_YELLOW				new_RGB(255,255,224)
#define COLOR_LEMON_CHIFFON				new_RGB(255,250,205)
#define COLOR_LIGHT_GOLDEN_ROD_YELLOW	new_RGB(250,250,210)
#define COLOR_PAPAYAWHIP				new_RGB(255,239,213)
#define COLOR_MOCCASIN					new_RGB(255,228,181)
#define COLOR_PEACHPUFF					new_RGB(255,218,185)
#define COLOR_PALE_GOLDEN_ROD			new_RGB(238,232,170)
#define COLOR_KHAKI						new_RGB(240,230,140)
#define COLOR_DARK_KHAKI				new_RGB(189,183,107)
//PURPLES
#define COLOR_LAVENDER					new_RGB(230,230,250)
#define COLOR_THISTLE					new_RGB(216,191,216)
#define COLOR_PLUM						new_RGB(221,160,221)
#define COLOR_VIOLET					new_RGB(238,130,238)
#define COLOR_ORCHID					new_RGB(218,112,214)
#define COLOR_FUCHSIA					new_RGB(255,  0,255)
#define COLOR_MAGENTA					new_RGB(255,  0,255)
#define COLOR_MEDIUM_ORCHID				new_RGB(186, 85,211)
#define COLOR_MEDIUM_PURPLE				new_RGB(147,112,219)
#define COLOR_BLUE_VIOLET				new_RGB(138, 43,226)
#define COLOR_DARK_VIOLET				new_RGB(148,  0,211)
#define COLOR_DARK_ORCHID				new_RGB(153, 50,204)
#define COLOR_DARK_MAGENTA				new_RGB(139,  0,139)
#define COLOR_PURPLE					new_RGB(128,  0,128)
#define COLOR_REBECCA_PURPLE			new_RGB(102, 51,153)
#define COLOR_INDIGO					new_RGB( 75,  0,130)
#define COLOR_MEDIUM_SLATE_BLUE			new_RGB(123,104,238)
#define COLOR_SLATE_BLUE				new_RGB(106, 90,205)
#define COLOR_DARK_SLATE_BLUE			new_RGB( 72, 61,139)
//GREEN
#define COLOR_GREEN_YELLOW				new_RGB(173,255, 47)
#define COLOR_CHARTREUSE				new_RGB(127,255,  0)
#define COLOR_LAWN_GREEN				new_RGB(124,252,  0)
#define COLOR_LIME						new_RGB(  0,255,  0)
#define COLOR_LIME_GREEN				new_RGB( 50,205, 50)
#define COLOR_PALE_GREEN				new_RGB(152,251,152)
#define COLOR_LIGHT_GREEN				new_RGB(144,238,144)
#define COLOR_MEDIUM_SPRING_GREEN		new_RGB(  0,250,154)
#define COLOR_SPRING_GREEN				new_RGB(  0,255,127)
#define COLOR_MEDIUM_SEA_GREEN			new_RGB( 60,179,113)
#define COLOR_SEA_GREEN					new_RGB( 46,139, 87)
#define COLOR_FOREST_GREEN				new_RGB( 34,139, 34)
#define COLOR_GREEN						new_RGB(  0,128,  0)
#define COLOR_DARK_GREEN				new_RGB(  0,100,  0)
#define COLOR_YELLOW_GREEN				new_RGB(154,205, 50)
#define COLOR_OLIVE_DRAB				new_RGB(107,142, 35)
#define COLOR_OLIVE						new_RGB(128,128,  0)
#define COLOR_DARK_OLIVE_GREEN			new_RGB( 85,107, 47)
#define COLOR_MEDIUM_AQUA_MARINE		new_RGB(102,205,170)
#define COLOR_DARK_SEA_GREEN			new_RGB(143,188,143)
#define COLOR_LIGHT_SEA_GREEN			new_RGB( 32,178,170)
#define COLOR_DARK_CYAN					new_RGB(  0,139,139)
#define COLOR_TEAL						new_RGB(  0,128,128)
//BLUES
#define COLOR_AQUA						new_RGB(  0,255,255)
#define COLOR_CYAN						new_RGB(  0,255,255)
#define COLOR_LIGHT_CYAN				new_RGB(224,255,255)
#define COLOR_PALE_TURQUOISE			new_RGB(175,238,238)
#define COLOR_AQUAMARINE				new_RGB(127,255,212)
#define COLOR_TURQUOISE					new_RGB( 64,224,208)
#define COLOR_MEDIUM_TURQUOISE			new_RGB( 72,209,204)
#define COLOR_DARK_TURQUOISE			new_RGB(  0,206,209)
#define COLOR_CADET_BLUE				new_RGB( 95,158,160)
#define COLOR_STEEL_BLUE				new_RGB( 70,130,180)
#define COLOR_LIGHT_STEEL_BLUE			new_RGB(176,196,222)
#define COLOR_POWDER_BLUE				new_RGB(176,224,230)
#define COLOR_LIGHT_BLUE				new_RGB(173,216,230)
#define COLOR_SKYB_LUE					new_RGB(135,206,235)
#define COLOR_LIGHT_SKY_BLUE			new_RGB(135,206,250)
#define COLOR_DEEP_SKY_BLUE				new_RGB(  0,191,255)
#define COLOR_DODGER_BLUE				new_RGB( 30,144,255)
#define COLOR_CORN_FLOWER_BLUE			new_RGB(100,149,237)
#define COLOR_ROYAL_BLUE				new_RGB( 65,105,225)
#define COLOR_BLUE						new_RGB(  0,  0,255)
#define COLOR_MEDIUM_BLUE				new_RGB(  0,  0,205)
#define COLOR_DARK_BLUE					new_RGB(  0,  0,139)
#define COLOR_NAVY						new_RGB(  0,  0,128)
#define COLOR_MIDNIGHT_BLUE				new_RGB( 25, 25,112)
//BROWNS
#define COLOR_CORN_SILK					new_RGB(255,248,220)
#define COLOR_BLANCHE_DALMOND			new_RGB(255,235,205)
#define COLOR_BISQUE					new_RGB(255,228,196)
#define COLOR_NAVAJO_WHITE				new_RGB(255,222,173)
#define COLOR_WHEAT						new_RGB(245,222,179)
#define COLOR_BURLY_WOOD				new_RGB(222,184,135)
#define COLOR_TAN						new_RGB(210,180,140)
#define COLOR_ROSY_BROWN				new_RGB(188,143,143)
#define COLOR_SANDY_BROWN				new_RGB(244,164, 96)
#define COLOR_GOLDEN_ROD				new_RGB(218,165, 32)
#define COLOR_DARK_GOLDEN_ROD			new_RGB(184,134, 11)
#define COLOR_PERU						new_RGB(205,133, 63)
#define COLOR_CHOCOLATE					new_RGB(210,105, 30)
#define COLOR_SADDLE_BROWN				new_RGB(139, 69, 19)
#define COLOR_SIENNA					new_RGB(160, 82, 45)
#define COLOR_BROWN						new_RGB(165, 42, 42)
#define COLOR_MAROON					new_RGB(128,  0,  0)
//WHITES	
#define COLOR_WHITE						new_RGB(255,255,255)
#define COLOR_SNOW						new_RGB(255,250,250)
#define COLOR_HONEYDEW					new_RGB(240,255,240)
#define COLOR_MINT_CREAM				new_RGB(245,255,250)
#define COLOR_AZURE						new_RGB(240,255,255)
#define COLOR_ALICE_BLUE				new_RGB(240,248,255)
#define COLOR_GHOST_WHITE				new_RGB(248,248,255)
#define COLOR_WHITE_SMOKE				new_RGB(245,245,245)
#define COLOR_SEA_SHELL					new_RGB(255,245,238)
#define COLOR_BEIGE						new_RGB(245,245,220)
#define COLOR_OLD_LACE					new_RGB(253,245,230)
#define COLOR_FLORAL_WHITE				new_RGB(255,250,240)
#define COLOR_IVORY						new_RGB(255,255,240)
#define COLOR_ANTIQUE_WHITE				new_RGB(250,235,215)
#define COLOR_LINEN						new_RGB(250,240,230)
#define COLOR_LAVENDER_BLUSH			new_RGB(255,240,245)
#define COLOR_MISTY_ROSE				new_RGB(255,228,225)
//GREYS 
#define COLOR_GAINSBORO					new_RGB(220,220,220)
#define COLOR_LIGHT_GRAY				new_RGB(211,211,211)
#define COLOR_LIGHT_GREY				new_RGB(211,211,211)
#define COLOR_SILVER					new_RGB(192,192,192)
#define COLOR_DARK_GRAY					new_RGB(169,169,169)
#define COLOR_DARK_GREY					new_RGB(169,169,169)
#define COLOR_GRAY						new_RGB(128,128,128)
#define COLOR_GREY						new_RGB(128,128,128)
#define COLOR_DIM_GRAY					new_RGB(105,105,105)
#define COLOR_DIM_GREY					new_RGB(105,105,105)
#define COLOR_LIGHT_SLATE_GRAY			new_RGB(119,136,153)
#define COLOR_LIGHT_SLATE_GREY			new_RGB(119,136,153)
#define COLOR_SLATE_GRAY				new_RGB(112,128,144)
#define COLOR_SLATE_GREY				new_RGB(112,128,144)
#define COLOR_DARK_SLATE_GRAY			new_RGB( 47, 79, 79)
#define COLOR_DARK_SLATE_GREY			new_RGB( 47, 79, 79)
#define COLOR_BLACK						new_RGB(  0,  0,  0)


//RED
#define COLOR_RGBA_INDIAN_RED				new_RGBA(205, 92, 92,255)
#define COLOR_RGBA_LIGHT_CORAL				new_RGBA(240,128,128,255)
#define COLOR_RGBA_SALMON					new_RGBA(250,128,114,255)
#define COLOR_RGBA_DARK_SALMON				new_RGBA(233,150,122,255)
#define COLOR_RGBA_LIGHT_SALMON				new_RGBA(255,160,122,255)
#define COLOR_RGBA_CRIMSON					new_RGBA(220, 20, 60,255)
#define COLOR_RGBA_RED						new_RGBA(255,  0,  0,255)
#define COLOR_RGBA_FIRE_BRICK				new_RGBA(178, 34, 34,255)
#define COLOR_RGBA_DARK_RED					new_RGBA(139,  0,  0,255)
//PINKS
#define COLOR_RGBA_PINK						new_RGBA(255,192,203,255)
#define COLOR_RGBA_LIGHT_PINK				new_RGBA(255,182,193,255)
#define COLOR_RGBA_HOT_PINK					new_RGBA(255,105,180,255)
#define COLOR_RGBA_DEEP_PINK				new_RGBA(255, 20,147,255)
#define COLOR_RGBA_MEDIUM_VIOLET_RED		new_RGBA(199, 21,133,255)
#define COLOR_RGBA_PALE_VIOLET_RED			new_RGBA(219,112,147,255)
//ORANGES
#define COLOR_RGBA_CORAL					new_RGBA(255,127, 80,255)
#define COLOR_RGBA_TOMATO					new_RGBA(255, 99, 71,255)
#define COLOR_RGBA_ORANGE_RED				new_RGBA(255, 69,  0,255)
#define COLOR_RGBA_DARK_ORANGE				new_RGBA(255,140,  0,255)
#define COLOR_RGBA_ORANGE					new_RGBA(255,165,  0,255)
//YELLOWS
#define COLOR_RGBA_GOLD						new_RGBA(255,215,  0,255)
#define COLOR_RGBA_YELLOW					new_RGBA(255,255,  0,255)
#define COLOR_RGBA_LIGHT_YELLOW				new_RGBA(255,255,224,255)
#define COLOR_RGBA_LEMON_CHIFFON			new_RGBA(255,250,205,255)
#define COLOR_RGBA_LIGHT_GOLDEN_ROD_YELLOW	new_RGBA(250,250,210,255)
#define COLOR_RGBA_PAPAYAWHIP				new_RGBA(255,239,213,255)
#define COLOR_RGBA_MOCCASIN					new_RGBA(255,228,181,255)
#define COLOR_RGBA_PEACHPUFF				new_RGBA(255,218,185,255)
#define COLOR_RGBA_PALE_GOLDEN_ROD			new_RGBA(238,232,170,255)
#define COLOR_RGBA_KHAKI					new_RGBA(240,230,140,255)
#define COLOR_RGBA_DARK_KHAKI				new_RGBA(189,183,107,255)
//PURPLES
#define COLOR_RGBA_LAVENDER					new_RGBA(230,230,250,255)
#define COLOR_RGBA_THISTLE					new_RGBA(216,191,216,255)
#define COLOR_RGBA_PLUM						new_RGBA(221,160,221,255)
#define COLOR_RGBA_VIOLET					new_RGBA(238,130,238,255)
#define COLOR_RGBA_ORCHID					new_RGBA(218,112,214,255)
#define COLOR_RGBA_FUCHSIA					new_RGBA(255,  0,255,255)
#define COLOR_RGBA_MAGENTA					new_RGBA(255,  0,255,255)
#define COLOR_RGBA_MEDIUM_ORCHID			new_RGBA(186, 85,211,255)
#define COLOR_RGBA_MEDIUM_PURPLE			new_RGBA(147,112,219,255)
#define COLOR_RGBA_BLUE_VIOLET				new_RGBA(138, 43,226,255)
#define COLOR_RGBA_DARK_VIOLET				new_RGBA(148,  0,211,255)
#define COLOR_RGBA_DARK_ORCHID				new_RGBA(153, 50,204,255)
#define COLOR_RGBA_DARK_MAGENTA				new_RGBA(139,  0,139,255)
#define COLOR_RGBA_PURPLE					new_RGBA(128,  0,128,255)
#define COLOR_RGBA_REBECCA_PURPLE			new_RGBA(102, 51,153,255)
#define COLOR_RGBA_INDIGO					new_RGBA( 75,  0,130,255)
#define COLOR_RGBA_MEDIUM_SLATE_BLUE		new_RGBA(123,104,238,255)
#define COLOR_RGBA_SLATE_BLUE				new_RGBA(106, 90,205,255)
#define COLOR_RGBA_DARK_SLATE_BLUE			new_RGBA( 72, 61,139,255)
//GREEN
#define COLOR_RGBA_GREEN_YELLOW				new_RGBA(173,255, 47,255)
#define COLOR_RGBA_CHARTREUSE				new_RGBA(127,255,  0,255)
#define COLOR_RGBA_LAWN_GREEN				new_RGBA(124,252,  0,255)
#define COLOR_RGBA_LIME						new_RGBA(  0,255,  0,255)
#define COLOR_RGBA_LIME_GREEN				new_RGBA( 50,205, 50,255)
#define COLOR_RGBA_PALE_GREEN				new_RGBA(152,251,152,255)
#define COLOR_RGBA_LIGHT_GREEN				new_RGBA(144,238,144,255)
#define COLOR_RGBA_MEDIUM_SPRING_GREEN		new_RGBA(  0,250,154,255)
#define COLOR_RGBA_SPRING_GREEN				new_RGBA(  0,255,127,255)
#define COLOR_RGBA_MEDIUM_SEA_GREEN			new_RGBA( 60,179,113,255)
#define COLOR_RGBA_SEA_GREEN				new_RGBA( 46,139, 87,255)
#define COLOR_RGBA_FOREST_GREEN				new_RGBA( 34,139, 34,255)
#define COLOR_RGBA_GREEN					new_RGBA(  0,128,  0,255)
#define COLOR_RGBA_DARK_GREEN				new_RGBA(  0,100,  0,255)
#define COLOR_RGBA_YELLOW_GREEN				new_RGBA(154,205, 50,255)
#define COLOR_RGBA_OLIVE_DRAB				new_RGBA(107,142, 35,255)
#define COLOR_RGBA_OLIVE					new_RGBA(128,128,  0,255)
#define COLOR_RGBA_DARK_OLIVE_GREEN			new_RGBA( 85,107, 47,255)
#define COLOR_RGBA_MEDIUM_AQUA_MARINE		new_RGBA(102,205,170,255)
#define COLOR_RGBA_DARK_SEA_GREEN			new_RGBA(143,188,143,255)
#define COLOR_RGBA_LIGHT_SEA_GREEN			new_RGBA( 32,178,170,255)
#define COLOR_RGBA_DARK_CYAN				new_RGBA(  0,139,139,255)
#define COLOR_RGBA_TEAL						new_RGBA(  0,128,128,255)
//BLUES
#define COLOR_RGBA_AQUA						new_RGBA(  0,255,255,255)
#define COLOR_RGBA_CYAN						new_RGBA(  0,255,255,255)
#define COLOR_RGBA_LIGHT_CYAN				new_RGBA(224,255,255,255)
#define COLOR_RGBA_PALE_TURQUOISE			new_RGBA(175,238,238,255)
#define COLOR_RGBA_AQUAMARINE				new_RGBA(127,255,212,255)
#define COLOR_RGBA_TURQUOISE				new_RGBA( 64,224,208,255)
#define COLOR_RGBA_MEDIUM_TURQUOISE			new_RGBA( 72,209,204,255)
#define COLOR_RGBA_DARK_TURQUOISE			new_RGBA(  0,206,209,255)
#define COLOR_RGBA_CADET_BLUE				new_RGBA( 95,158,160,255)
#define COLOR_RGBA_STEEL_BLUE				new_RGBA( 70,130,180,255)
#define COLOR_RGBA_LIGHT_STEEL_BLUE			new_RGBA(176,196,222,255)
#define COLOR_RGBA_POWDER_BLUE				new_RGBA(176,224,230,255)
#define COLOR_RGBA_LIGHT_BLUE				new_RGBA(173,216,230,255)
#define COLOR_RGBA_SKYB_LUE					new_RGBA(135,206,235,255)
#define COLOR_RGBA_LIGHT_SKY_BLUE			new_RGBA(135,206,250,255)
#define COLOR_RGBA_DEEP_SKY_BLUE			new_RGBA(  0,191,255,255)
#define COLOR_RGBA_DODGER_BLUE				new_RGBA( 30,144,255,255)
#define COLOR_RGBA_CORN_FLOWER_BLUE			new_RGBA(100,149,237,255)
#define COLOR_RGBA_ROYAL_BLUE				new_RGBA( 65,105,225,255)
#define COLOR_RGBA_BLUE						new_RGBA(  0,  0,255,255)
#define COLOR_RGBA_MEDIUM_BLUE				new_RGBA(  0,  0,205,255)
#define COLOR_RGBA_DARK_BLUE				new_RGBA(  0,  0,139,255)
#define COLOR_RGBA_NAVY						new_RGBA(  0,  0,128,255)
#define COLOR_RGBA_MIDNIGHT_BLUE			new_RGBA( 25, 25,112,255)
//BROWNS
#define COLOR_RGBA_CORN_SILK				new_RGBA(255,248,220,255)
#define COLOR_RGBA_BLANCHE_DALMOND			new_RGBA(255,235,205,255)
#define COLOR_RGBA_BISQUE					new_RGBA(255,228,196,255)
#define COLOR_RGBA_NAVAJO_WHITE				new_RGBA(255,222,173,255)
#define COLOR_RGBA_WHEAT					new_RGBA(245,222,179,255)
#define COLOR_RGBA_BURLY_WOOD				new_RGBA(222,184,135,255)
#define COLOR_RGBA_TAN						new_RGBA(210,180,140,255)
#define COLOR_RGBA_ROSY_BROWN				new_RGBA(188,143,143,255)
#define COLOR_RGBA_SANDY_BROWN				new_RGBA(244,164, 96,255)
#define COLOR_RGBA_GOLDEN_ROD				new_RGBA(218,165, 32,255)
#define COLOR_RGBA_DARK_GOLDEN_ROD			new_RGBA(184,134, 11,255)
#define COLOR_RGBA_PERU						new_RGBA(205,133, 63,255)
#define COLOR_RGBA_CHOCOLATE				new_RGBA(210,105, 30,255)
#define COLOR_RGBA_SADDLE_BROWN				new_RGBA(139, 69, 19,255)
#define COLOR_RGBA_SIENNA					new_RGBA(160, 82, 45,255)
#define COLOR_RGBA_BROWN					new_RGBA(165, 42, 42,255)
#define COLOR_RGBA_MAROON					new_RGBA(128,  0,  0,255)
//WHITES
#define COLOR_RGBA_WHITE					new_RGBA(255,255,255,255)
#define COLOR_RGBA_SNOW						new_RGBA(255,250,250,255)
#define COLOR_RGBA_HONEYDEW					new_RGBA(240,255,240,255)
#define COLOR_RGBA_MINT_CREAM				new_RGBA(245,255,250,255)
#define COLOR_RGBA_AZURE					new_RGBA(240,255,255,255)
#define COLOR_RGBA_ALICE_BLUE				new_RGBA(240,248,255,255)
#define COLOR_RGBA_GHOST_WHITE				new_RGBA(248,248,255,255)
#define COLOR_RGBA_WHITE_SMOKE				new_RGBA(245,245,245,255)
#define COLOR_RGBA_SEA_SHELL				new_RGBA(255,245,238,255)
#define COLOR_RGBA_BEIGE					new_RGBA(245,245,220,255)
#define COLOR_RGBA_OLD_LACE					new_RGBA(253,245,230,255)
#define COLOR_RGBA_FLORAL_WHITE				new_RGBA(255,250,240,255)
#define COLOR_RGBA_IVORY					new_RGBA(255,255,240,255)
#define COLOR_RGBA_ANTIQUE_WHITE			new_RGBA(250,235,215,255)
#define COLOR_RGBA_LINEN					new_RGBA(250,240,230,255)
#define COLOR_RGBA_LAVENDER_BLUSH			new_RGBA(255,240,245,255)
#define COLOR_RGBA_MISTY_ROSE				new_RGBA(255,228,225,255)
//GREYS
#define COLOR_RGBA_GAINSBORO				new_RGBA(220,220,220,255)
#define COLOR_RGBA_LIGHT_GRAY				new_RGBA(211,211,211,255)
#define COLOR_RGBA_LIGHT_GREY				new_RGBA(211,211,211,255)
#define COLOR_RGBA_SILVER					new_RGBA(192,192,192,255)
#define COLOR_RGBA_DARK_GRAY				new_RGBA(169,169,169,255)
#define COLOR_RGBA_DARK_GREY				new_RGBA(169,169,169,255)
#define COLOR_RGBA_GRAY						new_RGBA(128,128,128,255)
#define COLOR_RGBA_GREY						new_RGBA(128,128,128,255)
#define COLOR_RGBA_DIM_GRAY					new_RGBA(105,105,105,255)
#define COLOR_RGBA_DIM_GREY					new_RGBA(105,105,105,255)
#define COLOR_RGBA_LIGHT_SLATE_GRAY			new_RGBA(119,136,153,255)
#define COLOR_RGBA_LIGHT_SLATE_GREY			new_RGBA(119,136,153,255)
#define COLOR_RGBA_SLATE_GRAY				new_RGBA(112,128,144,255)
#define COLOR_RGBA_SLATE_GREY				new_RGBA(112,128,144,255)
#define COLOR_RGBA_DARK_SLATE_GRAY			new_RGBA( 47, 79, 79,255)
#define COLOR_RGBA_DARK_SLATE_GREY			new_RGBA( 47, 79, 79,255)
#define COLOR_RGBA_BLACK					new_RGBA(  0,  0,  0,255)

#define NO_FILL -1,-1,-1,-1
#define NO_STROKE -1,-1,-1,-1

#define KEY_F1 			 0
#define KEY_F2 			 1
#define KEY_F3 			 2
#define KEY_F4 			 3
#define KEY_F5 			 4
#define KEY_F6 			 5
#define KEY_F7 			 6
#define KEY_F8 			 7
#define KEY_F9 			 8
#define KEY_F10			 9
#define KEY_F11			10
#define KEY_F12			11
#define KEY_LEFT		12
#define KEY_UP			13
#define KEY_RIGHT		14
#define KEY_DOWN		15
#define KEY_PAGE_UP		16
#define KEY_PAGE_DOWN	17
#define KEY_HOME		18
#define KEY_END			19
#define KEY_INSERT		20

//Longueur de la fenetre Cigma
#define LF glutGet(GLUT_WINDOW_WIDTH)
//Hauteur de la fenetre Cigma
#define HF (glutGet(GLUT_WINDOW_HEIGHT)-getVerticalOffset())
//Position en X de la fenetre Cigma
#define XF glutGet(GLUT_WINDOW_X)
//Position en Y de la fenetre Cigma
#define YF glutGet(GLUT_WINDOW_Y)

/**
 * @brief indique l'etat appuyé d'une touche de clavier
 * @param key : touche du clavier
 */
#define ifkeypressed(key) if(keyboardState[key])
/**
 * @brief indique l'etat relaché d'une touche de clavier
 * @param key : touche du clavier
 */
#define ifkeyup(key) if(keyboardState[key] == 2)
/**
 * @brief indique l'etat appuyé d'une touche de clavier speciale
 * @param key : touche du clavier
 * touches possibles :
 *  -> KEY_F1 
 *  -> KEY_F2 		
 *  -> KEY_F3 		
 *  -> KEY_F4 		
 *  -> KEY_F5 		
 *  -> KEY_F6 		
 *  -> KEY_F7 		
 *  -> KEY_F8 		
 *  -> KEY_F9 		
 *  -> KEY_F10		
 *  -> KEY_F11		
 *  -> KEY_F12		
 *  -> KEY_LEFT	
 *  -> KEY_UP		
 *  -> KEY_RIGHT	
 *  -> KEY_DOWN	
 *  -> KEY_PAGE_UP	
 *  -> KEY_PAGE_DOWN
 *  -> KEY_HOME	
 *  -> KEY_END		
 *  -> KEY_INSERT	
 */
#define ifkeyspecial(key) if(specialKeyboardState[key])

/**
 * @brief indique l'etat relaché du bouton gauche de la souris
 */
#define ifleftclickup()     if(mouseState == 1 && handleSouris)
/**
 * @brief indique l'etat relaché du bouton droit de la souris
 */
#define ifrightclickup()    if(mouseState == 3 && handleSouris)
/**
 * @brief indique l'etat appuyé du bouton gauche de la souris
 */
#define ifleftclickdown()   if(mouseState == 2 && handleSouris)
/**
 * @brief indique l'etat appuyé du bouton droit de la souris
 */
#define ifrightclickdown()  if(mouseState == 4 && handleSouris)
/**
 * @brief indique l'etat haut du scroll de la souris
 */
#define ifscrollup()        if(mouseState == 5 && handleSouris)
/**
 * @brief indique l'etat bas du scroll de la souris
 */
#define ifscrolldown()      if(mouseState == 6 && handleSouris)

#define MOUSE_NOTHING 0
#define MOUSE_LEFT_UP 1
#define MOUSE_LEFT_DOWN 2
#define MOUSE_RIGHT_UP 3
#define MOUSE_RIGHT_DOWN 4
#define MOUSE_SCROLL_UP 5
#define MOUSE_SCROLL_DOWN 6

/**
 * @brief Couleur RGB sur 24 bits sans transparence
 */
typedef struct RGB{
	unsigned char r, g, b;
} RGB;

/**
 * @brief Couleur RGB sur 32 bits avec transparence
 */
typedef struct RGBA{
	unsigned char r, g, b, a;
} RGBA;

/**
 * @brief Police de texte 
 */
typedef struct FontText{
    int h;
	char* name;

	unsigned char ttf_buffer[1048576];	// = 1<<20
	unsigned char temp_bitmap[262144];	// = 512*512
	stbtt_bakedchar cdata[96]; // ASCII 32..126 is 95 glyphs
	GLuint ftex;
} FontText;

/**
 * @brief Pseudo Classe de Son
 */
typedef struct Sound Sound;
struct Sound{
	sfMusic* data;

	//secondes
	float duration;
	//en %
	float volume;
	bool loop;

	/**
	 * @brief Joue le son
	 * 
	 * @param self pointeur sur le son a traiter
	 */
	void (*play) (Sound* self);

	/**
	 * @brief arrete le son et le reinitialise
	 * 
	 * @param self pointeur sur le son a traiter
	 */
	void (*stop) (Sound* self);

	/**
	 * @brief mets le son en pause
	 * 
	 * @param self pointeur sur le son a traiter
	 */
	void (*pause) (Sound* self);

	/**
	 * @brief donne le temps écoulé depuis le commencement du son
	 * 
	 * @param self pointeur sur le son a traiter
	 */
	float (*getPosition) (Sound* self);

	/**
	 * @brief définit le temps de playback du son
	 * 
	 * @param self pointeur sur le son a traiter
	 * @param position temps en secondes
	 */
	void (*setPosition) (Sound* self,float position);

	/**
	 * @brief définit le volume de playback du son
	 * 
	 * @param self pointeur sur le son a traiter
	 * @param volume volume du son en %
	 */
	void (*setVolume) (Sound* self,float volume);

	/**
	 * @brief Définit si le son doit se jouer en boucle ou non
	 * 
	 * @param self pointeur sur le son a traiter
	 * @param loop jouer en boucle ?
	 */
	void (*setLoop) (Sound* self,bool loop);

	/**
	 * @brief retourne si oui ou non, les données du son sont NULL
	 * 
	 * @param self pointeur sur le son a traiter
	 */
	bool (*isNULL) (Sound* self);
};

/**
 * @brief contains SDL video data, used for Video class
 */
typedef struct SdlVideoData {
	SDL_Surface *sdlSurface;
	SDL_mutex *sdlMutex;
} SdlVideoData;

/**
 * @brief Video class, used to embed a video in a Cigma context
 */
typedef struct Video Video;
struct Video{
	char* path;
	int width,height;
	GLuint textureId;
	libvlc_media_player_t * mediaPlayer;	//player video
	libvlc_state_t state; 					//etat lecture
	SdlVideoData* sdlStruct;

	/**
	 * @brief updates a video
	 * @param vid video to update
	 */
	void (*update) (Video* self);

	/**
	 * @brief displays the video
	 * @param vid video to display
	 * @param x x-position to display the video
	 * @param y y-position to display the video
	 */
	void (*show) (Video* self,int x,int y);

	/**
	 * @brief starts the video
	 * @param vid video to start
	 */
	void (*play) (Video* self);

	/**
	 * @brief frees the memory allocated to a video
	 * @param vid video to free
	 */
	void (*delete) (Video* self);
};

/**
 * @brief Creates a new video from a file or an URL using libVLC
 * @param path path to a video file or an URL
 * @param width width of the video
 * @param height height of the video
 * @return Video 
 */
Video new_Video (char* path,int width,int height);
Video new_Video_NoSound (char* path,int width,int height);

//Fonctions élémentaires :

/**
 * @brief mets a jour les grahismes et render une frame sur la fenetre
 */
void updateGraphics();
/**
 * @brief démarre la boucle graphique Cigma
 * 
 * @param largeurFenetre largeur de la fenetre (en pixels)
 * @param hauteurFenetre hauteur de la fenetre (en pixels)
 * @param xfenetre abscisse de la fenetre (en pixels)
 * @param yfenetre ordonee de la fentre (en pixels)
 * @param nomFenetre nom de la fenetre
 * @param limFPS limite d'image par seconde max
 * @param callbacks fonctions callbacks de boucle graphique dans cet ordre : {initialize,update,render,mouseMoved,windowResize,windowClose} (chaque arg est NULLABLE)
 * @param argc Pointeur sur argc
 * @param argv Pointeur sur argv
 * @return int = 0 si pas d'erreurs, =1 si erreur
 */
int startGFX(int largeurFenetre,int hauteurFenetre,int xfenetre,int yfenetre,char* nomFenetre,int limFPS,void (*callbacks[7])(void),int* argc,char** argv);

/**
 * @brief la touche [CONTROL] su clavier est elle appuyée ?
 * 
 * @return true oui
 * @return false non
 */
bool controlPressed ();

/**
 * @brief la touche [SHIFT] du clavier est elle appuyée ?
 * 
 * @return true oui
 * @return false non
 */
bool shiftPressed ();

/**
 * @brief la touche [ALT] du clavier est elle appuyée ?
 * 
 * @return true oui
 * @return false non
 */
bool altPressed ();

/**
 * @brief Redefinit l'état du presse papier de l'OS 
 * 
 * @param str texte a copier dans le presse papier
 */
void setClipboard(char* str);

/**
 * @brief Renvoie l'etat du presse papier
 * 
 * @return char* texte contenu dans le presse papier
 */
char* getClipboard();

/**
 * @brief Rends le nombre de secondes écoulées depuis le 01/01/1970
 * 
 * @return double nombre de secondes écoulées depuis le 01/01/1970
 */
double getTimeVal ();

/**
 * @brief quite la fenetre graphique
 * 
 */
void endProcess ();

/**
 * @brief Redefinit la position de la fenetre
 * 
 * @param x abcisse (en pixels)
 * @param y ordonéee (en pixels)
 */
void setWindowPosition (int x,int y);

/**
 * @brief Redifinit la taille de la fenetre
 * 
 * @param width longueur (en pixels)
 * @param height hauteur (en pixels)
 */
void setWindowSize (int width,int height);

/**
 * @brief mets la fenetre en plein écran
 * 
 */
void fullscreen ();

/**
 * @brief minimize la fenetre
 * 
 */
void minimize ();

/**
 * @brief Redéfinit l'icone de la fenetre
 * 
 * @param imgname chemin vers l'icone (image png,jpg,bmp...)
 */
void setIcon (char* imgname);

/**
 * @brief Renvoie le nom de la fenetre
 * 
 * @return char* nom de la fenetre
 */
char* getWindowName ();

/**
 * @brief Renvoie l'ID freeglut de la fenetre
 * 
 * @return int ID
 */
int getWindowId_gl ();

/**
 * @brief Renvoie l'ID x11 de la fenetre
 * 
 * @return int ID
 */
unsigned long getWindowId_x11 ();

/**
 * @brief replaces the window cursor with OS standard cursors
 * 
 * @param glutcursor : GLUT constant for cursors. 
 * 	Possible Values :
 * 	 - GLUT_CURSOR_RIGHT_ARROW
 * 	 	Arrow pointing up and to the right.
 * 	 - GLUT_CURSOR_LEFT_ARROW
 * 	 	Arrow pointing up and to the left. [DEFAULT]
 * 	 - GLUT_CURSOR_INFO
 * 	 	Pointing hand.
 * 	 - GLUT_CURSOR_DESTROY
 * 	 	Skull & cross bones.
 * 	 - GLUT_CURSOR_HELP
 * 	 	Question mark.
 * 	 - GLUT_CURSOR_CYCLE
 * 	 	Arrows rotating in a circle.
 * 	 - GLUT_CURSOR_SPRAY
 * 	 	Spray can.
 * 	 - GLUT_CURSOR_WAIT
 * 	 	Wrist watch.
 * 	 - GLUT_CURSOR_TEXT
 * 	 	Insertion point cursor for text.
 * 	 - GLUT_CURSOR_CROSSHAIR
 * 	 	Simple cross-hair.
 * 	 - GLUT_CURSOR_UP_DOWN
 * 	 	Bi-directional pointing up & down.
 * 	 - GLUT_CURSOR_LEFT_RIGHT
 * 	 	Bi-directional pointing left & right.
 * 	 - GLUT_CURSOR_TOP_SIDE
 * 	 	Arrow pointing to top side.
 * 	 - GLUT_CURSOR_BOTTOM_SIDE
 * 	 	Arrow pointing to bottom side.
 * 	 - GLUT_CURSOR_LEFT_SIDE
 * 	 	Arrow pointing to left side.
 * 	 - GLUT_CURSOR_RIGHT_SIDE
 * 	 	Arrow pointing to right side.
 * 	 - GLUT_CURSOR_TOP_LEFT_CORNER
 * 	 	Arrow pointing to top-left corner.
 * 	 - GLUT_CURSOR_TOP_RIGHT_CORNER
 * 	 	Arrow pointing to top-right corner.
 * 	 - GLUT_CURSOR_BOTTOM_RIGHT_CORNER
 * 	 	Arrow pointing to bottom-left corner.
 * 	 - GLUT_CURSOR_BOTTOM_LEFT_CORNER
 * 	 	Arrow pointing to bottom-right corner.
 * 	 - GLUT_CURSOR_FULL_CROSSHAIR
 * 	 	Full-screen cross-hair cursor (if possible, otherwise GLUT_CURSOR_CROSSHAIR).
 * 	 - GLUT_CURSOR_NONE
 * 	 	Invisible cursor.
 * 	 - GLUT_CURSOR_INHERIT
 * 	 	Use parent's cursor.
 */
void setCursor (int glutcursor);

/**
 * @brief DO NOT USE
 */
void setVerticalOffset(int offset);

/**
 * @brief DO NOT USE
 */
int getVerticalOffset();
/**
 * @brief enleve l'overlay de fenetre de l'OS
 */
void removeOverlayOS ();
/**
 * @brief mets l'overlay de fenetre de l'OS
 */
void addOverlayOS ();

//Fonctions Couleur :

/**
 * @brief creation d'une nouvelle couleur RGB
 * @param r rouge 	de 0 a 255
 * @param g vert 	de 0 a 255
 * @param b bleu 	de 0 a 255
 * @return RGB couleur
 */
RGB new_RGB (unsigned char r,unsigned char g,unsigned char b);

/**
 * @brief creation d'une nouvelle couleur RGBA avec transparence
 * @param r rouge 	de 0 a 255
 * @param g vert 	de 0 a 255
 * @param b bleu 	de 0 a 255
 * @param a opacité de 0 a 255
 * @return RGBA couleur
 */
RGBA new_RGBA (unsigned char r,unsigned char g,unsigned char b,unsigned char a);

/**
 * @brief convertit une couleur RGB en une couleur RGBA 
 * @param color couleur
 * @param opacity opacité de 0 a 255
 * @return RGBA couleur
 */
RGBA RGBtoRGBA (RGB color,unsigned char opacity);

/**
 * @brief convertit une couleur RGBA en une couleur RGB
 * @param color couleur
 * @return RGB couleur
 */
RGB RGBAtoRGB (RGBA color);

void setColor4i (unsigned char r, unsigned char g, unsigned char b,unsigned char a);
void setColorRGB (RGB color);
void setColorRGBA (RGBA color);
void setColor (unsigned char r,unsigned char g,unsigned char b);
void color (unsigned char r,unsigned char g,unsigned char b);

//Fonctions Utilitaires :

void clear(unsigned char r,unsigned char g,unsigned char b);
void clearRGB(RGB color);
void displayFPS (int nbrefresh);

//Fonctions Pinceau :

void fill(int r,int g,int b,int a);
void fillRGBA(RGBA color);
void fillRGB(RGB color);
RGBA getFill();

void stroke (int r,int g,int b,int a);
void strokeRGBA (RGBA color);
void strokeRGB (RGB color);
RGBA getStroke();

void strokeWidth(int width);
int getStrokeWidth();

void rotate(double angle);
void selfRotate(double angle);
double getSelfRotate();

//Fonction Dessin Géometrique :

void dot(int x,int y);
void line(int x1, int y1, int x2, int y2);
void vertexLine(int x1,int y1,int x2,int y2);
void cross (int x,int y,int width,int height);

void rectangle(int x,int y, int width,int height);
void quads (int x1,int y1,int x2,int y2,int x3,int y3,int x4,int y4);
void circle(int x, int y, float r,int nbpts);
void triangle(int x1,int y1,int x2,int y2,int x3,int y3);
void square(int x,int y,int width);
void roundedRectangle (int x,int y,int width,int height,float coef,int rnbpts);
void ellipse(int x,int y,int radiusX, int radiusY);

void fillRectangle(int x,int y, int width,int height);
void fillQuads (int x1,int y1,int x2,int y2,int x3,int y3,int x4,int y4);
void fillCircle(int x, int y, float r,int nbpts);
void fillTriangle(int x1,int y1,int x2,int y2,int x3,int y3);
void fillSquare(int x,int y,int width);
void fillRoundedRectangle (int x,int y,int width,int height,float coef,int rnbpts);
void fillEllipse(int x,int y,int radiusX, int radiusY);

void smoothRectangle(int x,int y,int width,int height);
void smoothQuads(int x1,int y1,int x2,int y2,int x3,int y3,int x4,int y4);
void smoothCircle (int x, int y, float r,int nbpts);
void smoothTriangle (int x1,int y1,int x2,int y2,int x3,int y3);
void smoothSquare (int x,int y,int width);
void smoothRoundedRectangle(int x,int y,int width,int height,float coef,int rnbpts);
void smoothEllipse(int x,int y,int radiusX, int radiusY);

//Fonctions Texte :

typedef int font_type;
#define FONT_TYPE_NULL 0
#define FONT_TYPE_BITMAP_CHARACTERS 1
#define FONT_TYPE_STROKE_CHARACTERS 2
#define FONT_TYPE_TRUETYPE 3

font_type getFontType (void* font);
FontText* new_FontText(char* fontName);
FontText* makeVectorialFont (void* font,int size);
void* copyFont (void* font);
void freeFontText(FontText** ft);
void text(void *ft_font, int x, int y, int h,char* format,...);
int getTextWidth (void* font,int h, char* format,...);
int getTextHeight (void* font);
//retourne la position x en pixel de chaque charactere de la chaine 
int* getCharPosition (FontText* font,int h,char* text,...);
void bitmapText(void * font,int x,int y, char *format,...);
void strokedText(char *text, float x, float y,int h,FontText* font);
void truetypeTextScaled(char* text,FontText* ft_font,int x,int y,float scale);

//Fonctions Sonores :

Sound new_Sound (char* filename);
void freeSound (Sound* sound);

//Fonctions 3D : 
void rectangle3D(float posx, float posy, float posz, float lx, float ly, float lz);
void drawRepere(float unitaire);
void drawGrid(int rayon, float step);
void drawSphere(float posx, float posy, float posz, float rayon, float prec, GLenum style);

//loading de fichier OBJ
GLuint loadObj(char *fname);
void drawModel(GLuint model,float size);


//Cameras
#ifndef class
	//pseudo classe en C
	#define class(x) typedef struct x x; struct x
#endif

/**
 * @brief actualise les coordonnées x_cam et y_cam pour que l'on puisse se deplacer a
 * l'aide de la souris sur un plan 2D
 */
class(Camera2D){
    bool grabbed;
    float rotation_x;
    float rotation_y;
    int x;
    int y;
    float zoom;

    int offsetx;
	int offsety;
	int oldAS;
	int oldOS;

    void (*update) (Camera2D* self);
    void (*apply) (Camera2D* self);
    void (*reset) (Camera2D* self);
};

Camera2D new_GrabCamera2D ();

class(CameraFPS){
    float angle;
    float angle1;
    float lx;
    float ly;
    float lz;
    float x;
    float y;
    float z;
    float zoom;
    float speed;
    float rotSpeed;
    bool warping;
    bool mouseCaptured;

    int offsetx;
	int offsety;
	int oldAS;
	int oldOS;

    void (*update) (CameraFPS* self);
    void (*apply) (CameraFPS* self);
    void (*reset) (CameraFPS* self);
};

CameraFPS new_CameraFPS ();

class(Camera3D){
    bool grabbed;
    float rotation_x;
    float rotation_y;
    float rotation_z;
    int x;
    int y;
    int z;
    float zoom;

    int offsetx;
	int offsety;
	int oldAS;
	int oldOS;

    void (*update) (Camera3D* self);
    void (*apply) (Camera3D* self);
    void (*reset) (Camera3D* self);
};

Camera3D new_GrabCamera3D ();


// Globales externes 
extern unsigned char keyboardState[255];
extern unsigned char specialKeyboardState[21];
extern int mouseState;
extern bool handleSouris;
extern int AS;
extern int OS;





#ifndef GL_CLAMP_TO_EDGE
	#define GL_CLAMP_TO_EDGE 0x812F
#endif

GLuint loadTexture(const char * filename,bool useMipMap);
int takeScreenshot(const char * filename);
void drawAxis(double scale);
int initFullScreen(unsigned int * width,unsigned int * height);
int XPMFromImage(const char * imagefile, const char * XPMfile);
SDL_Cursor * cursorFromXPM(const char * xpm[]);

typedef struct Image{
	int width;
	int height;
	float zoom;
	float static_width;
	float static_height;
	RGBA color;
	GLuint donneesRGB;
}Image;

Image* new_Image (char* filename,bool antialiasing);
void image (Image* I,int x,int y);
void imageCustom (Image* I,int x,int y,int width,int height);
void fitImageToRectangle (Image* img,int max_width,int max_height);
void freeImage(Image** img);

/**
 * @brief affiche du texte en ajustant sa taille de facon a ce qu'l rentre dans un rectangle
 * 
 * @param text texte a afficher
 * @param x x du rectangle
 * @param y y du rectangle
 * @param width largeur du rectangle
 * @param height hauteur du rectangle
 */
void fitTextToRectangle (void *font, int x, int y,int width,int height,char* format,...);

#endif