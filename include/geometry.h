#ifndef K_GEO_LIB
#define K_GEO_LIB

typedef struct Point_2f{
	float x,y;
}Point_2f;

typedef struct Point{
	int x,y;
}Point;

typedef struct Line{
	Point p1,p2;
}Line;

typedef struct Polygon{
	Point* pts;
	int nbpts;
	int width;
	int height;
}Polygon;

bool pointIsInRectangle (int x,int y,int r_pos_x,int r_pos_y,int r_width,int r_height);
void drawPolygon (Polygon P,int x,int y);
void drawPolygonZoomed (Polygon P,int x,int y,float zoom);
bool pointIsInCircle (int x,int y,int xc,int yc,int r);
void freePolygon(Polygon p);
Point** makeLine (int xa, int ya, int xb, int yb);
//concatenation de tableau de points
Point** Point_cat(Point** p1,Point** p2);

Point new_Point (int x,int y);
Point_2f new_Point_2f (float x,float y);
Line new_Line (int x1,int y1,int x2,int y2);

char* Point_tostring (Point p);
char* Point_2f_tostring (Point_2f p);
char* Ligne_tostring (Line l);
char* Polygon_tostring (Polygon p);

#endif