#ifndef CSS_PARSER_H
#define CSS_PARSER_H

#include "../include/cigma.h"

typedef struct Ratio{
    int px;
    float prct;
}Ratio;


#define NB_FIELDS_CSS 11

typedef struct Style{
    bool defaults[NB_FIELDS_CSS];

	Ratio width,height;
	void* font;
	RGBA fill;
	RGBA stroke;
	RGBA text_color;
	Image* skin;
	float round_factor;
	Ratio stroke_width;
	Ratio text_size;
} Style;



Ratio new_Ratio (int px,float percent);
int ratioValue (Ratio r,int max);
Style parseCSS (char* path,char* classname);
Style addStyles(Style s1,Style s2);

#endif