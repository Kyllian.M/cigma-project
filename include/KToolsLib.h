#ifndef _TR1_STDIO_H
     #include <stdio.h>
#endif
#ifndef _TR1_STDBOOL_H
     #include <stdbool.h>
#endif

#define NOIR 0,0,0
#define BLANC 255,255,255

#define PI 3.14159265358979323846
#define count(x) count2((void**)x)
#define betterFree(x) betterFree2((void**)x)

#define foreach(val,tab) \
	for(int foreach_cpt = 0,foreach_size = count((void**)tab)-1,foreach_keep = 1;\
	foreach_keep && foreach_cpt < foreach_size; \
	foreach_keep = !foreach_keep, foreach_cpt++) \
	for(val = (tab) + foreach_cpt; foreach_keep ; foreach_keep = !foreach_keep)

#define typename(x) _Generic((x),                                                 \
        _Bool: "_Bool",                  unsigned char: "unsigned char",          \
         char: "char",                     signed char: "signed char",            \
    short int: "short int",         unsigned short int: "unsigned short int",     \
          int: "int",                     unsigned int: "unsigned int",           \
     long int: "long int",           unsigned long int: "unsigned long int",      \
long long int: "long long int", unsigned long long int: "unsigned long long int", \
        float: "float",                         double: "double",                 \
  long double: "long double",                   char *: "pointer to char",        \
       void *: "pointer to void",                int *: "pointer to int",         \
      default: "other")

#define in_range(var,low,max) int var=low;var<max;var++
#define in_range_decr(var,high,min) int var=high;var>min;var--

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
													     DYNAMIC ARRAYS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
#ifndef DYNAMIC_ARRAYS
#define DYNAMIC_ARRAYS

/**
 * @brief Dyanmic array for a specific type of variable
 * @param type : type of the array
 */
#define DynArray(type) type** 

/**
 * @brief Creates a dynamic array of the selected type
 *  - accessor/mutator : *(array[index])
 * @param type : type of the array
 */
#define new_DynArrayOf(type) (type**) calloc(1,sizeof(type*))

/**
 * @brief Returns the number of elements in the dynamic array
 * @param array : array to be measured
 */
#define DynArray_count(array) ({ 	     \
	double** A = (double**) array;	\
	int i = 0;				     \
	while(A[i]){					\
		i++;						\
	}							\
	i;							\
})

/**
 * @brief adds an element to the end of the dynamic array
 * @param array : array to be modified
 * @param elem : element to be added
 */
#define DynArray_add(array,elem) {                		\
  	int nb = DynArray_count((void**)array);                \
	array = realloc(array,sizeof(typeof(elem)*) *(nb+2)); 	\
	array[nb] = malloc(sizeof(typeof(elem)));         	\
	*(array[nb]) = elem;                              	\
	array[nb+1] = NULL;                               	\
}

/**
 * @brief removes an element from the dynamic array
 * @param array : array to be modified
 * @param index : index of the element to be removed
 */
#define  DynArray_delete(array,index) {   	\
	free(array[index]); 			     \
	for(int i = index;array[i] != NULL;i++) \
		array[i] = array[i+1];             \
}

/**
 * @brief frees all the memory occupied by a dynamic array
 * @param array : array to be freed
 */
#define DynArray_free(array){								\
	int cpt = DynArray_count((void**)array);				\
	for(int i=0;i<cpt;i++) {free(array[i]); array[i] = NULL;}   \
	free(array); 										\
	array = NULL;										\
}

/**
 * @brief deletes all the elements of a dynamic array
 * @param array : array to be emptied
 */
#define DynArray_deleteAll(array){							\
	int cpt = DynArray_count((void**)array);				\
	for(int i=0;i<cpt;i++) {free(array[i]); array[i] = NULL;}	\
}

#endif

bool str_same (char* str1,char* str2);
int str_substringIndex (char* source,char* substr);
char* str_removeAll(char* src,char target);
int parseInt (char* str);
float parseFloat (char* str);
double min (double a,double b);
double max (double a,double b);
char** str_split(char* a_str, const char a_delim);
int getDbText (char* fcontents,char* filename);
int count2 (void** T);
char* str_replaceAll(char* src/*,char* target,char* replacement*/);
char* fileGetText (char* filename);
char* fileGetExtension (char* path);

char* runProcess (char* command,char* logs_location);

//retourne le nombre d elements initialisés dans un tableau bidimensionnel d entier
int trueSizeOfIntTab (int **T);
//cree par Kyllian MARIE

//retourne le nombre d elements initialisés dans un tableau d entier
int trueSizeOfInt (int *T);
//cree par Kyllian MARIE

//retourne true si un fichier donné existe
bool fileExist (char* nomFichier);
//cree par Kyllian MARIE

//fclose seulement si le fichier != NULL
void betterfclose (FILE* f);
//cree par Kyllian MARIE

//free seulement si le pointeur != NULL
void betterFree2 (void **ptr);
//cree par Kyllian MARIE

//permet de malloc un tableau a deux dimensions
void** bigMalloc (int size,int DIM1,int DIM2);
//cree par Kyllian MARIE

//permet de generer une chaine contenant un chemin vers un fichier donné avec un nombre aleatoire
char* randomPath(int nbFichier,char* path,char* extension);
//cree par Kyllian MARIE


bool isValInArray (void** tab,void* val);
char* str_format (char* format,...);

float randFloat ();

/**
 * @brief returns true if the string starts with another string
 * @param str string to be evaluated
 * @param start expected start to the string
 * @return true : the string starts with the expected starting expression
 * @return false : the string doesn't starts with the expected starting expression
 */
bool str_startsWith(char* str,char* start);