// TYPEDEFS
#include "../include/geometry.h"
#include "../include/cssparser.h"

#ifndef class
	//pseudo classe en C
	#define class(x) typedef struct x x; struct x
#endif

typedef Point** Trajectoire;


// Structure bouton clickable
class(Button){
	//coordonées du bouton en pixels
	int x,y;
	//texte affiché sur le bouton
	char *text;
	//est ce que le bouton est survolé par la souris
	bool hover;
	//type de listener pour son event (click,touche du clavier)
	char listener[50];
	//ancienes dimensions du bouton
	int oldwidth,oldheight;
	//evenement a déclencher lors de l'apppui sur le bouton
	void (*event) (void);

	//style du bouton
	Style style;
	//style du bouton lors d'un survollage
	Style hover_style;

	/**
	 * @brief ajout d'un evenement a un bouton
	 * @param self : bouton
	 * @param listener type d'evenement activant le bouton ex: "click", "key_a" ...
	 * @param event fonction callback du bouton
	 */
	void (*addEventListener) 	(Button* self,char* listener,void (*event) (void));

	/**
	 * @brief arrondie les bordures d'un bouton
	 * @param self : bouton
	 * @param factor : facteur de rondeur entre 0 et 1
	 */
	void (*roundBorders) 		(Button* self,float factor);

	/**
	 * @brief affiche un bouton
	 * @param self : bouton
	 * @param x : abscisse en pixels
	 * @param y ordonee en pixels
	 */
	void (*show) 				(Button* self,int x,int y);

	/**
	 * @brief actualise un bouton
	 * @param self : bouton
	 */
	void (*update)				(Button* self);
};

/**
 * @brief création d'un nouveau bouton
 * 
 * @param text texte a afficher sur le bouton
 * @param width largeur du bouton
 * @param height hauteur du bouton
 * @param font police d'affichage
 * @return Button 
 */
Button new_Button		(char* text,int width,int height,void* font);

/**
 * @brief création d'un nouveau bouton a partir d'un fichier CSS
 * @param text texte a afficher sur le bouton
 * @param pathcss chemin du fichier CSS
 * @param classname classe css du bouton
 * @return Button 
 */
Button new_Button_CSS	(char* text,char* pathcss,char* classname);

//structure champs de texte
class(Textfield){
	//coordonées du textfield
	int x,y;
	//texte contenu dans le textfield
	char *text;
	//le textfield est il en train d'etre édité
	bool isEditing;

	//etat d'affichage du curseur 
	bool blink;
	//debut de la zone de selction de texte 
	int selecZonex; 
	//fin de la zone de selection de texte 
	int selecZonew; 
	//position du curseur dans le texte
	int cursorpos;
	//temps a la création du textfield
	double t0;
	//si oui ou non le champs de selction est actif
	bool held;
	//texte selectioné par le selecteur de texte
	char* selected_text;

	//style css
	Style style;
	//style css lors d'un focus sur le textfield 
	Style focus_style;
	
	/**
	 * @brief arrondit les bordures du textfield
	 * @param self : textfield
	 * @param factor : facteur de rondeur de 0 a 1
	 */
	void (*roundBorders) 	(Textfield* self,float factor);

	/**
	 * @brief affiche le textfield 
	 * @param self : textfield
	 * @param x : abscisse en pixels
	 * @param y : ordonée en pixels
	 */
	void (*show) 			(Textfield* self,int x,int y);

	/**
	 * @brief actualise le textfield
	 * @param self : textfield
	 */
	void (*update)			(Textfield* self);
};

/**
 * @brief crée un nouveau textfield
 * @param width largeur du textfield en pixels 
 * @param height hauteur du textfield en pixels
 * @param font police de texte
 * @return Textfield 
 */
Textfield new_Textfield(int width,int height,void* font);

/**
 * @brief crée un nouveau textfield a partir d'un fichier CSS
 * @param csspath chemin d'acces du fichier CSS
 * @param classname nom de la classe CSS
 * @return Textfield 
 */
Textfield new_Textfield_CSS(char* csspath,char* classname);

//structure label
class(Label){
	//coordonées du Label
	int x,y;
	//texte contenu dans le label
	char *text;

	//debut de la zone de selction de texte 
	int selecZonex; 
	//fin de la zone de selection de texte 
	int selecZonew; 
	//si oui ou non le champs de selction est actif
	bool held;
	//texte selectioné par le selecteur de texte
	char* selected_text;

	//style css du label
	Style style;
	
	/**
	 * @brief affiche le label
	 * @param self : label
	 * @param x : abscisse en pixels
	 * @param y : ordonée en pixels
	 */
	void (*show) 			(Label* self,int x,int y);
	/**
	 * @brief actualise le label
	 * @param self : label
	 */
	void (*update)			(Label* self);
};

/**
 * @brief crée un nouveau label
 * @param text texte a afficher
 * @param width largeur en pixels
 * @param height hauteur en pixels
 * @param font police du texte
 * @return Label 
 */
Label new_Label(char* text,int width,int height,void* font);
/**
 * @brief crée un nouveau label a partir d'un fichier CSS
 * @param text texte a afficher
 * @param csspath chemin d acces du fichier css
 * @param classname nom de la classe CSS
 * @return Label 
 */
Label new_Label_CSS(char* text,char* csspath,char* classname);


//structure menu clic droit
class(ClickMenu){
	//options du menu
	Button* option;
	//position des séparateurs
	int* separateurs;
	//nombre d'options
	int nboption;
	//nombre de séparateurs
	int nbseparateurs;
	//click men est affiché ou non 
	bool is_shown;

	/**
	 * @brief affiche le clickmenu
	 * @param self : clickmenu
	 * @param x : abscisse en pixels
	 * @param y ordonnée en pixels
	 */
	void (*show) (ClickMenu* self,int x,int y);

	/**
	 * @brief actualise le clickmenu
	 * @param self : clickmenu
	 */
	void (*update) (ClickMenu* self);
};

//structure menu déroulant
class(DropMenu){
	//position du label
	int x,y;
	//nom des options
	char** option;
	//nom de l option selectionnée
	char* selected_option;
	//index de l option selectioné
	int index_selected_option;
	//le dropmenu est il déroullé ?
	bool isOpened;
	//index de l option survollée
	int index_option_hovered;
	//callback lors de la selection d une option
	void (*event) ();

	//style css
	Style style;
	//style css au survol
	Style style_hover;

	//Methodes

	/**
	 * @brief ajout d'une option au dropMenu
	 * @param self : DropMenu
	 * @param option : option a ajouter
	 */
	void (*add) (DropMenu* self,char* option);

	/**
	 * @brief retourne l'index de l'option donnée
	 * @param self : DropMenu
	 * @param option : option dont on desire l index
	 */
	int  (*indexOf) (DropMenu* self,char* option);

	/**
	 * @brief supprime une option du dropmenu
	 * @param self : DropMenu
	 * @param option : option a supprimer
	 */
	void (*delete) (DropMenu* self,char* option);

	/**
	 * @brief affiche le dropmenu
	 * @param self : DropMenu
	 * @param x : abscisse en pixels
	 * @param y : ordonée en pixels
	 */
	void (*show) (DropMenu* self,int x,int y);

	/**
	 * @brief actualise le dropmenu
	 * @param self : DropMenu
	 */
	void (*update) (DropMenu* self);

	/**
	 * @brief supprime toutes les options du dropmenu
	 * @param self : DropMenu
	 */
	void (*deleteAll) (DropMenu* self);
};

/**
 * @brief Cree un nouveau dropmenu
 * @param width largeur en pixels
 * @param height heuteur en pixels
 * @return DropMenu 
 */
DropMenu new_DropMenu (int width,int height);

/**
 * @brief Cree un nouveau dropmenu a partir d un fichier CSS
 * @param csspath chemin d acces du fichier css
 * @param classname nom de la class css
 * @return DropMenu 
 */
DropMenu new_DropMenuCSS (char* csspath,char* classname);

//structure permettant de faire varier des coordonées selon une trajectoire
class(Mover){
	//le mover est actif ?
	bool activated;
	//trajectoir a suivre
	Trajectoire trajectoire;
	//index du point courrant de la trajectoire
	int current_pt;
	//l'animation se repette ?
	bool repeat;
	//temps a la creation du mover
	double t0;
	//combien de temps dure l'animation en secondes
	float tps;
	//temps passé depuis la creation du mover
	float elapsed_time;
	//inverser la trajectoire ?
	bool reverse;
	//faire une boucle en inversant la trajectoire ?
	bool reverse_loop;
	//repeter l animation ?
	bool loop;
	
	//x a modifier
	int *x;
	//y a modifier
	int *y;

	/**
	 * @brief active le mover et demmare l'animation
	 * @param self : Mover
	 */
	void (*activate)(Mover* self);
	/**
	 * @brief desactive le mover et stope l'animation
	 * @param self : Mover
	 */
	void (*deactivate)(Mover* self);
	/**
	 * @brief assigne les coordonées a faire varier par le mover
	 * @param self : Mover
	 */
	void (*assign)(Mover* self,int* x,int *y);
	/**
	 * @brief actualise le mover
	 * @param self : Mover
	 */
	void (*update)(Mover* self);
};

/**
 * @brief cree un nouveau Mover
 * @param trajectoire points indiquant la trajectoire a suivre par le mover
 * @param repeat doit on repeter l annimation
 * @param tps combien de temps dure l animation en secondes
 * @param reverse_loop faire une boucle avec une inversion de la trajectoire ?
 * @param loop faire une boucle ?
 * @return Mover 
 */
Mover new_Mover (Point** trajectoire,bool repeat,float tps,bool reverse_loop,bool loop);

//structure slider
class(Slider){
	//position du slider en pixels
	int x,y;
	//valeurs minimale te maximale selectionable sur le slider
	double min,max;
	//valeur du slider
	double value;
	//autoriser les nombres a virgule ?
	bool float_mode;
	//le slider est il selectionné ?
	bool selected;
	//callback lors de la modification de la valeur du slider
	void (*callback) (void);

	//style css
	Style style;

	/**
	 * @brief definit si le slider accepte les flottants
	 * @param self : Slider
	 */
	void (*setFloatMode) (Slider* self);
	/**
	 * @brief definit si le slider n accepte que les entiers
	 * @param self : Slider
	 */
	void (*setIntMode) (Slider* self);
	/**
	 * @brief affiche le slider
	 * @param self : Slider
	 * @param x : abscisse en pixels
	 * @param y : ordonnée en pixels
	 */
	void (*show) (Slider* self,int x,int y);
	/**
	 * @brief définit le callback a la selection d une option
	 * @param self : Slider
	 * @param callback : fonction appelée a la modification du slider
	 */
	void (*setCallback) (Slider* self,void (*callback) (void));
	/**
	 * @brief actualise le slider
	 * @param self : Slider
	 */
	void (*update) (Slider* self);
};

/**
 * @brief crée un nouveau slider
 * @param min valeur minimale selectionable
 * @param max valeur maximale selectionable
 * @param default_value valeur par défaut
 * @return Slider 
 */
Slider new_Slider (double min,double max,double default_value);

/**
 * @brief cree un nouveau slider a partir d'un fichier CSS
 * @param min valeur minimale selectionable
 * @param max valeur maximale selectionable
 * @param default_value valeur par défaut
 * @param csspath chemin d acces du fichier CSS
 * @param classname nom de la classe CSS
 * @return Slider 
 */
Slider new_Slider_CSS (double min,double max,double default_value,char* csspath,char* classname);

//structure selecteur de couleur RGB
class(ColorPicker){
	//le color picker est il selectionné
	bool selected;
	//couleur selectionée
	RGB selection;
	//gradient de couleur
	Image* gradient;
	//indexeur comprenant toutes les couleurs selcetionnables
	RGB** indexer;
	//largeur en pixels
	int width;
	//hauteur en pixels
	int height;
	//position en pixels
	int x,y;
	//position de la couleur selectionée sur le gradient
	int xselec,yselec;

	Slider r_slider;
	Slider g_slider;
	Slider b_slider;

	Button validate;

	//actualisation
	void (*update) (ColorPicker* self);
	//affichage
	void (*show) (ColorPicker* self,int x,int y);
	//definit la fonction a appeler lorsque une couleur est selectionée
	void (*setCallback) (ColorPicker* self,void (*function) (void));
};

class(Counter){
	int x,y;
	int value;
	int valmax;
	int valmin;
	Style style;
	void (*eventListener) (void);


	void (*update) (Counter* self);
	void (*show) (Counter* self,int x,int y);
	void (*addEventListener) (Counter* self,void (*func)(void));
};

Counter new_Counter (int valmin,int val,int valmax);
Counter new_Counter_CSS (int valmin,int val,int valmax,char* csspath,char* classname);


class(Overlay){
	Image* icon;
	Style style;
	bool movewindow;
	bool activated;

	void(*update) (Overlay* self);
	void(*show) (Overlay* self);
	void(*activate) (Overlay*self);
};

Overlay new_Overlay(char* imagePathIcon);
Overlay new_Overlay_CSS(char* imagePathIcon,char* csspath,char* classname);