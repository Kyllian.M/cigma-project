#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "../include/cigma.h"

#include "../include/KToolsLib.h"
#include "../include/consoletools.h"
#include "../include/geometry.h"
#include "../include/cssparser.h"
#include "../include/Cwing.h"


/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	BUTTON
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void addEventListenerButton (Button *B,char* listener,void (*event) (void));
void roundBordersButton 	(Button* B,float coef);
void showButton				(Button *B,int x,int y);
void updateButton 			(Button* B);

Style defaultStyleButton(){
	Style style;
	for(int i=0;i<NB_FIELDS_CSS;i++){
		style.defaults[i] = true;
	}
	style.font = makeVectorialFont(GLUT_STROKE_ROMAN,12);
	style.width = new_Ratio(60,0);
	style.height = new_Ratio(30,0);
	style.stroke_width = new_Ratio(2,0);
	style.round_factor = 0.5;
	style.stroke = new_RGBA(154,154,154,255);
	style.fill = COLOR_RGBA_GAINSBORO;
	style.text_color = RGBtoRGBA(COLOR_BLACK,178);
	style.skin = NULL;
	style.text_size = new_Ratio(12,0);
	return style;
}

Style defaultStyleButtonHover(){
	Style style = defaultStyleButton();
	style.stroke = new_RGBA(137,206,248,230);
	style.stroke_width = new_Ratio(3,0);
	style.fill = new_RGBA(240,240,240,255);
	return style;
}

void updateFontSizeButton(Button* b){
	DEBUGSTART;
	Style style = b->hover ? b->hover_style : b->style;
	if(getFontType(style.font) != FONT_TYPE_NULL && getFontType(style.font) != FONT_TYPE_BITMAP_CHARACTERS){
		int i;
		for(i= ratioValue(style.height,HF)*0.7;getTextWidth(style.font,i,b->text) > ratioValue(style.width,LF)*0.9;i--){}
		b->hover_style.text_size = b->style.text_size = new_Ratio(i,0);
	}
	else{
		b->hover_style.text_size = b->style.text_size = new_Ratio(getTextHeight(style.font),0);
	}
	DEBUGEND;
}

Button new_Button(char* text,int width,int height,void* font){
	DEBUGSTART;
	Button B;

	B.x = 0;
	B.y = 0;
	B.oldwidth = 0;
	B.oldheight = 0;
	B.text = text;
	B.hover = false;
	B.listener[0] = '\0';
	B.event = NULL;

	Style style 		= defaultStyleButton();
	Style style_hover 	= defaultStyleButtonHover();
	style_hover.width 	= style.width 	= new_Ratio(width,0);
	style_hover.height 	= style.height 	= new_Ratio(height,0);
	if(font != NULL)
		style_hover.font 	= style.font 	= font;
	B.style = style;
	B.hover_style = style_hover;
	updateFontSizeButton(&B);

	B.addEventListener = addEventListenerButton;
	B.show = showButton;
	B.update = updateButton;
	B.roundBorders = roundBordersButton;

	DEBUGEND;
	return B;
}

Button new_Button_CSS(char* text,char* pathcss,char* classname){
	Button B;

	B.x = 0;
	B.y = 0;
	B.oldwidth = 0;
	B.oldheight = 0;
	B.text = text;
	B.hover = false;
	B.listener[0] = '\0';
	B.event = NULL;

	Style style = addStyles(parseCSS(pathcss,classname),defaultStyleButton());
	B.style = style;
	Style style_hover = addStyles(addStyles(style,parseCSS(pathcss,str_format("%s:hover",classname))),defaultStyleButtonHover());
	B.hover_style = style_hover;
	updateFontSizeButton(&B);

	B.addEventListener = addEventListenerButton;
	B.show = showButton;
	B.update = updateButton;
	B.roundBorders = roundBordersButton;
	DEBUGEND;
	return B;
}

void addEventListenerButton (Button *B,char* listener,void (*event) (void)){
	B->event = event;
	strcpy(B->listener,listener);
}

void roundBordersButton (Button* B,float coef){
	DEBUGSTART;
	B->style.round_factor = coef;
	DEBUGEND;
}

void showButton(Button *B,int x,int y){
	double selfrot = getSelfRotate();
	selfRotate(0);

	Style style = B->style;
	if(B->hover){
		style = B->hover_style;
	}

	B->x = x;
	B->y = y;

	int width = ratioValue(style.width,LF);
	int height = ratioValue(style.height,HF);
	int textsize = ratioValue(style.text_size,height);

	if(B->oldheight != height || B->oldwidth != width){
		updateFontSizeButton(B);
	}
	B->oldheight = height;
	B->oldwidth = width;

	fillRGBA(style.fill);
	strokeRGBA(style.stroke);
	strokeWidth(ratioValue(style.stroke_width,width));
	if(style.round_factor == 0){
		rectangle(x,y,width,height);
	}
	else{
		roundedRectangle(x,y,width,height,style.round_factor,64);
	}
	if(style.skin == NULL){
		strokeRGBA(style.text_color);
		strokeWidth(2);
		int pos_x = x+width/2-getTextWidth(style.font,textsize,B->text)/2;
		int pos_y = y+height/2-textsize/2;
		text(style.font,pos_x,pos_y,textsize,B->text);
	}
	else{
		int pos_x = x+width/2 - style.skin->static_width/2;
		int pos_y = y+height/2 - style.skin->static_height/2;
		image(style.skin,pos_x,pos_y);
	}

	selfRotate(selfrot);
}

void updateButton (Button* B){
	Style style = B->style;
	if(B->hover){
		style = B->hover_style;
	}

	int width = ratioValue(style.width,LF);
	int height = ratioValue(style.height,HF);
	if(pointIsInRectangle(AS,OS,B->x,B->y,width,height)){
		B->hover = true;
		setCursor(GLUT_CURSOR_INFO);
		if(B->event!= NULL && !strcmp("click",B->listener)){
			ifleftclickup(){
				B->event();
				handleSouris = false;
			}
		}
	}
	else{
		B->hover = false;
	}
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	TEXTFIELD
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void showTextfield(Textfield *T,int x,int y);
void roundBordersTextfield (Textfield *T,float coef);
void updateTextfield (Textfield *T);
void updateTextSizeTextfield (Textfield* T);

Style defaultStyleTextfield(){
	Style style;
	for(int i=0;i<NB_FIELDS_CSS;i++){
		style.defaults[i] = true;
	}
	style.font = makeVectorialFont(GLUT_STROKE_ROMAN,12);
	style.width = new_Ratio(300,0);
	style.height = new_Ratio(27,0);
	style.stroke_width = new_Ratio(2,0);
	style.round_factor = 0.5;
	style.stroke = new_RGBA(154,154,154,255);
	style.fill = COLOR_RGBA_GAINSBORO;
	style.text_color = RGBtoRGBA(COLOR_BLACK,178);
	style.skin = NULL;
	style.text_size = new_Ratio(30*0.7,0);
	return style;
}

Style defaultStyleTextfieldFocus(){
	Style style = defaultStyleTextfield();
	style.stroke = new_RGBA(137,206,248,230);
	style.stroke_width = new_Ratio(3,0);
	style.fill = new_RGBA(240,240,240,255);
	return style;
}

Textfield new_Textfield(int width,int height,void* font){
	Textfield T;
	T.x = 0;
	T.y = 0;
	T.text = malloc(sizeof(char));
	T.text[0] = '\0';
	T.isEditing = false;
	T.blink = true;
	T.selecZonex = 0; 
	T.selecZonew = 0; 
	T.cursorpos = 0;
	T.t0 = -1;
	T.held = false;
	T.selected_text = NULL;

	Style style = defaultStyleTextfield();
	Style style_focus = defaultStyleTextfieldFocus();
	style_focus.width 	= style.width 	= new_Ratio(width,0);
	style_focus.height 	= style.height 	= new_Ratio(height,0);
	if(font != NULL)
		style_focus.font 	= style.font 	= font;
	T.style = style;
	T.focus_style = style_focus;

	T.update = updateTextfield;
	T.show = showTextfield;
	T.roundBorders = roundBordersTextfield;

	updateTextSizeTextfield(&T);
	return T;
}

Textfield new_Textfield_CSS(char* csspath,char* classname){
	Textfield T;
	T.x = 0;
	T.y = 0;
	T.text = malloc(sizeof(char));
	T.text[0] = '\0';
	T.isEditing = false;
	T.blink = true;
	T.selecZonex = 0; 
	T.selecZonew = 0; 
	T.cursorpos = 0;
	T.t0 = -1;
	T.held = false;
	T.selected_text = NULL;

	Style style = addStyles(parseCSS(csspath,classname),defaultStyleTextfield());
	T.style = style;
	Style style_focus = addStyles(addStyles(style,parseCSS(csspath,str_format("%s:focus",classname))),defaultStyleTextfieldFocus());
	T.focus_style = style_focus;

	T.update = updateTextfield;
	T.show = showTextfield;
	T.roundBorders = roundBordersTextfield;

	updateTextSizeTextfield(&T);
	return T;
}

void updateTextSizeTextfield (Textfield* T){
	if(getFontType(T->style.font) != FONT_TYPE_BITMAP_CHARACTERS){
		Style style = T->isEditing ? T->focus_style : T->style;
		int textsize = ratioValue(style.text_size,0);
		int width = ratioValue(style.width,LF);
		int height = ratioValue(style.height,HF);
		bool cond1 = getTextWidth(style.font,textsize,T->text) > width*0.9;
		bool cond2 = getTextWidth(style.font,textsize,T->text) < width*0.9 && textsize < height*0.7;
		if(cond1 || cond2){
			int i;
			for(i= height*0.7;getTextWidth(style.font,i,T->text) > width*0.9;i--){}
			T->focus_style.text_size.px = T->style.text_size.px = i;
			T->focus_style.text_size.prct = T->style.text_size.prct = 0;
		}
	}
}

void showTextfield(Textfield *T,int x,int y){
	double selfrot = getSelfRotate();
	selfRotate(0);

	T->x = x;
	T->y = y;
	Style style = T->isEditing ? T->focus_style : T->style;
	int width = ratioValue(style.width,LF);
	int height = ratioValue(style.height,HF);
	int textsize = ratioValue(style.text_size,height);
	int strokewidth = ratioValue(style.stroke_width,width);

	if(style.skin == NULL){
		strokeWidth(strokewidth);
		strokeRGBA(style.stroke);
		fillRGBA(style.fill);
		if(style.round_factor <= 0)
			rectangle(x,y,width,height);
		else 
			roundedRectangle(x,y,width,height,style.round_factor,64);
		strokeRGBA(style.text_color);
		int pos_x = x+width/2-getTextWidth(style.font,textsize,T->text)/2;
		int pos_y = y+height/2-textsize/2;
		strokeWidth(2);
		text(style.font,pos_x,pos_y,textsize,T->text);
		if(T->isEditing && T->blink && T->selecZonew == 0){
			int poschar = 0;
			if(T->cursorpos != 0){
				if(T->cursorpos >= strlen(T->text)){
					char* ch = malloc(sizeof(char)*(strlen(T->text)+2));
					sprintf(ch,"%s.",T->text);
					int* charpos = getCharPosition(style.font,textsize,ch);
					poschar = charpos[strlen(T->text)];
					free(charpos);
					free(ch);
				}
				else{
					int* charpos = getCharPosition(style.font,textsize,T->text);
					poschar = charpos[T->cursorpos];
					free(charpos);
				}
			}
			strokeRGBA(style.text_color);
			rectangle(pos_x + poschar - 1,pos_y,2,textsize);
		}
		fill(50,100,255,70);
		strokeWidth(0);
		rectangle(x+T->selecZonex,y,T->selecZonew,height);
	}
	if(style.skin != NULL){
		//TODO afficher textfield avec skin
	}

	selfRotate(selfrot);
}

void roundBordersTextfield (Textfield *T,float coef){
	T->style.round_factor = coef;
}

void updateTextfield (Textfield *T){//TODO Gerer reverse highlight
	static double t0 = -1;
	Style style = T->isEditing ? T->focus_style : T->style;
	int width = ratioValue(style.width,LF);
	int height = ratioValue(style.height,HF);
	int textsize = ratioValue(style.text_size,height);

	if(t0 == -1){
		t0 = getTimeVal();
	}
	if(T->t0 == -1){
		T->t0 = getTimeVal();
	}
	bool mouseInTextField = AS > T->x && AS < T->x+width && OS> T->y && OS < T->y+height;
	if(mouseInTextField){
		setCursor(GLUT_CURSOR_TEXT);
	}
	ifleftclickdown(){
		if(mouseInTextField){
			T->held = true;
			T->selecZonew = 0;
			T->selecZonex = AS - T->x;
		}
	}
	if(T->held){
		T->selecZonew = min(AS-(T->selecZonex + T->x),width - (T->selecZonex));
		if(T->selecZonew < 5){
			T->selecZonew = 0;
			T->selected_text = NULL;
		}
		else{
			if(strlen(T->text) > 0){
				int* charpos = getCharPosition(style.font,textsize,T->text);
					
				int pos_x = width/2-getTextWidth(style.font,textsize,T->text)/2;
				char *str = malloc(sizeof(char)*1000);//TODO CHANGER TAILLE
				int cpt=0;

				int lastcharindex = -1;
				bool firstchar = true;
				int firstcharindex = -1;
				for(int i=0;i<strlen(T->text);i++){
					if(pointIsInRectangle(pos_x+charpos[i],1,T->selecZonex,0,T->selecZonew,2)){
						str[cpt] = T->text[i];
						cpt++;
						lastcharindex = i;
						if(firstchar){
							firstchar = false;
							firstcharindex = i;
						}
					}
				}
				str[cpt] = '\0';

				println("text is %s",str);
				if(firstcharindex != -1){//gerer selection last char
					int posfirstchar = pos_x + charpos[firstcharindex];
					T->selecZonex = posfirstchar;
				}
				if(lastcharindex != -1){//gerer selection last char
					if(lastcharindex != strlen(T->text)-1)
						lastcharindex++;
					int poslastchar = pos_x + charpos[lastcharindex];
					T->selecZonew = poslastchar - T->selecZonex;
				}

				if(T->selected_text != NULL){
					free(charpos);
					charpos = NULL;
				}
				T->selected_text = str;
			}
			else{
				T->selecZonew = 0;
			}
		}
	}
	ifleftclickup(){
		if(mouseInTextField){
			T->isEditing = true;
			T->held = false;
				
			int pos_x = width/2-getTextWidth(style.font,textsize,T->text)/2 + T->x;

			if(AS <= pos_x){
				T->cursorpos = 0;
				println("before");
			}
			else{
				if(AS > pos_x + getTextWidth(style.font,textsize,T->text)){
					T->cursorpos = strlen(T->text);
					println("after %d",T->cursorpos);
				}
				else{
					int* charpos = getCharPosition(style.font,textsize,T->text);
					int firstcharindex = -1;
					for(int i=0;i<strlen(T->text)-1;i++){
						if(AS > pos_x + charpos[i] && AS < pos_x + charpos[i+1]){
							firstcharindex = i;
							break;
						}
					}
					if(firstcharindex != -1){//gerer selection last char
						T->cursorpos = firstcharindex;
					}
					else{
						T->cursorpos = strlen(T->text);
					}
					println("between %d",T->cursorpos);
					free(charpos);
					charpos = NULL;
				}
			}
		}

		else{
			T->isEditing = false;
			T->selecZonew = 0;
		}
	}
	if(T->isEditing){
		double t1 = getTimeVal();
		int delta1 = ((double)T->t0 - t0)*2.5;
		int delta2 = ((double)t1 - t0)*2.5;
		if(delta1 != delta2){
			T->blink = !T->blink;
		}
		T->t0 = t1;
		int len = strlen(T->text);
		if(controlPressed()){
			ifkeyup('c' - 'a' + 1){	//ctrl c
				if(T->selected_text != NULL && T->selected_text[0] != '\0'){
					//println("ctrl c %s",T->selected_text);
					setClipboard(T->selected_text);
				}
			}
			ifkeyup('v' - 'a' + 1){	//ctrl v
				char* paste = getClipboard();
				//println("ctrl v %s",paste);
					
				T->text = realloc(T->text,sizeof(char)*(len+1+strlen(paste)));
				strcat(T->text,paste);
				updateTextSizeTextfield(T);
				T->cursorpos = strlen(T->text);

				free(paste);
				paste = NULL;
			}
		}
		else{
			ifkeyspecial(KEY_LEFT){
				if(T->cursorpos > 0){
					T->cursorpos--;
				}
			}
			ifkeyspecial(KEY_RIGHT){
				if(T->cursorpos < len){
					T->cursorpos++;
				}
			}
			for(int i=0;i<255;i++){
				ifkeyup(i){
					T->selecZonew = 0;
					switch(i){
						case 13:
							T->isEditing = false;
						break;
						case 8:
							if(strlen(T->text) > 0){
								T->text = realloc(T->text,sizeof(char)*len);

								if(!(T->cursorpos == 0 && len != 0)){
									if(T->cursorpos == len){
										T->text[len-1] = '\0';
										T->cursorpos = strlen(T->text);
									}
									else{
										char* part1 = malloc(sizeof(char)*(T->cursorpos));
										char* part2 = malloc(sizeof(char)*(len-T->cursorpos+1));
										for(int i=0;i<T->cursorpos-1;i++){
											part1[i] = T->text[i];
										}
										part1[T->cursorpos-1] = '\0';
										for(int i=T->cursorpos;i<strlen(T->text);i++){
											part2[i-T->cursorpos] = T->text[i];
										}
										part2[strlen(T->text)-T->cursorpos] = '\0';
										sprintf(T->text,"%s%s",part1,part2);
										free(part1);
										free(part2);
										T->cursorpos--;
									}
								}

								updateTextSizeTextfield(T);
								keyboardState[8] = false;
							}
						break;
						default:
							T->text = realloc(T->text,sizeof(char)*(len+2));
							if(T->cursorpos == 0 && len != 0){
								char* oldtext = malloc(sizeof(char)*(len+1));
								sprintf(oldtext,"%s",T->text);
								sprintf(T->text,"%c%s",i,oldtext);
								free(oldtext);
								T->cursorpos++;
							}
							else{
								if(T->cursorpos >= len){
									T->text[len] = i;
									T->text[len+1] = '\0';
									T->cursorpos = len+1;
								}
								else{
									char* part1 = malloc(sizeof(char)*(T->cursorpos+1));
									char* part2 = malloc(sizeof(char)*(strlen(T->text)-T->cursorpos+1));
									for(int i=0;i<T->cursorpos;i++){
										part1[i] = T->text[i];
									}
									part1[T->cursorpos] = '\0';
									for(int i=T->cursorpos;i<len;i++){
										part2[i-T->cursorpos] = T->text[i];
									}
									part2[len-T->cursorpos] = '\0';
									sprintf(T->text,"%s%c%s",part1,i,part2);
									free(part1);
									free(part2);
									T->cursorpos++;
								}
							}
							updateTextSizeTextfield(T);
							keyboardState[i] = false;
						break;
					}
				}
			}
		}
	}
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	LABEL
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void showLabel(Label *L,int x,int y);
void updateLabel (Label *L);
void updateTextSizeLabel (Label* L);

Style defaultStyleLabel(){
	Style style;
	for(int i=0;i<NB_FIELDS_CSS;i++){
		style.defaults[i] = true;
	}
	style.font = makeVectorialFont(GLUT_STROKE_ROMAN,12);
	style.width = new_Ratio(300,0);
	style.height = new_Ratio(27,0);
	style.stroke_width = new_Ratio(0,0);
	style.round_factor = 0;
	style.stroke = new_RGBA(NO_STROKE);
	style.fill = new_RGBA(0,0,0,0);
	style.text_color = RGBtoRGBA(COLOR_BLACK,178);
	style.skin = NULL;
	style.text_size = new_Ratio(30*0.7,0);
	return style;
}

Label new_Label(char* text,int width,int height,void* font){
	Label L;
	L.x = 0;
	L.y = 0;
	L.text = str_format(text);
	L.selecZonex = 0; 
	L.selecZonew = 0; 
	L.held = false;
	L.selected_text = NULL;

	Style style = defaultStyleLabel();
	if(font != NULL)
		style.font = font;
	L.style = style;

	L.update = updateLabel;
	L.show = showLabel;

	updateTextSizeLabel(&L);
	return L;
}

Label new_Label_CSS(char* text,char* csspath,char* classname){
	Label L;
	L.x = 0;
	L.y = 0;
	L.text = str_format(text);
	L.selecZonex = 0; 
	L.selecZonew = 0; 
	L.held = false;
	L.selected_text = NULL;

	Style style = addStyles(parseCSS(csspath,classname),defaultStyleLabel());
	L.style = style;

	L.update = updateLabel;
	L.show = showLabel;

	updateTextSizeLabel(&L);
	return L;
}

void updateTextSizeLabel (Label* L){
	if(getFontType(L->style.font) != FONT_TYPE_BITMAP_CHARACTERS){
		Style style = L->style;
		int textsize = ratioValue(style.text_size,0);
		int width = ratioValue(style.width,LF);
		int height = ratioValue(style.height,HF);
		bool cond1 = getTextWidth(style.font,textsize,L->text) > width*0.9;
		bool cond2 = getTextWidth(style.font,textsize,L->text) < width*0.9 && textsize < height*0.7;
		if(cond1 || cond2){
			int i;
			for(i= height*0.7;getTextWidth(style.font,i,L->text) > width*0.9;i--){}
			L->style.text_size.px = i;
			L->style.text_size.prct = 0;
		}
	}
}

void showLabel(Label *L,int x,int y){
	double selfrot = getSelfRotate();
	selfRotate(0);

	L->x = x;
	L->y = y;
	Style style = L->style;
	int width = ratioValue(style.width,LF);
	int height = ratioValue(style.height,HF);
	int textsize = ratioValue(style.text_size,height);
	int strokewidth = ratioValue(style.stroke_width,width);

	if(style.skin == NULL){
		strokeWidth(strokewidth);
		strokeRGBA(style.stroke);
		fillRGBA(style.fill);
		if(style.round_factor <= 0)
			rectangle(x,y,width,height);
		else 
			roundedRectangle(x,y,width,height,style.round_factor,64);
		strokeRGBA(style.text_color);
		int pos_x = x+width/2-getTextWidth(style.font,textsize,L->text)/2;
		int pos_y = y+height/2-textsize/2;
		strokeWidth(2);
		text(style.font,pos_x,pos_y,textsize,L->text);
		fill(50,100,255,70);
		strokeWidth(0);
		rectangle(x+L->selecZonex,y,L->selecZonew,height);
	}
	if(style.skin != NULL){
		//TODO afficher Label avec skin
	}

	selfRotate(selfrot);
}

void updateLabel (Label *L){//TODO Gerer reverse highlight
	Style style = L->style;
	int width = ratioValue(style.width,LF);
	int height = ratioValue(style.height,HF);
	int textsize = ratioValue(style.text_size,height);

	bool mouseInLabel = AS > L->x && AS < L->x+width && OS> L->y && OS < L->y+height;
	if(mouseInLabel){
		setCursor(GLUT_CURSOR_TEXT);
	}
	ifleftclickdown(){
		if(mouseInLabel){
			L->held = true;
			L->selecZonew = 0;
			L->selecZonex = AS - L->x;
		}
	}
	if(L->held){
		L->selecZonew = min(AS-(L->selecZonex + L->x),width - (L->selecZonex));
		if(L->selecZonew < 5){
			L->selecZonew = 0;
			L->selected_text = NULL;
		}
		else{
			if(strlen(L->text) > 0){
				int* charpos = getCharPosition(style.font,textsize,L->text);
					
				int pos_x = width/2-getTextWidth(style.font,textsize,L->text)/2;
				char *str = malloc(sizeof(char)*1000);//TODO CHANGER TAILLE
				int cpt=0;

				int lastcharindex = -1;
				bool firstchar = true;
				int firstcharindex = -1;
				for(int i=0;i<strlen(L->text);i++){
					if(pointIsInRectangle(pos_x+charpos[i],1,L->selecZonex,0,L->selecZonew,2)){
						str[cpt] = L->text[i];
						cpt++;
						lastcharindex = i;
						if(firstchar){
							firstchar = false;
							firstcharindex = i;
						}
					}
				}
				str[cpt] = '\0';
				if(firstcharindex != -1){//gerer selection last char
					int posfirstchar = pos_x + charpos[firstcharindex];
					L->selecZonex = posfirstchar;
				}
				if(lastcharindex != -1){//gerer selection last char
					if(lastcharindex != strlen(L->text)-1)
						lastcharindex++;
					int poslastchar = pos_x + charpos[lastcharindex];
					L->selecZonew = poslastchar - L->selecZonex;
				}

				if(L->selected_text != NULL){
					free(charpos);
					charpos = NULL;
				}
				L->selected_text = str;
			}
			else{
				L->selecZonew = 0;
			}
		}
	}
	ifleftclickup(){
		if(!L->held){
			L->selecZonew = 0;
		}
		L->held = false;
	}
	if(controlPressed()){
		ifkeyup('c' - 'a' + 1){	//ctrl c
			if(L->selected_text != NULL && L->selected_text[0] != '\0'){
				setClipboard(L->selected_text);
			}
		}
	}
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	SLIDER
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void setFloatModeSlider(Slider* s);
void setIntModeSlider(Slider* s);
void afficheSlider(Slider *s,int x,int y);
void onChangeSlider(Slider* s,void (*callback) (void));
void updateSlider (Slider *s);

Style defaultStyleSlider (){
	Style style;
	for(int i=0;i<NB_FIELDS_CSS;i++){
        style.defaults[i] = true;
    }
    style.width = new_Ratio(200,0);
    style.height = new_Ratio(10,0);
    style.font = GLUT_BITMAP_HELVETICA_12;
    style.fill = COLOR_RGBA_WHITE;
    style.stroke = COLOR_RGBA_BLACK;
    style.text_color = COLOR_RGBA_BLACK;
    style.skin = NULL;
    style.round_factor = 0;
    style.stroke_width = new_Ratio(2,0);
    style.text_size = new_Ratio(12,0);
	return style;
}


Slider new_Slider (double min,double max,double default_value){
	Slider s;
	Style style = defaultStyleSlider();
	s.callback = NULL;
	s.float_mode = false;
	s.max = max;
	s.min = min;
	s.value = default_value;
	s.selected = false;
	s.style = style;
	s.x = 0;
	s.y = 0;
	s.setCallback = onChangeSlider;
	s.setIntMode = setIntModeSlider;
	s.setFloatMode = setFloatModeSlider;
	s.show = afficheSlider;
	s.update = updateSlider;
	return s;
}

Slider new_Slider_CSS (double min,double max,double default_value,char* csspath,char* classname){
	Slider s;
	Style style = addStyles(parseCSS(csspath,classname),defaultStyleSlider());
	s.callback = NULL;
	s.float_mode = false;
	s.max = max;
	s.min = min;
	s.value = default_value;
	s.selected = false;
	s.style = style;
	s.x = 0;
	s.y = 0;
	s.setCallback = onChangeSlider;
	s.setIntMode = setIntModeSlider;
	s.setFloatMode = setFloatModeSlider;
	s.show = afficheSlider;
	s.update = updateSlider;
	return s;
}

void setFloatModeSlider(Slider* s){
	s->float_mode = true;
}

void setIntModeSlider(Slider* s){
	s->float_mode = false;
}

void afficheSlider(Slider *s,int x,int y){
	double selfrot = getSelfRotate();
	selfRotate(0);

	int width = ratioValue(s->style.width,LF);
	int height = ratioValue(s->style.height,HF);
	int textsize = ratioValue(s->style.text_size,height);
	int borderwidth = ratioValue(s->style.stroke_width,width);
	FontText* font = s->style.font;
	s->x = x;
	s->y = y;
	fillRGBA(s->style.stroke);
	strokeRGBA(s->style.stroke);
	strokeWidth(0);
	rectangle(x-borderwidth/2,y-height/2,borderwidth,height);
	rectangle(x,y-borderwidth/2,width,borderwidth);
	rectangle(x+width-borderwidth/2,y-height/2,borderwidth,height);
	int	x1 = (int) ((s->value- s->min)*1. / fabs(s->max-s->min)*1.*width) + x - height/2,
		x2 = x1 + height,
		x3 = x1 + height/2,
		y1 = y + height+borderwidth,
		y2 = y + height+borderwidth,
		y3 = y + borderwidth;
	triangle(x1,y1,x2,y2,x3,y3);
	strokeWidth(1);

	char* txt;
	if(s->float_mode){
		txt = str_format("%.2f",s->value);
	}
	else{
		txt = str_format("%d",(int)s->value);
	}
	strokeRGBA(s->style.text_color);
	text(font,x3-getTextWidth(font,textsize,txt)/2,y1+5,textsize,txt);
	betterFree(&txt);

	selfRotate(selfrot);
}

void onChangeSlider(Slider* s,void (*callback) (void)){
	s->callback = callback;
}

void updateSlider (Slider *s){
	int width = ratioValue(s->style.width,LF);
	int height = ratioValue(s->style.height,HF);
	if(mouseState == MOUSE_LEFT_DOWN){
		if(!s->selected){
			if(pointIsInRectangle(AS,OS,s->x,s->y-height/2,width,height)){
				s->selected = true;
			}
		}
		if(s->selected){
			s->value = min(max((AS - s->x),0),width)*1. / width * fabs(s->max-s->min) + s->min;
			if(s->callback != NULL)
				s->callback();
		}
	}
	if(mouseState == MOUSE_LEFT_UP){
		s->selected = false;
	}
}


/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	MOVER
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void activateMover (Mover* M);
void deactivateMover (Mover* M);
void assignToMover (Mover* M,int* x,int* y);
void updateMover (Mover* M);

Mover new_Mover (Point** trajectoire,bool repeat,float tps,bool reverse_loop,bool loop){
	Mover M;
	M.activated = false;
	M.trajectoire = trajectoire;
	M.repeat = repeat;
	M.tps = tps;
	M.reverse_loop = reverse_loop;
	M.reverse = false;
	M.loop = loop;
	M.elapsed_time = 0;
	M.current_pt = 0;

	M.activate = activateMover;
	M.deactivate = deactivateMover;
	M.assign = assignToMover;
	M.update = updateMover;
	return M;
}

void activateMover (Mover* M){
	M->activated = true;
	M->t0 = getTimeVal();
}

void deactivateMover (Mover* M){
	M->activated = false;
	M->t0 = getTimeVal();
}

void assignToMover (Mover* M,int* x,int* y){
	M->x = x;
	M->y = y;
}

void updateMover (Mover* M){
	if(M->activated){
		M->elapsed_time = getTimeVal() - M->t0;
		bool finished_loop = M->elapsed_time >= M->tps;
		if(finished_loop){
			M->elapsed_time = M->tps;
			M->activated = false;
		}
		int index;
		if(M->reverse)
			index = (count(M->trajectoire)-2) * (1 - 1.*(M->elapsed_time/M->tps));
		
		else 
			index = 1.*(M->elapsed_time/M->tps)*(count(M->trajectoire)-2);
		*(M->x) = M->trajectoire[index]->x;
		*(M->y) = M->trajectoire[index]->y;
		if(M->loop && finished_loop){
			activateMover(M);
			if(M->reverse_loop)
				M->reverse = !M->reverse;
		}
	}
}


/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	COUNTER
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

Style defaultStyleCounter(){
	Style style;
	for(int i=0;i<NB_FIELDS_CSS;i++){
        style.defaults[i] = true;
    }
    style.width = new_Ratio(30,0);
    style.height = new_Ratio(50,0);
    style.font = makeVectorialFont(GLUT_STROKE_ROMAN,12);
    style.fill = COLOR_RGBA_WHITE;
    style.stroke = COLOR_RGBA_DARK_GREY;
    style.text_color = COLOR_RGBA_BLACK;
    style.skin = NULL;
    style.round_factor = 0.8;
    style.stroke_width = new_Ratio(2,0);
    style.text_size = new_Ratio(12,0);
	return style;
}

void addEventListenerCounter (Counter* self,void (*func) (void)){
	self->eventListener = func;
}

void afficheCounter (Counter* c,int x,int y){
	double selfrot = getSelfRotate();
	selfRotate(0);

	static FontText* arrowfont = NULL;
	if(arrowfont == NULL)
		arrowfont = makeVectorialFont(GLUT_STROKE_ROMAN,12);
	c->x = x;
	c->y = y;
	int width = ratioValue(c->style.width,LF);
	int height = ratioValue(c->style.height,HF);
	int borderwidth = ratioValue(c->style.stroke_width,width);
	void* font = c->style.font;
	fillRGBA(c->style.fill);
	strokeRGBA(c->style.stroke);
	strokeWidth(ratioValue(c->style.stroke_width,width));
	if(c->style.round_factor == 0){
		rectangle(x,y+height*0.6,width,height*0.4);
		rectangle(x,y,width,height*0.4);
	}
	else{
		roundedRectangle(x+borderwidth/2,y+height*0.6,width-borderwidth,height*0.4,c->style.round_factor,40);
		roundedRectangle(x+borderwidth/2,y,width-borderwidth,height*0.4,c->style.round_factor,40);
	}
	rectangle(x,y+height*0.2,width,height*0.6);
	strokeRGBA(c->style.text_color);
	strokeWidth(4);
	fitTextToRectangle(arrowfont,x,y,width,height*0.2,"v");
	fitTextToRectangle(arrowfont,x,y+height*0.8,width,height*0.2,"^");
	strokeWidth(2);
	fitTextToRectangle(font,x,y+height*0.2,width,height*0.6,str_format("%d",c->value));

	selfRotate(selfrot);
}

void updateCounter (Counter* c){
	int width = ratioValue(c->style.width,LF);
	int height = ratioValue(c->style.height,HF);
	if(handleSouris && mouseState == MOUSE_LEFT_UP){
		if(pointIsInRectangle(AS,OS,c->x,c->y,width,height*0.2)){
			if(c->value - 1 >= c->valmin){
				c->value--;
				if(c->eventListener != NULL){
					c->eventListener();
				}
			}
			handleSouris = false;
		}
		if(pointIsInRectangle(AS,OS,c->x,c->y + height*0.8,width,height*0.2)){
			if(c->value + 1 <= c->valmax){
				c->value++;
				if(c->eventListener != NULL){
					c->eventListener();
				}
			}
			handleSouris = false;
		}
	}
}

Counter new_Counter (int valmin,int val,int valmax){
	Counter ret;
	ret.x=0;
	ret.y = 0;
	ret.value = val;
	ret.valmax = valmax;
	ret.valmin = valmin;
	ret.eventListener = NULL;
	ret.style = defaultStyleCounter();

	ret.show = afficheCounter;
	ret.update = updateCounter;
	ret.addEventListener = addEventListenerCounter;
	return ret;
}

Counter new_Counter_CSS (int valmin,int val,int valmax,char* csspath,char* classname){
	Counter ret;
	ret.x=0;
	ret.y = 0;
	ret.value = val;
	ret.valmax = valmax;
	ret.valmin = valmin;
	ret.eventListener = NULL;
	Style style = addStyles(parseCSS(csspath,classname),defaultStyleCounter());
	ret.style = style;

	ret.show = afficheCounter;
	ret.update = updateCounter;
	ret.addEventListener = addEventListenerCounter;
	return ret;
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	DROPMENU
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

int indexOfOptionDropMenu (DropMenu* DM,char* opt){
	int index = -1;
	for(int i=0;i<count(DM->option);i++){
		if(!strcmp(DM->option[i],opt)){
			index = i;
			break;
		}
	}
	return index;
}

void deleteAllFromDropMenu (DropMenu* DM){
	if(DM->option != NULL){
		for(int i=0;i<count(DM->option);i++){
			DM->option[i]= NULL;
		}
	}
	DM->selected_option = 0;
}

void deleteFromDropMenu (DropMenu* DM,char* opt){
	int index = DM->indexOf(DM,opt);
	if(index == -1){
		printerr("ERROR : could not delete option '%s' from DropMenu : option not found",opt);
	}
	else{
		char** newopt = malloc(sizeof(char*)*(count(DM->option)));
		int lastindex = count(DM->option)-1;
		int cpt = 0;
		for(int i=0;DM->option[i];i++){
			if(i != index){
				newopt[cpt] = DM->option[i];
				cpt++;
			}
		}
		betterFree(&(DM->option));
		newopt[lastindex] = NULL;
		DM->option = newopt;
	}
}

void addToDropMenu (DropMenu* DM,char* opt){
	int nboption = count(DM->option);
	DM->option = realloc(DM->option,sizeof(char*)*(nboption+2));
	DM->option[nboption] = opt;
	DM->option[nboption+1] = NULL;
}

void afficheDropMenu (DropMenu* DM,int x,int y){
	double selfrot = getSelfRotate();
	selfRotate(0);

	Style style = DM->style;
	int width = ratioValue(style.width,LF);
	int height = ratioValue(style.height,HF);
	void* font = style.font;
	DM->x = x;
	DM->y = y;

	fillRGBA(style.fill);
	strokeWidth(ratioValue(style.stroke_width,width));
	strokeRGBA(style.stroke);
	if(style.round_factor == 0){
		rectangle(x,y,x+width,y+height);
	}
	else{
		roundedRectangle(x,y,width,height,style.round_factor,50);
	}
	if(DM->option[0] != NULL){
		strokeRGBA(style.text_color);
		strokeWidth(2);
		fitTextToRectangle(font,x,y+height/10,width,4*height/5,DM->option[DM->index_selected_option]);
		int tsize = 9;
		fillRGBA(style.stroke);
		if(count(DM->option) > 1){
			fillTriangle(
				width + x - 20			, y + height/2 + tsize/2,
				width + x - 20 + tsize/2, y + height/2 - tsize/2,
				width + x - 20 + tsize	, y + height/2 + tsize/2
			);
		}
		if(DM->isOpened){
			int i = 0;
			int nbopt = count(DM->option);
			strokeWidth(ratioValue(style.stroke_width,width));
			fillRGBA(style.fill);
			strokeRGBA(style.stroke);
			roundedRectangle(x,y-height*(nbopt-1)-2,width,height*(nbopt-1),DM->style_hover.round_factor,50);
			strokeRGBA(style.text_color);
			for(int i2=0;i2<nbopt;i2++){
				if(i2 != DM->index_selected_option){
					int x1 = x;
					int y1 = y - height*(i+1);
					strokeRGBA(style.text_color);
					if(i2 == DM->index_option_hovered){
						fillRGBA(DM->style_hover.fill);
						strokeRGBA(DM->style_hover.text_color);
						strokeWidth(ratioValue(DM->style_hover.stroke_width,width));
						roundedRectangle(x1,y1,width,height,DM->style_hover.round_factor,50);
					}
					strokeWidth(2);
					fitTextToRectangle(font,x1,y1+height/10,width,4*height/5,DM->option[i2]);
					i++;
				}
			}
		}
	}

	selfRotate(selfrot);
}

void dropMenuUpdate (DropMenu* DM){
	DM->index_option_hovered = -1;
	bool gereClic = handleSouris && mouseState == MOUSE_LEFT_UP;
	if(DM->isOpened == false){
		if(pointIsInRectangle(AS,OS,DM->x,DM->y,ratioValue(DM->style.width,LF),ratioValue(DM->style.height,HF))){
			//glutSetCursor(GLUT_CURSOR_HELP);
			if(gereClic){
				DM->isOpened = true;
				handleSouris = false;
			}
		}
	}
	else{
		int i = 0;
		int nbopt = count(DM->option);
		for(int i2=0;i2<nbopt;i2++){
			if(i2 != DM->index_selected_option){
				int x = DM->x;
				int width = ratioValue(DM->style.width,LF);
				int y = DM->y - ratioValue(DM->style.height,HF)*(i+1);
				int height = ratioValue(DM->style.height,HF);
				if(pointIsInRectangle(AS,OS,x,y,width,height)){
					DM->index_option_hovered = i2;
					if(gereClic){
						DM->index_selected_option = i2;
						DM->selected_option = DM->option[i2];
						handleSouris = false;
						if(DM->event != NULL){
							DM->event();
						}
						break;
					}
				}
				i++;
			}
		}
		if(gereClic)
			DM->isOpened = false;
	}
}

Style defaultStyleDropMenu (){
	Style style;
	for(int i=0;i<NB_FIELDS_CSS;i++){
		style.defaults[i] = true;
	}
	style.font = makeVectorialFont(GLUT_STROKE_ROMAN,12);
	style.width = new_Ratio(300,0);
	style.height = new_Ratio(27,0);
	style.stroke_width = new_Ratio(2,0);
	style.round_factor = 0.5;
	style.stroke = new_RGBA(154,154,154,255);
	style.fill = COLOR_RGBA_WHITE;
	style.text_color = RGBtoRGBA(COLOR_BLACK,178);
	style.skin = NULL;
	style.text_size = new_Ratio(30*0.7,0);
	return style;
}

Style defaultStyleDropMenuHover (){
	Style style = defaultStyleDropMenu();
	style.stroke_width = new_Ratio(0,0);
	style.fill = new_RGBA(137,206,248,255);
	style.text_color = COLOR_RGBA_WHITE;
	style.round_factor = 0.3;
	return style;
}

DropMenu new_DropMenu (int width,int height){
	DropMenu ret;
	ret.index_option_hovered = -1;
	ret.x=0;
	ret.y=0;

	Style style,style_hover;
	style = defaultStyleDropMenu();
	style_hover = defaultStyleDropMenuHover();
	style_hover.width = style.width = new_Ratio(width,0);
	style_hover.height = style.height = new_Ratio(height,0);

	ret.style = style;
	ret.style_hover = style_hover;
    ret.option = malloc(sizeof(char*));
	ret.option[0] = NULL;
	ret.selected_option = NULL;
	ret.index_selected_option = 0;
	ret.isOpened = false;
	ret.add = addToDropMenu;
	ret.indexOf = indexOfOptionDropMenu;
	ret.delete = deleteFromDropMenu;
	ret.update = dropMenuUpdate;
	ret.show = afficheDropMenu;
	ret.deleteAll = deleteAllFromDropMenu;
	ret.event = NULL;
	return ret;
}

DropMenu new_DropMenuCSS (char* csspath,char* classname){
	DropMenu ret;
	ret.index_option_hovered = -1;
	ret.x=0;
	ret.y=0;

	Style style,style_hover;
	style = addStyles(parseCSS(csspath,classname),defaultStyleDropMenu());
	style_hover = addStyles(addStyles(style,parseCSS(csspath,str_format("%s:hover",classname))),defaultStyleDropMenuHover());

	ret.style = style;
	ret.style_hover = style_hover;
    ret.option = malloc(sizeof(char*));
	ret.option[0] = NULL;
	ret.selected_option = NULL;
	ret.index_selected_option = 0;
	ret.isOpened = false;
	ret.add = addToDropMenu;
	ret.indexOf = indexOfOptionDropMenu;
	ret.delete = deleteFromDropMenu;
	ret.update = dropMenuUpdate;
	ret.show = afficheDropMenu;
	ret.deleteAll = deleteAllFromDropMenu;
	ret.event = NULL;
	return ret;
}

// OVERLAY

Style defaultStyleOverlay(){
	Style style;
	for(int i=0;i<NB_FIELDS_CSS;i++){
		style.defaults[i] = true;
	}
	style.font = new_FontText("/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf");
	style.width = new_Ratio(0,0);	//IGNORED
	style.height = new_Ratio(30,0);
	style.stroke_width = new_Ratio(1,0);
	style.round_factor = 0;			//IGNORED
	style.stroke = new_RGBA(0,0,0,255);
	style.fill = COLOR_RGBA_DIM_GREY;
	style.text_color = new_RGBA(200,200,200,255);
	style.skin = NULL;				//IGNORED
	style.text_size = new_Ratio(10,0);
	return style;
}

void OverlayActivate (Overlay* overlay);
void displayOverlay (Overlay* overlay);
void updateOverlay (Overlay* overlay);

Overlay new_Overlay(char* imagePathIcon){
	Overlay overlay;
	overlay.icon = new_Image(imagePathIcon,true);
	overlay.style = defaultStyleOverlay();
	overlay.movewindow = false;
	overlay.activated = false;

	overlay.activate = OverlayActivate;
	overlay.show = displayOverlay;
	overlay.update = updateOverlay;
	return overlay;
}

Overlay new_Overlay_CSS(char* imagePathIcon,char* csspath,char* classname){
	Overlay overlay;
	overlay.icon = new_Image(imagePathIcon,true);
	overlay.style = addStyles(parseCSS(csspath,classname),defaultStyleOverlay());
	overlay.movewindow = false;
	overlay.activated = false;

	overlay.activate = OverlayActivate;
	overlay.show = displayOverlay;
	overlay.update = updateOverlay;
	return overlay;
}

void OverlayActivate (Overlay* ov){
	ov->activated = true;
	int height = ratioValue(ov->style.height,HF);
	setWindowSize(LF,HF+height);
	setVerticalOffset(height);
	removeOverlayOS();
}

void displayOverlay (Overlay* overlay){
	if(overlay->activated){
		Style style = overlay->style;
		int height = ratioValue(style.height,HF);
		int borderWidth = ratioValue(style.stroke_width,LF);
		int fontsize = ratioValue(style.text_size,height);

		selfRotate(0);
		fillRGBA(style.fill);
		fillRectangle(0,HF,LF,height);
		strokeRGBA(style.text_color);
		text(style.font,LF/2-getTextWidth(style.font,fontsize,getWindowName())/2,HF+ (height/2-fontsize/2),fontsize,getWindowName());
		strokeWidth(borderWidth);

		fillRGBA(style.stroke);
		vertexLine(0,0,LF,0);
		vertexLine(LF,0,LF,HF+height);
		vertexLine(LF,HF+30,0,HF+height);
		vertexLine(0,HF,LF,HF);
		vertexLine(0,HF+height,0,0);

		int w = 7;
		int tw = 13;
		fillRGBA(new_RGBA(40,40,40,127));
		strokeRGBA(new_RGBA(80,80,80,127));

		triangle(LF-w-tw,w,LF-w,w+tw,LF-w,w);

		strokeWidth(1);
		w = 10;
		strokeRGBA(style.text_color);
		cross(LF-10-10,HF+height/2-5,10,10);

		fill(0,0,0,0);
		rectangle(LF-40,HF+height/2-5,10,10);

		line(LF-60,HF+height/2,LF-50,HF+height/2);

		imageCustom(overlay->icon,5,HF+height/6,2*height/3,2*height/3);
	}
}

void updateOverlay (Overlay* overlay){
	if(overlay->activated){
		Style style = overlay->style;
		int height = ratioValue(style.height,HF);

		//MOVE
		static bool movewin = false;
		static int offsetx,offsety;
		static int oldwinx=0,oldwiny=0;
		ifleftclickdown(){
			if(pointIsInRectangle(AS,OS,0,HF,LF-65,height)){
				movewin = true;
				offsetx = AS;
				offsety = OS;
			}
		}

		ifleftclickup(){
			movewin = false;
		}

		if(movewin){
			int winx = XF+AS-offsetx,
				winy = YF-OS+offsety;
			if(winx != oldwinx || winy != oldwiny){
				setWindowPosition(winx,winy);
				sfTime sftime;
				sftime.microseconds = 10000;
				sfSleep(sftime);
			}
			oldwinx = winx;
			oldwiny = winy;
		}

		overlay->movewindow = movewin;

		//reshape
		int w = 7;
		int tw = 13;
		static bool sizewin = false;
		static int offsetx2,offsety2;
		ifleftclickdown(){
			if(pointIsInRectangle(AS,OS,LF-w-tw,0,LF,w+tw)){
				sizewin = true;
				offsetx2 = LF - AS;
				offsety2 = OS;
			}
		}

		ifleftclickup(){
			sizewin = false;
		}

		if(sizewin){
			int minsize = 60;
			int winx = max(minsize,AS + offsetx2),
				winy = max(minsize,(HF+height)-OS + offsety2);

			setWindowSize(winx,winy);
			sfTime sftime;
			sftime.microseconds = 10000;
			sfSleep(sftime);
		}

		ifleftclickup(){

			if(pointIsInRectangle(AS,OS,LF-25,HF,25,height)){
				endProcess();
			}

			if(pointIsInRectangle(AS,OS,LF-45,HF,20,height)){
				fullscreen();
			}

			if(pointIsInRectangle(AS,OS,LF-65,HF,20,height)){
				minimize();
			}

		}
	}
}