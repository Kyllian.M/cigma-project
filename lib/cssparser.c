#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "../include/cigma.h"

#include "../include/KToolsLib.h"
#include "../include/consoletools.h"
#include "../include/cssparser.h"

//TODO Gerer hover comme css

#define CSS_PROP_UNHANDLED          -1  // css property unhandled by Cwing
#define CSS_PROP_COLOR              0   // text_color
#define CSS_PROP_BACKGROUND_COLOR   1   // fill
#define CSS_PROP_WIDTH              2   // width
#define CSS_PROP_HEIGHT             3   // height
#define CSS_PROP_FONT_FAMILY        4   // font
#define CSS_PROP_BORDER_COLOR       5   // stroke
#define CSS_PROP_BACKGROUND_IMAGE   6   // skin
#define CSS_PROP_BORDER_RADIUS      7   // round_factor
#define CSS_PROP_BORDER_WIDTH       8   // stroke_width
#define CSS_PROP_FONT_SIZE          9   // text_size

#define COLOR_FORMAT_HEX            0
#define COLOR_FORMAT_RGB            1
#define COLOR_FORMAT_RGBA           2
#define COLOR_FORMAT_TRANSPARENT    3
#define COLOR_FORMAT_OTHER          4

Ratio new_Ratio (int px,float percent){
    Ratio r;
    r.px = px;
    r.prct = percent;
    return r;
}

int ratioValue (Ratio r,int max){
    return r.px + (r.prct/100)*max;
}

Ratio parseRatio (char* value){
    Ratio r;
    r.px = 0;
    r.prct = 0;
    for(int i=0;i<strlen(value);i++){
        if(value[i] == 'p' && (i+1) < strlen(value) && value[i+1] == 'x'){
            int px = 0;
            int fact = 1;
            for(int i2 = i-1;i2 >= 0 && ( value[i2] == '-' || value[i2] == '.' || (value[i2] >= '0' && value[i2] <= '9') );i2--){
                if(value[i2] == '.'){
                    px = px > 0.5 ? 1 : 0;
                    fact = 1;
                }
                else{
                    if(value[i2] == '-'){
                        px = -px;
                    }
                    else{
                        px += (value[i2]-'0')*fact;
                        fact = fact*10;
                    }
                }
            }
            r.px += px;
        }
        if(value[i] == '%'){
            float prct = 0;
            int fact = 1;
            for(int i2 = i-1;i2 >= 0 && ( value[i2] == '-' || value[i2] == '.' || (value[i2] >= '0' && value[i2] <= '9') );i2--){
                if(value[i2] == '.'){
                    prct = prct*1./(fact);
                    fact = 1;
                }
                else{
                    if(value[i2] == '-'){
                        prct = -prct;
                    }
                    else{
                        prct += (value[i2]-'0')*fact;
                        fact = fact*10;
                    }
                }
            }
            r.prct += prct;
        }
    }
    return r;
}

RGBA parseCssColor(char* color);

void setCssColor (Style* style,char* value){
    style->text_color = parseCssColor(value);
}

void setCssBackgroudColor (Style* style,char* value){
    style->fill = parseCssColor(value);
}

void setCssWidth (Style* style,char* value){
    style->width = parseRatio(value);
}

void setCssHeight (Style* style,char* value){
    style->height = parseRatio(value);
}

void setCssFontFamily (Style* style,char* val){
    char* value = str_removeAll(val,'\'');
    bool selected = false;
    if(str_same(value,"GLUT_BITMAP_8_BY_13"       )){style->font = GLUT_BITMAP_8_BY_13;         selected = true;}  
	if(str_same(value,"GLUT_BITMAP_9_BY_15"       )){style->font = GLUT_BITMAP_9_BY_15;         selected = true;}  
	if(str_same(value,"GLUT_BITMAP_HELVETICA_10"  )){style->font = GLUT_BITMAP_HELVETICA_10;    selected = true;}
	if(str_same(value,"GLUT_BITMAP_HELVETICA_12"  )){style->font = GLUT_BITMAP_HELVETICA_12;    selected = true;}
	if(str_same(value,"GLUT_BITMAP_HELVETICA_18"  )){style->font = GLUT_BITMAP_HELVETICA_18;    selected = true;}
	if(str_same(value,"GLUT_BITMAP_TIMES_ROMAN_10")){style->font = GLUT_BITMAP_TIMES_ROMAN_10;  selected = true;}
	if(str_same(value,"GLUT_BITMAP_TIMES_ROMAN_24")){style->font = GLUT_BITMAP_TIMES_ROMAN_24;  selected = true;}

    if(str_same(value,"GLUT_STROKE_MONO_ROMAN"  )){style->font = GLUT_STROKE_MONO_ROMAN;    selected = true;}
	if(str_same(value,"GLUT_STROKE_ROMAN"       )){style->font = GLUT_STROKE_ROMAN;         selected = true;}

    if(str_same(value,"8-by-13"       )){style->font = GLUT_BITMAP_8_BY_13;         selected = true;}  
	if(str_same(value,"9-by-15"       )){style->font = GLUT_BITMAP_9_BY_15;         selected = true;}  
	if(str_same(value,"helvetica-10"  )){style->font = GLUT_BITMAP_HELVETICA_10;    selected = true;}
	if(str_same(value,"helvetica-12"  )){style->font = GLUT_BITMAP_HELVETICA_12;    selected = true;}
	if(str_same(value,"helvetica-18"  )){style->font = GLUT_BITMAP_HELVETICA_18;    selected = true;}
	if(str_same(value,"times-roman-10")){style->font = GLUT_BITMAP_TIMES_ROMAN_10;  selected = true;}
	if(str_same(value,"times-roman-24")){style->font = GLUT_BITMAP_TIMES_ROMAN_24;  selected = true;}

    if(str_same(value,"mono-roman"      )){style->font = makeVectorialFont(GLUT_STROKE_MONO_ROMAN,12);    selected = true;}  
	if(str_same(value,"roman"           )){style->font = makeVectorialFont(GLUT_STROKE_ROMAN,12);         selected = true;} 

    if(!selected){
        style->font = new_FontText(value);
    }
}

void setCssBorderColor (Style* style,char* value){
    style->stroke = parseCssColor(value);
}

void setCssBackgroundImage (Style* style,char* val){
    char* value = str_removeAll(val,'\'');
    style->skin = new_Image(value,true);
}

void setCssBorderRadius (Style* style,char* value){
    style->round_factor = (parseRatio(value).prct)/100;
}

void setCssBorderWidth (Style* style,char* value){
    style->stroke_width = parseRatio(value);
}

void setCssFontSize (Style* style,char* value){
    style->text_size = parseRatio(value);
}


int getCSSprop (char* propname){
    if(str_same(propname,"color")){
        return CSS_PROP_COLOR;
    }
    if(str_same(propname,"background-color")){
        return CSS_PROP_BACKGROUND_COLOR;
    }
    if(str_same(propname,"width")){
        return CSS_PROP_WIDTH;
    }
    if(str_same(propname,"height")){
        return CSS_PROP_HEIGHT;
    }
    if(str_same(propname,"font-family")){
        return CSS_PROP_FONT_FAMILY;
    }
    if(str_same(propname,"border-color")){
        return CSS_PROP_BORDER_COLOR;
    }
    if(str_same(propname,"background-image")){
        return CSS_PROP_BACKGROUND_IMAGE;
    }
    if(str_same(propname,"border-radius")){
        return CSS_PROP_BORDER_RADIUS;
    }
    if(str_same(propname,"font-size")){
        return CSS_PROP_FONT_SIZE;
    }
    if(str_same(propname,"border-width")){
        return CSS_PROP_BORDER_WIDTH;
    }
    return CSS_PROP_UNHANDLED;
}

/*
Ratio width,height;
	void* font;
	RGBA fill;
	RGBA stroke;
	RGBA hover_color;
	RGBA text_color;
	Image* skin;
	float round_factor;
	Ratio stroke_width;
	Ratio text_size;
*/

Style new_Style_Default(){
    Style style;
    for(int i=0;i<NB_FIELDS_CSS;i++){
        style.defaults[i] = true;
    }
    Ratio ratio0 = new_Ratio(20,0);
    style.width = ratio0;
    style.height = ratio0;
    style.font = GLUT_BITMAP_HELVETICA_12;
    style.fill = COLOR_RGBA_WHITE;
    style.stroke = COLOR_RGBA_DARK_GREY;
    style.text_color = COLOR_RGBA_BLACK;
    style.skin = NULL;
    style.round_factor = 0.15;
    style.stroke_width = new_Ratio(2,0);
    style.text_size = new_Ratio(12,0);
    return style;
}

Style addStyles(Style s1,Style s2){
    Style s = s1;
    if(!s2.defaults[CSS_PROP_WIDTH]             || s1.defaults[CSS_PROP_WIDTH]              ){
        s.width = s2.width;
        s.defaults[CSS_PROP_WIDTH] = s2.defaults[CSS_PROP_WIDTH];
    }
    if(!s2.defaults[CSS_PROP_HEIGHT]            || s1.defaults[CSS_PROP_HEIGHT]             ){
        s.height = s2.height;
        s.defaults[CSS_PROP_HEIGHT] = s2.defaults[CSS_PROP_HEIGHT];
    }
    if(!s2.defaults[CSS_PROP_FONT_FAMILY]       || s1.defaults[CSS_PROP_FONT_FAMILY]        ){
        s.font = s2.font;
        s.defaults[CSS_PROP_FONT_FAMILY] = s2.defaults[CSS_PROP_FONT_FAMILY];
    }
    if(!s2.defaults[CSS_PROP_BACKGROUND_COLOR]  || s1.defaults[CSS_PROP_BACKGROUND_COLOR]   ){
        s.fill = s2.fill;
        s.defaults[CSS_PROP_BACKGROUND_COLOR] = s2.defaults[CSS_PROP_BACKGROUND_COLOR];
    }
    if(!s2.defaults[CSS_PROP_BORDER_COLOR]      || s1.defaults[CSS_PROP_BORDER_COLOR]       ){
        s.stroke = s2.stroke;
        s.defaults[CSS_PROP_BORDER_COLOR] = s2.defaults[CSS_PROP_BORDER_COLOR];
    }
    if(!s2.defaults[CSS_PROP_COLOR]             || s1.defaults[CSS_PROP_COLOR]              ){
        s.text_color = s2.text_color;
        s.defaults[CSS_PROP_COLOR] = s2.defaults[CSS_PROP_COLOR];
    }
    if(!s2.defaults[CSS_PROP_BACKGROUND_IMAGE]  || s1.defaults[CSS_PROP_BACKGROUND_IMAGE]   ){
        s.skin = s2.skin;
        s.defaults[CSS_PROP_BACKGROUND_IMAGE] = s2.defaults[CSS_PROP_BACKGROUND_IMAGE];
    }
    if(!s2.defaults[CSS_PROP_BORDER_RADIUS]     || s1.defaults[CSS_PROP_BORDER_RADIUS]      ){
        s.round_factor = s2.round_factor;
        s.defaults[CSS_PROP_BORDER_RADIUS] = s2.defaults[CSS_PROP_BORDER_RADIUS];
    }
    if(!s2.defaults[CSS_PROP_BORDER_WIDTH]      || s1.defaults[CSS_PROP_BORDER_WIDTH]       ){
        s.stroke_width = s2.stroke_width;
        s.defaults[CSS_PROP_BORDER_WIDTH] = s2.defaults[CSS_PROP_BORDER_WIDTH];
    }
    if(!s2.defaults[CSS_PROP_FONT_SIZE]         || s1.defaults[CSS_PROP_FONT_SIZE]          ){
        s.text_size = s2.text_size;
        s.defaults[CSS_PROP_FONT_SIZE]  = s2.defaults[CSS_PROP_FONT_SIZE];
    }
    return s;
}

Style parseCSS (char* path,char* class){
    Style style = new_Style_Default();
    for(int i=0;i<NB_FIELDS_CSS;i++){
        style.defaults[i] = true;
    }

    char* fcontents = fileGetText(path);

    if(fcontents == NULL){
        printerr("CSS PARSER ERROR @parseCSS() : could not open file %s\n",path);
        return style;
    }
    //println("'''%s'''",fcontents);

    char prefixes[] = {'.','#'};
    char suffixes[] = {'{',' ','\t','\n'}; 
    int classindex = -1;
    char* search = malloc(sizeof(char)*(strlen(class)+3));
    for(int i=0;classindex == -1 && i < 8;i++){
        sprintf(search,"%c%s%c",prefixes[i%2],class,suffixes[i%4]);
        classindex = str_substringIndex(fcontents,search);
    }
    free(search);

    if(classindex == -1){
        printerr("CSS PARSER ERROR @parseCSS() : could not find class '%s' in file %s\n",class,path);
        return style;
    }
    classindex++;

    for(int i=classindex;i<strlen(fcontents);i++){
        if(fcontents[i] == '{'){
            fcontents[i] = '~';
        }
        if(fcontents[i] == '}'){
            fcontents[i] = '~';
            break;
        }
    }
    char** temp = str_split(fcontents,'~');
    char* classcontents = temp[1];
    //println("'''%s'''",classcontents);
    free(temp[0]);
    if(temp[2] != NULL){
        free(temp[2]);
    }
    free(temp);
    free(fcontents);

    char* noline1 = str_removeAll(classcontents,'\r');
    free(classcontents);
    //println("'''%s'''",noline1);
    char* noline2 = str_removeAll(noline1,'\n');
    free(noline1);
    //println("'''%s'''",noline2);
    char* nospace = str_removeAll(noline2,' ');
    free(noline2);
    //println("'''%s'''",nospace);
    char* notab = str_removeAll(nospace,'\t');
    free(nospace);
    //println("'''%s'''",notab);

    char** tab = str_split(notab,';');
    char*** lines = malloc(sizeof(char**)*(count(tab)+1));

    for(int i = 0;i< count(tab);i++){
        lines[i] = str_split(tab[i],':');
    }
    lines[count(tab)] = NULL;

    for(int i=0;i<count(lines);i++){
        char* property = lines[i][0];
        char* value = lines[i][1];
        int prop = getCSSprop(property);
        switch(prop){
            case CSS_PROP_UNHANDLED         : 
                printerr("CSS PARSER ERROR @parseCSS() : Unhandled css property '%s'",property);
            break;
            case CSS_PROP_COLOR             : 
                setCssColor(&style,value);
            break;
            case CSS_PROP_BACKGROUND_COLOR  : 
                setCssBackgroudColor(&style,value);
            break; 
            case CSS_PROP_WIDTH             : 
                setCssWidth(&style,value);
            break; 
            case CSS_PROP_HEIGHT            : 
                setCssHeight(&style,value);
            break; 
            case CSS_PROP_FONT_FAMILY       : 
                setCssFontFamily(&style,value);
            break; 
            case CSS_PROP_BORDER_COLOR      : 
                setCssBorderColor(&style,value);
            break; 
            case CSS_PROP_BACKGROUND_IMAGE  : 
                setCssBackgroundImage(&style,value);
            break; 
            case CSS_PROP_BORDER_RADIUS     : 
                setCssBorderRadius(&style,value);
            break; 
            case CSS_PROP_BORDER_WIDTH      : 
                setCssBorderWidth(&style,value);
            break; 
            case CSS_PROP_FONT_SIZE         : 
                setCssFontSize(&style,value);
            break; 
        }
        style.defaults[prop] = false;
    }


    return style;

}


RGBA parseCssColor(char* color){
    int colorformat = COLOR_FORMAT_OTHER;
    if(color[0] == '#'){
        colorformat = COLOR_FORMAT_HEX;
    }
    if(color[0] == 'r' && color[1] == 'g' && color[2] == 'b' && color[3] != 'a'){
        colorformat = COLOR_FORMAT_RGB;
    }
    if(color[0] == 'r' && color[1] == 'g' && color[2] == 'b' && color[3] == 'a'){
        colorformat = COLOR_FORMAT_RGBA;
    }
    if(str_same(color,"transparent")){
        colorformat = COLOR_FORMAT_TRANSPARENT;
    }

    switch(colorformat){
        case COLOR_FORMAT_TRANSPARENT:;
            return new_RGBA(0,0,0,0);
        break;
        case COLOR_FORMAT_HEX:;
            int r,g,b;
            char chr[3];
            chr[0] = color[1];
            chr[1] = color[2];
            chr[2] = '\0';
            r = strtol(chr,NULL,16);
            chr[0] = color[3];
            chr[1] = color[4];
            chr[2] = '\0';
            g = strtol(chr,NULL,16);
            chr[0] = color[5];
            chr[1] = color[6];
            chr[2] = '\0';
            b = strtol(chr,NULL,16);
            return new_RGBA(r,g,b,255);
        break;
        case COLOR_FORMAT_RGB:;
            char cleanstr[12];
            for(int i=4;color[i] != ')';i++){
                cleanstr[i-4] = color[i];
                cleanstr[i-3] = '\0';
            }
            char** rgb = str_split(cleanstr,',');
            int red = atoi(rgb[0]);
            int green = atoi(rgb[1]);
            int blue = atoi(rgb[2]);
            return new_RGBA(red,green,blue,255);
        break;
        case COLOR_FORMAT_RGBA:;
            char cleanstr2[100];
            for(int i=5;color[i] != ')';i++){
                cleanstr2[i-5] = color[i];
                cleanstr2[i-4] = '\0';
            }
            char** rgba = str_split(cleanstr2,',');
            int re = atoi(rgba[0]);
            int gr = atoi(rgba[1]);
            int bl = atoi(rgba[2]);
            float a= atof(rgba[3]);
            return new_RGBA(re,gr,bl,a*255);
        break;
        case COLOR_FORMAT_OTHER:;
            //un grand merci a google sheets, txtformat.com et la commande ctrl alt down de Vscode pour m'avoir économisé 9 heure de frappe sur le clavier
            if(str_same(color,"indianred"               )){return	COLOR_RGBA_INDIAN_RED;}
            if(str_same(color,"lightcoral"              )){return	COLOR_RGBA_LIGHT_CORAL;}
            if(str_same(color,"salmon"                  )){return	COLOR_RGBA_SALMON;}
            if(str_same(color,"darksalmon"              )){return	COLOR_RGBA_DARK_SALMON;}
            if(str_same(color,"lightsalmon"             )){return	COLOR_RGBA_LIGHT_SALMON;}
            if(str_same(color,"crimson"                 )){return	COLOR_RGBA_CRIMSON;}
            if(str_same(color,"red"                     )){return	COLOR_RGBA_RED;}
            if(str_same(color,"firebrick"               )){return	COLOR_RGBA_FIRE_BRICK;}
            if(str_same(color,"darkred"                 )){return	COLOR_RGBA_DARK_RED;}
            if(str_same(color,"pink"                    )){return	COLOR_RGBA_PINK;}
            if(str_same(color,"lightpink"               )){return	COLOR_RGBA_LIGHT_PINK;}
            if(str_same(color,"hotpink"                 )){return	COLOR_RGBA_HOT_PINK;}
            if(str_same(color,"deeppink"                )){return	COLOR_RGBA_DEEP_PINK;}
            if(str_same(color,"mediumvioletred"         )){return	COLOR_RGBA_MEDIUM_VIOLET_RED;}
            if(str_same(color,"palevioletred"           )){return	COLOR_RGBA_PALE_VIOLET_RED;}
            if(str_same(color,"coral"                   )){return	COLOR_RGBA_CORAL;}
            if(str_same(color,"tomato"                  )){return	COLOR_RGBA_TOMATO;}
            if(str_same(color,"orangered"               )){return	COLOR_RGBA_ORANGE_RED;}
            if(str_same(color,"darkorange"              )){return	COLOR_RGBA_DARK_ORANGE;}
            if(str_same(color,"orange"                  )){return	COLOR_RGBA_ORANGE;}
            if(str_same(color,"gold"                    )){return	COLOR_RGBA_GOLD;}
            if(str_same(color,"yellow"                  )){return	COLOR_RGBA_YELLOW;}
            if(str_same(color,"lightyellow"             )){return	COLOR_RGBA_LIGHT_YELLOW;}
            if(str_same(color,"lemonchiffon"            )){return	COLOR_RGBA_LEMON_CHIFFON;}
            if(str_same(color,"lightgoldenrodyellow"    )){return	COLOR_RGBA_LIGHT_GOLDEN_ROD_YELLOW;}
            if(str_same(color,"papayawhip"              )){return	COLOR_RGBA_PAPAYAWHIP;}
            if(str_same(color,"moccasin"                )){return	COLOR_RGBA_MOCCASIN;}
            if(str_same(color,"peachpuff"               )){return	COLOR_RGBA_PEACHPUFF;}
            if(str_same(color,"palegoldenrod"           )){return	COLOR_RGBA_PALE_GOLDEN_ROD;}
            if(str_same(color,"khaki"                   )){return	COLOR_RGBA_KHAKI;}
            if(str_same(color,"darkkhaki"               )){return	COLOR_RGBA_DARK_KHAKI;}
            if(str_same(color,"lavender"                )){return	COLOR_RGBA_LAVENDER;}
            if(str_same(color,"thistle"                 )){return	COLOR_RGBA_THISTLE;}
            if(str_same(color,"plum"                    )){return	COLOR_RGBA_PLUM;}
            if(str_same(color,"violet"                  )){return	COLOR_RGBA_VIOLET;}
            if(str_same(color,"orchid"                  )){return	COLOR_RGBA_ORCHID;}
            if(str_same(color,"fuchsia"                 )){return	COLOR_RGBA_FUCHSIA;}
            if(str_same(color,"magenta"                 )){return	COLOR_RGBA_MAGENTA;}
            if(str_same(color,"mediumorchid"            )){return	COLOR_RGBA_MEDIUM_ORCHID;}
            if(str_same(color,"mediumpurple"            )){return	COLOR_RGBA_MEDIUM_PURPLE;}
            if(str_same(color,"blueviolet"              )){return	COLOR_RGBA_BLUE_VIOLET;}
            if(str_same(color,"darkviolet"              )){return	COLOR_RGBA_DARK_VIOLET;}
            if(str_same(color,"darkorchid"              )){return	COLOR_RGBA_DARK_ORCHID;}
            if(str_same(color,"darkmagenta"             )){return	COLOR_RGBA_DARK_MAGENTA;}
            if(str_same(color,"purple"                  )){return	COLOR_RGBA_PURPLE;}
            if(str_same(color,"rebeccapurple"           )){return	COLOR_RGBA_REBECCA_PURPLE;}
            if(str_same(color,"indigo"                  )){return	COLOR_RGBA_INDIGO;}
            if(str_same(color,"mediumslateblue"         )){return	COLOR_RGBA_MEDIUM_SLATE_BLUE;}
            if(str_same(color,"slateblue"               )){return	COLOR_RGBA_SLATE_BLUE;}
            if(str_same(color,"darkslateblue"           )){return	COLOR_RGBA_DARK_SLATE_BLUE;}
            if(str_same(color,"greenyellow"             )){return	COLOR_RGBA_GREEN_YELLOW;}
            if(str_same(color,"chartreuse"              )){return	COLOR_RGBA_CHARTREUSE;}
            if(str_same(color,"lawngreen"               )){return	COLOR_RGBA_LAWN_GREEN;}
            if(str_same(color,"lime"                    )){return	COLOR_RGBA_LIME;}
            if(str_same(color,"limegreen"               )){return	COLOR_RGBA_LIME_GREEN;}
            if(str_same(color,"palegreen"               )){return	COLOR_RGBA_PALE_GREEN;}
            if(str_same(color,"lightgreen"              )){return	COLOR_RGBA_LIGHT_GREEN;}
            if(str_same(color,"mediumspringgreen"       )){return	COLOR_RGBA_MEDIUM_SPRING_GREEN;}
            if(str_same(color,"springgreen"             )){return	COLOR_RGBA_SPRING_GREEN;}
            if(str_same(color,"mediumseagreen"          )){return	COLOR_RGBA_MEDIUM_SEA_GREEN;}
            if(str_same(color,"seagreen"                )){return	COLOR_RGBA_SEA_GREEN;}
            if(str_same(color,"forestgreen"             )){return	COLOR_RGBA_FOREST_GREEN;}
            if(str_same(color,"green"                   )){return	COLOR_RGBA_GREEN;}
            if(str_same(color,"darkgreen"               )){return	COLOR_RGBA_DARK_GREEN;}
            if(str_same(color,"yellowgreen"             )){return	COLOR_RGBA_YELLOW_GREEN;}
            if(str_same(color,"olivedrab"               )){return	COLOR_RGBA_OLIVE_DRAB;}
            if(str_same(color,"olive"                   )){return	COLOR_RGBA_OLIVE;}
            if(str_same(color,"darkolivegreen"          )){return	COLOR_RGBA_DARK_OLIVE_GREEN;}
            if(str_same(color,"mediumaquamarine"        )){return	COLOR_RGBA_MEDIUM_AQUA_MARINE;}
            if(str_same(color,"darkseagreen"            )){return	COLOR_RGBA_DARK_SEA_GREEN;}
            if(str_same(color,"lightseagreen"           )){return	COLOR_RGBA_LIGHT_SEA_GREEN;}
            if(str_same(color,"darkcyan"                )){return	COLOR_RGBA_DARK_CYAN;}
            if(str_same(color,"teal"                    )){return	COLOR_RGBA_TEAL;}
            if(str_same(color,"aqua"                    )){return	COLOR_RGBA_AQUA;}
            if(str_same(color,"cyan"                    )){return	COLOR_RGBA_CYAN;}
            if(str_same(color,"lightcyan"               )){return	COLOR_RGBA_LIGHT_CYAN;}
            if(str_same(color,"paleturquoise"           )){return	COLOR_RGBA_PALE_TURQUOISE;}
            if(str_same(color,"aquamarine"              )){return	COLOR_RGBA_AQUAMARINE;}
            if(str_same(color,"turquoise"               )){return	COLOR_RGBA_TURQUOISE;}
            if(str_same(color,"mediumturquoise"         )){return	COLOR_RGBA_MEDIUM_TURQUOISE;}
            if(str_same(color,"darkturquoise"           )){return	COLOR_RGBA_DARK_TURQUOISE;}
            if(str_same(color,"cadetblue"               )){return	COLOR_RGBA_CADET_BLUE;}
            if(str_same(color,"steelblue"               )){return	COLOR_RGBA_STEEL_BLUE;}
            if(str_same(color,"lightsteelblue"          )){return	COLOR_RGBA_LIGHT_STEEL_BLUE;}
            if(str_same(color,"powderblue"              )){return	COLOR_RGBA_POWDER_BLUE;}
            if(str_same(color,"lightblue"               )){return	COLOR_RGBA_LIGHT_BLUE;}
            if(str_same(color,"skyblue"                 )){return	COLOR_RGBA_SKYB_LUE;}
            if(str_same(color,"lightskyblue"            )){return	COLOR_RGBA_LIGHT_SKY_BLUE;}
            if(str_same(color,"deepskyblue"             )){return	COLOR_RGBA_DEEP_SKY_BLUE;}
            if(str_same(color,"dodgerblue"              )){return	COLOR_RGBA_DODGER_BLUE;}
            if(str_same(color,"cornflowerblue"          )){return	COLOR_RGBA_CORN_FLOWER_BLUE;}
            if(str_same(color,"royalblue"               )){return	COLOR_RGBA_ROYAL_BLUE;}
            if(str_same(color,"blue"                    )){return	COLOR_RGBA_BLUE;}
            if(str_same(color,"mediumblue"              )){return	COLOR_RGBA_MEDIUM_BLUE;}
            if(str_same(color,"darkblue"                )){return	COLOR_RGBA_DARK_BLUE;}
            if(str_same(color,"navy"                    )){return	COLOR_RGBA_NAVY;}
            if(str_same(color,"midnightblue"            )){return	COLOR_RGBA_MIDNIGHT_BLUE;}
            if(str_same(color,"cornsilk"                )){return	COLOR_RGBA_CORN_SILK;}
            if(str_same(color,"blanchedalmond"          )){return	COLOR_RGBA_BLANCHE_DALMOND;}
            if(str_same(color,"bisque"                  )){return	COLOR_RGBA_BISQUE;}
            if(str_same(color,"navajowhite"             )){return	COLOR_RGBA_NAVAJO_WHITE;}
            if(str_same(color,"wheat"                   )){return	COLOR_RGBA_WHEAT;}
            if(str_same(color,"burlywood"               )){return	COLOR_RGBA_BURLY_WOOD;}
            if(str_same(color,"tan"                     )){return	COLOR_RGBA_TAN;}
            if(str_same(color,"rosybrown"               )){return	COLOR_RGBA_ROSY_BROWN;}
            if(str_same(color,"sandybrown"              )){return	COLOR_RGBA_SANDY_BROWN;}
            if(str_same(color,"goldenrod"               )){return	COLOR_RGBA_GOLDEN_ROD;}
            if(str_same(color,"darkgoldenrod"           )){return	COLOR_RGBA_DARK_GOLDEN_ROD;}
            if(str_same(color,"peru"                    )){return	COLOR_RGBA_PERU;}
            if(str_same(color,"chocolate"               )){return	COLOR_RGBA_CHOCOLATE;}
            if(str_same(color,"saddlebrown"             )){return	COLOR_RGBA_SADDLE_BROWN;}
            if(str_same(color,"sienna"                  )){return	COLOR_RGBA_SIENNA;}
            if(str_same(color,"brown"                   )){return	COLOR_RGBA_BROWN;}
            if(str_same(color,"maroon"                  )){return	COLOR_RGBA_MAROON;}
            if(str_same(color,"white"                   )){return	COLOR_RGBA_WHITE;}
            if(str_same(color,"snow"                    )){return	COLOR_RGBA_SNOW;}
            if(str_same(color,"honeydew"                )){return	COLOR_RGBA_HONEYDEW;}
            if(str_same(color,"mintcream"               )){return	COLOR_RGBA_MINT_CREAM;}
            if(str_same(color,"azure"                   )){return	COLOR_RGBA_AZURE;}
            if(str_same(color,"aliceblue"               )){return	COLOR_RGBA_ALICE_BLUE;}
            if(str_same(color,"ghostwhite"              )){return	COLOR_RGBA_GHOST_WHITE;}
            if(str_same(color,"whitesmoke"              )){return	COLOR_RGBA_WHITE_SMOKE;}
            if(str_same(color,"seashell"                )){return	COLOR_RGBA_SEA_SHELL;}
            if(str_same(color,"beige"                   )){return	COLOR_RGBA_BEIGE;}
            if(str_same(color,"oldlace"                 )){return	COLOR_RGBA_OLD_LACE;}
            if(str_same(color,"floralwhite"             )){return	COLOR_RGBA_FLORAL_WHITE;}
            if(str_same(color,"ivory"                   )){return	COLOR_RGBA_IVORY;}
            if(str_same(color,"antiquewhite"            )){return	COLOR_RGBA_ANTIQUE_WHITE;}
            if(str_same(color,"linen"                   )){return	COLOR_RGBA_LINEN;}
            if(str_same(color,"lavenderblush"           )){return	COLOR_RGBA_LAVENDER_BLUSH;}
            if(str_same(color,"mistyrose"               )){return	COLOR_RGBA_MISTY_ROSE;}
            if(str_same(color,"gainsboro"               )){return	COLOR_RGBA_GAINSBORO;}
            if(str_same(color,"lightgray"               )){return	COLOR_RGBA_LIGHT_GRAY;}
            if(str_same(color,"lightgrey"               )){return	COLOR_RGBA_LIGHT_GREY;}
            if(str_same(color,"silver"                  )){return	COLOR_RGBA_SILVER;}
            if(str_same(color,"darkgray"                )){return	COLOR_RGBA_DARK_GRAY;}
            if(str_same(color,"darkgrey"                )){return	COLOR_RGBA_DARK_GREY;}
            if(str_same(color,"gray"                    )){return	COLOR_RGBA_GRAY;}
            if(str_same(color,"grey"                    )){return	COLOR_RGBA_GREY;}
            if(str_same(color,"dimgray"                 )){return	COLOR_RGBA_DIM_GRAY;}
            if(str_same(color,"dimgrey"                 )){return	COLOR_RGBA_DIM_GREY;}
            if(str_same(color,"lightslategray"          )){return	COLOR_RGBA_LIGHT_SLATE_GRAY;}
            if(str_same(color,"lightslategrey"          )){return	COLOR_RGBA_LIGHT_SLATE_GREY;}
            if(str_same(color,"slategray"               )){return	COLOR_RGBA_SLATE_GRAY;}
            if(str_same(color,"slategrey"               )){return	COLOR_RGBA_SLATE_GREY;}
            if(str_same(color,"darkslategray"           )){return	COLOR_RGBA_DARK_SLATE_GRAY;}
            if(str_same(color,"darkslategrey"           )){return	COLOR_RGBA_DARK_SLATE_GREY;}
            if(str_same(color,"black"                   )){return	COLOR_RGBA_BLACK;}
        break;
    }
    return new_RGBA(0,0,0,255);
}