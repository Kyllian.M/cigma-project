#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "../include/cigma.h"

#include "../include/KToolsLib.h"
#include "../include/consoletools.h"
#include "../include/geometry.h"

/**
 * @brief cree un tableau de point formant un segment entre deux points
 * 
 * @param xa abscisse du point 1
 * @param ya ordonée du point 1
 * @param xb abscisse du point 2
 * @param yb ordonee du point 2
 * @return Point** tableau de pointeurs sur points terminé par un NULL
 * Kyllian MARIE, 2019
 */
Point** makeLine (int xa, int ya, int xb, int yb){
	Point** p = malloc(sizeof(Point*));
	int cpt = 1;
	float a;
	float c;
	int yi;
	int xi;
	if( xb == xa){
		a = 9999999;
	}
	else{
		a = (yb-ya)/(1.*xb-xa);
	}
	c = ya - a*xa;
	if(a>= -1 || a<=1){
		if(xa<=xb){
			for(xi=xa;xi<xb;xi++){
				yi = a*xi + c;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
		else{
			for(xi=xa;xi>xb;xi--){
				yi = a*xi + c;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
	}
	else if((a<-1. || a>1.) && a != 9999999){
		if(ya<=yb){
			for(yi=ya;yi<yb;yi++){
				xi = (yi-c)*1./a;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
		else{
			for(yi=ya;yi>yb;yi--){
				xi = (yi-c)*1./a;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
	}
	else if(a == 9999999){
		if(ya<=yb){
			for(yi=ya;yi<yb;yi++){
				xi = xa;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
		else{
			for(yi=ya;yi>yb;yi--){
				xi = xa;
				p = realloc(p,sizeof(Point*)*cpt);
				p[cpt-1] = malloc(sizeof(Point));
				p[cpt-1]->x = xi;
				p[cpt-1]->y = yi;
				cpt++;
			}
		}
	}
	p = realloc(p,sizeof(Point*)*cpt);
	p[cpt-1] = NULL;
	return p;
}

Point** Point_cat(Point** p1,Point** p2){
	int size1 = count(p1)-1,
		size2 = count(p2)-1;
	Point** p = calloc((size1 + size2 + 1),sizeof(Point*));
	for(int i = 0; i < size1; i++){
		p[i] = malloc(sizeof(Point));
		*(p[i]) = (Point) {p1[i]->x,p1[i]->y};
	}
	for(int i = size1; i < size1 + size2; i++){
		p[i] = malloc(sizeof(Point));
		*(p[i]) = (Point) {p2[i-size1]->x,p2[i-size1]->y};
	}
	p[size1 + size2] = NULL;
	return p;
}

bool pointIsInCircle (int x,int y,int xc,int yc,int r){
	return (sqrt((x-xc)*(x-xc) + (y-yc)*(y-yc)) < r);
}

bool pointIsInRectangle (int x,int y,int r_pos_x,int r_pos_y,int r_width,int r_height){
	return (x >= r_pos_x && x <= r_pos_x + r_width && y >= r_pos_y && y <= r_pos_y + r_height);
}

void freePolygon(Polygon p){
	betterFree(&(p.pts));
}

void drawPolygon (Polygon P,int x,int y){
	glBegin(GL_POLYGON);
		for(int i = 0;i < P.nbpts-1;i++){
			glVertex2i(P.pts[i].x + x,P.pts[i].y + y);
			glVertex2i(P.pts[i+1].x + x,P.pts[i+1].y + y);
		}
  	glEnd();
}

void drawPolygonZoomed (Polygon P,int x,int y,float zoom){
	glBegin(GL_POLYGON);
		for(int i = 0;i < P.nbpts-1;i++){
			glVertex2i(P.pts[i].x*zoom + x,P.pts[i].y*zoom + y);
			glVertex2i(P.pts[i+1].x*zoom + x,P.pts[i+1].y*zoom + y);
		}
  	glEnd();
}

Point new_Point (int x,int y){
	return (Point) {x,y};
}

Point_2f new_Point_2f (float x,float y){
	return (Point_2f) {x,y};
}

Line new_Line (int x1,int y1,int x2,int y2){
	return (Line) {new_Point(x1,y1),new_Point(x2,y2)};
}

char* Point_tostring (Point p){
	return str_format("Point( x=%d , y=%d )",p.x,p.y);
}

char* Point_2f_tostring (Point_2f p){
	return str_format("Point( x=%f , y=%f )",p.x,p.y);
}

char* Line_tostring (Line l){
	return str_format("Line( %s , %s )",Point_tostring(l.p1),Point_tostring(l.p2));
}

char* Polygon_tostring (Polygon p){
	char* ret = malloc(sizeof(char));
	ret[0] = '\0';
	int size = 1;
	size += strlen("Polygon {\n");
	ret = realloc(ret,size*sizeof(char*));
	strcat(ret,"Polygon {\n");
	for(int i=0;i<p.nbpts;i++){
		char *pt = Point_tostring(p.pts[i]);
		size += strlen(pt)+2;
		ret = realloc(ret,size*sizeof(char*));
		strcat(ret,"\t");
		strcat(ret,pt);
		ret[size-1] = '\0';
		ret[size-2] = '\n';
	}
	size += 1;
	ret = realloc(ret,size*sizeof(char*));
	strcat(ret,"}");
	return ret;
}