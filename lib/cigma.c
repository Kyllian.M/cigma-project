/*
APPS : 
sudo apt-get install xclip
sudo apt-get install vlc

LIBS :
sudo apt-get install libcsfml-audio2.3
sudo apt-get install libvlc-dev
sudo apt-get install libsdl1.2-dev
sudo apt-get install freeglut3-dev

JUST IN CASE :
sudo apt-get install libcsfml-dev
sudo apt-get install freeglut3

MAKEFILE : 
-lm -lSDL -lcsfml-audio -lcsfml-system -lvlc -lglut -lGLU -lGL -lX11 -g
*/


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <stdarg.h>
#include <X11/Xutil.h>
#include <vlc/vlc.h>
#include <vlc/libvlc.h>
#include <vlc/libvlc_media_player.h>

#include "../include/KToolsLib.h"
#define STB_TRUETYPE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include "../include/stb-master/stb_image.h"

#include "../include/cigma.h"

/* Motif window hints */
#define MWM_HINTS_DECORATIONS     (1L << 1)
#define PROP_MWM_HINTS_ELEMENTS   5

/* Motif  window hints */
typedef struct {
    long flags;
    long functions;
    long decorations;
    long inputMode;
    long unknown;
} MWMHints;

//definition des globales externes
unsigned char keyboardState[255];
unsigned char specialKeyboardState[21];
bool handleSouris = false;
int mouseState = 0;
int AS = -9999;
int OS = -9999;


//definition des globales cigma
int windowID;
Window windowXID;
bool continueloop = true;
bool minimized = false;
Image* icon;
int nbframe = 0;
double launchtime;
double self_rotate = 0;
RGBA stroke_color = (RGBA){0,0,0,0};
double update_delay = 0;
double display_delay = 0;
bool render_next_frame = false;
bool nostroke = true;
int vertical_offset = 0;
char* windowname = "";

int currentCursor = GLUT_CURSOR_LEFT_ARROW;

bool key_shift_pressed = false;
bool key_alt_pressed = false;
bool key_control_pressed = false;

void doNothing(){}

//callbacks
void (*initializefunc) 	 	() = doNothing;
void (*updatefunc) 		 	() = doNothing;
void (*render2Dfunc) 		 	() = doNothing;
void (*render3Dfunc) 		 	() = doNothing;
void (*mouseMovedfunc)   	() = doNothing;
void (*windowResizefunc) 	() = doNothing;
void (*windowClosefunc)  	() = doNothing;

// Tool functions (Not accessible outside of this file)
int** 			getPointsRoundedRectangle	(int width,int height,float coef,int rnbpts);
double 			getSystemTime				(void);
SDL_Surface* 	flipSurface					(SDL_Surface * surface);
unsigned long*  getCigmaIcon32 				();
unsigned long*  getCigmaIcon32Reversed 		();
Image* 			textureFromLongRGBA			(unsigned long* data,int width,int height);

// Opengl callback functions
void gestionSouris (int button,int state,int xa,int ya);
void motion (int xa,int ya);
void keyDown (unsigned char a,int x,int y);
void keyUp (unsigned char a,int x,int y);
void keySpecial (int key, int x, int y);
void displayFunc(void);
void resizeFunc(int largeur, int hauteur);
void updateGraphics();
void timerCallback(int delay);
void smoothRenderMode();


/**
 * @brief Launches Cigma Lib
 * 
 * @param largeurFenetre : height of the window
 * @param hauteurFenetre : width of the window
 * @param xfenetre : x coordinate of the window
 * @param yfenetre : y coordinate of the window
 * @param nomFenetre : name of the window
 * @param limFPS : Frames per Second limit
 * @param callbacks : functions that will be called during execution (NULL is accepted)
 *  callbacks[0] = initialize
 * 	callbacks[1] = update
 * 	callbacks[2] = render2D
 * 	callbacks[3] = render3D
 * 	callbacks[4] = mouseMoved
 * 	callbacks[5] = windowResize
 * 	callbacks[6] = windowClose
 * @param argc : standard main argc
 * @param argv : standard main argv
 * @return int : opengl id of the window
 */
int startGFX(int largeurFenetre,int hauteurFenetre,int xfenetre,int yfenetre,char* nomFenetre,int limFPS,void (*callbacks[7])(void),int* argc,char** argv){
	launchtime = getTimeVal();
	glutInit (argc,argv);
	glutInitDisplayMode (GLUT_DOUBLE|GLUT_RGBA|GLUT_DEPTH);

	//setting the window data
	glutInitWindowPosition(xfenetre,yfenetre);
	glutInitWindowSize(largeurFenetre, hauteurFenetre);
	windowID = glutCreateWindow(nomFenetre);
	windowname = nomFenetre;
	windowXID = glXGetCurrentDrawable();

	Atom net_wm_icon = XInternAtom(glXGetCurrentDisplay(), "_NET_WM_ICON", False);
    Atom cardinal = XInternAtom(glXGetCurrentDisplay(), "CARDINAL", False);
	int length =2 + 32*32;
	unsigned long *icon32 = getCigmaIcon32();
	unsigned long *icon2 = getCigmaIcon32Reversed();
	XChangeProperty(glXGetCurrentDisplay(), windowXID, net_wm_icon, cardinal, 32, PropModeReplace, (const unsigned char*) icon32, length);
	icon = textureFromLongRGBA(icon2,32,32);
	free(icon32);
	free(icon2);
	

	//correcting perspective
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glDepthFunc(GL_LEQUAL);

	smoothRenderMode();
	glEnable(GL_COLOR_MATERIAL);

	glPointSize(1);
	glLineWidth(1);

	//Setting Callbacks
	if(callbacks[0] != NULL){
		initializefunc = callbacks[0];
	}
	if(callbacks[1] != NULL){
		updatefunc = callbacks[1];
	}
	if(callbacks[2] != NULL){
		render2Dfunc = callbacks[2];
	}
	if(callbacks[3] != NULL){
		render3Dfunc = callbacks[3];
	}
	if(callbacks[4] != NULL){
		mouseMovedfunc = callbacks[4];
	}
	if(callbacks[5] != NULL){
		windowResizefunc = callbacks[5];
	}
	if(callbacks[6] != NULL){
		windowClosefunc = callbacks[6];
	}

	glutDisplayFunc(displayFunc);
	glutIdleFunc(displayFunc);
	glutMouseFunc(gestionSouris);
	glutMotionFunc(motion);
	glutPassiveMotionFunc(motion);
	glutKeyboardFunc(keyDown);
	glutKeyboardUpFunc(keyUp);
	glutReshapeFunc(resizeFunc);
	glutSpecialFunc(keySpecial);

	//initializing keyboard
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
	for(int i=0;i<255;i++){
		keyboardState[i] = false;
	}
	for(int i=0;i<21;i++){
		specialKeyboardState[i] = false;
	}
	
	initializefunc();
	int delay;	//delay between each frame
	if(limFPS <= 0){
		delay = 0;
	}
	else{
		delay = 1000/limFPS;
	}
	
	glutTimerFunc(delay, timerCallback, delay);		//calling the update function indefinatly
	glutMainLoop();		//start GLUT
	return windowID;
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	OPENGL CALLBACKS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

//FONCTION DE GESTION DES CLICKS ET DEPLACEMENTS DE LA SOURIS
void gestionSouris (int button,int state,int xa,int ya){
	switch (button){
		case GLUT_LEFT_BUTTON :
			//LEFT CLICK
			switch(state){
				case GLUT_UP:
					//PRESSED
					mouseState = MOUSE_LEFT_UP;
				break;
				case GLUT_DOWN:
					//UNPRESSED
					mouseState = MOUSE_LEFT_DOWN;
				break;
			}
		break;
		case 3 :
			//SCROLLING UP
			mouseState = MOUSE_SCROLL_UP;
		break;
		case 4 :
			//SCROLLING DOWN
			mouseState = MOUSE_SCROLL_DOWN;
		break;
		case GLUT_RIGHT_BUTTON :
			//RIGHT CLICK
			switch(state){
				case GLUT_UP:
					//PRESSED
					mouseState = MOUSE_RIGHT_UP;
				break;
				case GLUT_DOWN:
					//UNPRESSED
					mouseState = MOUSE_RIGHT_DOWN;
				break;
			}
		break;
	}
	AS = xa;
	OS = glutGet(GLUT_WINDOW_HEIGHT) - ya;
	handleSouris = true;
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DE MOTION DE LA SOURIS 
void motion (int xa,int ya){
	AS = xa;
	OS = glutGet(GLUT_WINDOW_HEIGHT) - ya;
	mouseMovedfunc();
}
///CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

bool controlPressed (){
	return key_control_pressed;
}

bool shiftPressed (){
	return key_shift_pressed;
}

bool altPressed (){
	return key_alt_pressed;
}

//FONCTION CALLBACK DE PRESSION CLAVIER
void keyDown (unsigned char a,int x,int y){
	key_control_pressed = glutGetModifiers() == GLUT_ACTIVE_CTRL;
	key_shift_pressed = glutGetModifiers() == GLUT_ACTIVE_SHIFT;
	key_alt_pressed = glutGetModifiers() == GLUT_ACTIVE_ALT;
	keyboardState[a] = true;
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

//FONCTION CALLBACK DE RELACHEMENT CLAVIER
void keyUp (unsigned char a,int x,int y){
	key_control_pressed = glutGetModifiers() == GLUT_ACTIVE_CTRL;
	key_shift_pressed = glutGetModifiers() == GLUT_ACTIVE_SHIFT;
	key_alt_pressed = glutGetModifiers() == GLUT_ACTIVE_ALT;
	keyboardState[a] = 2;
}
//CREE PAR KYLLIAN MARIE EN CIR2 A L'ISEN TOULON

void keySpecial (int key, int x, int y){
	switch(key){
		case GLUT_KEY_F1 		: specialKeyboardState[ 0] = true; break;		
		case GLUT_KEY_F2 		: specialKeyboardState[ 1] = true; break;		
		case GLUT_KEY_F3 		: specialKeyboardState[ 2] = true; break;		
		case GLUT_KEY_F4 		: specialKeyboardState[ 3] = true; break;		
		case GLUT_KEY_F5 		: specialKeyboardState[ 4] = true; break;		
		case GLUT_KEY_F6 		: specialKeyboardState[ 5] = true; break;		
		case GLUT_KEY_F7 		: specialKeyboardState[ 6] = true; break;		
		case GLUT_KEY_F8 		: specialKeyboardState[ 7] = true; break;		
		case GLUT_KEY_F9 		: specialKeyboardState[ 8] = true; break;		
		case GLUT_KEY_F10		: specialKeyboardState[ 9] = true; break;	
		case GLUT_KEY_F11		: specialKeyboardState[10] = true; break;	
		case GLUT_KEY_F12		: specialKeyboardState[11] = true; break;	
		case GLUT_KEY_LEFT		: specialKeyboardState[12] = true; break;	
		case GLUT_KEY_UP		: specialKeyboardState[13] = true; break;	
		case GLUT_KEY_RIGHT		: specialKeyboardState[14] = true; break;	
		case GLUT_KEY_DOWN		: specialKeyboardState[15] = true; break;
		case GLUT_KEY_PAGE_UP	: specialKeyboardState[16] = true; break;
		case GLUT_KEY_PAGE_DOWN	: specialKeyboardState[17] = true; break;
		case GLUT_KEY_HOME		: specialKeyboardState[18] = true; break;	
		case GLUT_KEY_END		: specialKeyboardState[19] = true; break;	
		case GLUT_KEY_INSERT	: specialKeyboardState[20] = true; break;	
	}
}

void glut3DPreset (){
	glPushMatrix();
	glLoadIdentity();

    //Init 3D projection & view
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    gluPerspective(60, (GLdouble) LF/HF, 1, 10000);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    
	glEnable(GL_DEPTH_TEST);
	//Camera placement
	//eye = cameraPos (x,y,z)
	//center = camera direction (x,y,z)
	//Up : 0,1,0
	gluLookAt(0,100,-500, 0, 0, 0, 0, 1 ,0);

    glPushMatrix();
	setColor4i(160,20,20, 255);
	//draw 3D

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
}

void glut2DPreset(){
	glPushMatrix();
	glLoadIdentity();
	
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

	gluOrtho2D(0,LF,0, HF);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void displayFunc(void){
	double timenow = getTimeVal();
	glFlush();
	glClearColor(0.f, 0.f, 0.f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glut3DPreset();
	render3Dfunc();
	glPopMatrix();
	glDisable(GL_LIGHT0);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glPopMatrix();
	glut2DPreset();
	render2Dfunc();
	glPopMatrix();
	glutSwapBuffers();
	display_delay = getTimeVal() - timenow;
}

void resizeFunc(int largeur, int hauteur){
	glViewport(0, 0, largeur, hauteur);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0., (GLdouble)largeur, 0., (GLdouble)hauteur, -1., 1.);
	glMatrixMode (GL_MODELVIEW);
	windowResizefunc();
}

void updateGraphics(){
	render_next_frame = true;
}

void timerCallback(int delay){
	currentCursor = GLUT_CURSOR_INHERIT;
	static int oldcursor = GLUT_CURSOR_LEFT_ARROW;

	render_next_frame = false;
	double timenow = getTimeVal();
	updatefunc();
	update_delay = getTimeVal() - timenow;
	ifkeypressed(27){ //ECHAP
		continueloop = false;
		windowClosefunc();
		glutIdleFunc(0); // Turn off Idle function if used.
		glutDestroyWindow(windowID);// Close open windows
	}
	if(continueloop){
		Window* focused = malloc(sizeof(Window));
		int* revert = malloc(sizeof(int));
		XGetInputFocus(glXGetCurrentDisplay(),focused,revert);
		if(minimized && *focused == windowXID){
			glutHideWindow();
			glutShowWindow();
			minimized = false;
		}
		free(focused);
		free(revert);

		for(int i=0;i<255;i++){
			if(keyboardState[i] == 2)
			keyboardState[i] = false;
		}
		for(int i=0;i<21;i++){
			specialKeyboardState[i] = false;
		}
		glutTimerFunc(delay, timerCallback, delay);
		if(render_next_frame){
			glutPostRedisplay();
		}
		handleSouris = false;
	}

	if(currentCursor != oldcursor){
		glutSetCursor(currentCursor);
	}
	oldcursor = currentCursor;
}

void smoothRenderMode(){
	glShadeModel(GL_SMOOTH);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glHint (GL_POINT_SMOOTH_HINT, GL_NICEST);
	glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);
	glHint (GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_NORMALIZE);
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

/**
 * @brief replaces the window cursor with OS standard cursors
 * 
 * @param glutcursor : GLUT constant for cursors. 
 * 	Possible Values :
 * 	 - GLUT_CURSOR_RIGHT_ARROW
 * 	 	Arrow pointing up and to the right.
 * 	 - GLUT_CURSOR_LEFT_ARROW
 * 	 	Arrow pointing up and to the left. [DEFAULT]
 * 	 - GLUT_CURSOR_INFO
 * 	 	Pointing hand.
 * 	 - GLUT_CURSOR_DESTROY
 * 	 	Skull & cross bones.
 * 	 - GLUT_CURSOR_HELP
 * 	 	Question mark.
 * 	 - GLUT_CURSOR_CYCLE
 * 	 	Arrows rotating in a circle.
 * 	 - GLUT_CURSOR_SPRAY
 * 	 	Spray can.
 * 	 - GLUT_CURSOR_WAIT
 * 	 	Wrist watch.
 * 	 - GLUT_CURSOR_TEXT
 * 	 	Insertion point cursor for text.
 * 	 - GLUT_CURSOR_CROSSHAIR
 * 	 	Simple cross-hair.
 * 	 - GLUT_CURSOR_UP_DOWN
 * 	 	Bi-directional pointing up & down.
 * 	 - GLUT_CURSOR_LEFT_RIGHT
 * 	 	Bi-directional pointing left & right.
 * 	 - GLUT_CURSOR_TOP_SIDE
 * 	 	Arrow pointing to top side.
 * 	 - GLUT_CURSOR_BOTTOM_SIDE
 * 	 	Arrow pointing to bottom side.
 * 	 - GLUT_CURSOR_LEFT_SIDE
 * 	 	Arrow pointing to left side.
 * 	 - GLUT_CURSOR_RIGHT_SIDE
 * 	 	Arrow pointing to right side.
 * 	 - GLUT_CURSOR_TOP_LEFT_CORNER
 * 	 	Arrow pointing to top-left corner.
 * 	 - GLUT_CURSOR_TOP_RIGHT_CORNER
 * 	 	Arrow pointing to top-right corner.
 * 	 - GLUT_CURSOR_BOTTOM_RIGHT_CORNER
 * 	 	Arrow pointing to bottom-left corner.
 * 	 - GLUT_CURSOR_BOTTOM_LEFT_CORNER
 * 	 	Arrow pointing to bottom-right corner.
 * 	 - GLUT_CURSOR_FULL_CROSSHAIR
 * 	 	Full-screen cross-hair cursor (if possible, otherwise GLUT_CURSOR_CROSSHAIR).
 * 	 - GLUT_CURSOR_NONE
 * 	 	Invisible cursor.
 * 	 - GLUT_CURSOR_INHERIT
 * 	 	Use parent's cursor.
 */
void setCursor (int glutcursor){
	currentCursor = glutcursor;
}

//decallage vertical pour overlay
void setVerticalOffset(int offset){
	vertical_offset = offset;
}

int getVerticalOffset(){
	return vertical_offset;
}

double getTimeVal (){
	struct timeval systime;
	gettimeofday(&systime, NULL);
	return (double)systime.tv_sec+(double)systime.tv_usec/1000000.;
}

char* getWindowName (){
	return windowname;
}

int getWindowId_gl (){
	return windowID;
}

unsigned long getWindowId_x11 (){
	return windowXID;
}

void setWindowPosition (int x,int y){
	glutPositionWindow(x,y);
}

void setWindowSize (int width,int height){
	glutReshapeWindow(width,height);
}

//methode du bled
void setClipboard(char* str){
	FILE *fp = NULL;
    fp = popen("xclip -selection clipboard", "w");
    if (fp == NULL) {
        printf("CIGMA ERROR @setClipboard : XCLIP MISSING : Could not copy '%s' to clipboard\n Install xclip with sudo apt-get install xclip for copy/paste functions",str);
    }
    else{
		fputs(str,fp);
		fputc ( '\n', fp);
		pclose(fp);
	}
}

char* getClipboard(){
	FILE *fp = NULL;
    fp = popen("xclip -selection clipboard -o", "r");
    if (fp == NULL) {
        printf("CIGMA ERROR @setClipboard : XCLIP MISSING : Could not access clipboard\n Install xclip with sudo apt-get install xclip for copy/paste functions");
    }
    else{
		char clipboard[4000];	//4000 est la limite de charactere du clipboard unix
		fgets(clipboard,4000,fp);
		char* ret = malloc(sizeof(char)*strlen(clipboard));
		clipboard[strlen(clipboard)-1] = '\0';	//suppression du '\n' a la fin du clipboard
		sprintf(ret,"%s",clipboard);
		pclose(fp);
		return ret;
	}
	return NULL;
}

void endProcess (){
	continueloop = false;
	windowClosefunc();
	glutIdleFunc(0);
	glutDestroyWindow(windowID);
}

void fullscreen (){
	setWindowPosition(0,0);
	glutFullScreen();
}

void minimize (){
	glutIconifyWindow();
	XSetInputFocus(glXGetCurrentDisplay(),None,RevertToNone,CurrentTime);	//perte du focus
	minimized = true;
}

void setIcon (char* imgname){
	Image* img = new_Image(imgname,false);
	int nbpixels = img->width*img->height;
	unsigned char* data_rgba = malloc(sizeof(unsigned char)*nbpixels*4);
	glGetTexImage(img->donneesRGB,0,GL_RGBA,GL_UNSIGNED_BYTE,data_rgba);
	unsigned long* data_argb = malloc(sizeof(unsigned long)*(nbpixels + 2));
	data_argb[0] = img->width;
	data_argb[1] = img->height;
	int cpt = 2;

	for (int i=img->height-1; i >= 0; i--){
        for(int i2 = 0; i2 < img->width; i2++) {
			unsigned long r = data_rgba[(i*img->width+i2)*4];
			unsigned long g = data_rgba[(i*img->width+i2)*4+1];
			unsigned long b = data_rgba[(i*img->width+i2)*4+2];
			unsigned long a = data_rgba[(i*img->width+i2)*4+3];
			data_argb[cpt] = (a << 8*3) | (r << 8*2) | (g << 8) | (b); 
			cpt++;
        }
    }

	free(data_rgba);
	freeImage(&img);

	Atom net_wm_icon = XInternAtom(glXGetCurrentDisplay(), "_NET_WM_ICON", False);
    Atom cardinal = XInternAtom(glXGetCurrentDisplay(), "CARDINAL", False);
	int length =2 + nbpixels;
	XChangeProperty(glXGetCurrentDisplay(), windowXID, net_wm_icon, cardinal, 32, PropModeReplace, (const unsigned char*) data_argb, length);
	free(data_argb);
}

void removeOverlayOS (){
	MWMHints mwmhints;
	Atom prop;
	memset(&mwmhints, 0, sizeof(mwmhints));
	prop = XInternAtom(glXGetCurrentDisplay(), "_MOTIF_WM_HINTS", False);
	mwmhints.flags = MWM_HINTS_DECORATIONS;
	mwmhints.decorations = 0;
	XChangeProperty(glXGetCurrentDisplay(), windowXID, prop, prop, 32, PropModeReplace, (unsigned char *) &mwmhints, PROP_MWM_HINTS_ELEMENTS);
}

void addOverlayOS (){
	MWMHints mwmhints;
	Atom prop;
	memset(&mwmhints, 0, sizeof(mwmhints));
	prop = XInternAtom(glXGetCurrentDisplay(), "_MOTIF_WM_HINTS", False);
	mwmhints.flags = MWM_HINTS_DECORATIONS;
	mwmhints.decorations = 1;
	XChangeProperty(glXGetCurrentDisplay(), windowXID, prop, prop, 32, PropModeReplace, (unsigned char *) &mwmhints, PROP_MWM_HINTS_ELEMENTS);
}


/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	COLORS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

RGB new_RGB (unsigned char r,unsigned char g,unsigned char b){
	RGB ret;
	ret.r = r;
	ret.g = g;
	ret.b = b;
	return ret;
}

RGBA new_RGBA (unsigned char r,unsigned char g,unsigned char b,unsigned char a){
	RGBA ret;
	ret.r = r;
	ret.g = g;
	ret.b = b;
	ret.a = a;
	return ret;
}

RGBA RGBtoRGBA (RGB color,unsigned char opacity){
	return new_RGBA(color.r,color.g,color.b,opacity);
}

RGB RGBAtoRGB (RGBA color){
	return new_RGB(color.r,color.g,color.b);
}

void setColor4i (unsigned char r, unsigned char g, unsigned char b,unsigned char a){
	glColor4f(r*1./255,g*1./255,b*1./255,a*1./255);
}

void setColorRGB (RGB color){
	setColor4i(color.r,color.g,color.b,255);
}

void setColorRGBA (RGBA color){
	setColor4i(color.r,color.g,color.b,color.a);
}

void setColor (unsigned char r,unsigned char g,unsigned char b){
	setColor4i(r,g,b,255);
}

void color (unsigned char r,unsigned char g,unsigned char b){
	setColor(r,g,b);
}

RGB getPixelColor(int x,int y){
	int numpixel = y*LF + x;
	RGB ret;
	ret.r = (numpixel >> 16)&0x0FF;
	ret.g = (numpixel >> 8)&0x0FF;
	ret.b = numpixel&0x0FF;
	return ret;
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	PAINTBRUSH FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void fill(int r,int g,int b,int a){
	setColor4i(r,g,b,a);
}

void fillRGBA(RGBA color){
	setColor4i(color.r,color.g,color.b,color.a);
}

void fillRGB(RGB color){
	setColor4i(color.r,color.g,color.b,255);
}

RGBA getFill(){
	float color[4];
	glGetFloatv(GL_CURRENT_COLOR, color);
	return new_RGBA(color[0]*255,color[1]*255,color[2]*255,color[3]*255);
}

void stroke (int r,int g,int b,int a){
	if(r != -1 && g != -1 && b != -1 && a != -1){ //NOFILL
		stroke_color = new_RGBA(r,g,b,a);
	}
	else{
		stroke_color = new_RGBA(0,0,0,0);
	}
}

void strokeRGBA (RGBA color){
	stroke_color = color;
}

void strokeRGB (RGB color){
	stroke_color = RGBtoRGBA(color,255);
}

RGBA getStroke(){
	return stroke_color;
}

void hardResetPosition (int x,int y){
	glLoadIdentity();
}

void resetPosition (int x,int y){
	if(self_rotate != 0){
		glRotated(-self_rotate,0,0,1);
	}
	glTranslated(-x,-y,0);
}

void setPosition(int x,int y){
	glTranslated(x,y,0);
	if(self_rotate != 0){
		glRotated(self_rotate,0,0,1);
	}
}

void translate(int x,int y){
	glTranslated(x,y,0);
}

void rotate(double angle){
	glRotated(angle,0,0,1);
}

void selfRotate(double angle){	//degres
	self_rotate = angle;
}

double getSelfRotate(){
	return self_rotate;
}

void strokeWidth(int width){
	nostroke = (width == 0);
	if(!nostroke){
		glPointSize(width);
		glLineWidth(width);
	}
}

int getStrokeWidth(){
	if(nostroke){
		return 0;
	}
	int ret;
	glGetIntegerv(GL_LINE_WIDTH,&ret);
	return ret;
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	BASIC GEOMETRY
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void dot(int x,int y){
	RGBA fill = getFill();
	setColorRGBA(getStroke());
	glBegin(GL_POINTS);
		glVertex2f(x, y);
	glEnd();
	setColorRGBA(fill);
}

//non affecté par selfrotate
void vertexLine(int x1,int y1,int x2,int y2){
	glBegin(GL_LINES);
		glVertex2f(x1, y1);
		glVertex2f(x2, y2);
	glEnd();
}

void line(int x1, int y1, int x2, int y2){
	RGBA fill = getFill();
	setColorRGBA(getStroke());
	int xcenter = (x1+x2)/2;
	int ycenter = (y1+y2)/2;
	setPosition(xcenter,ycenter);
	glBegin(GL_LINES);
		glVertex2f(x1 - xcenter, y1 - ycenter);
		glVertex2f(x2 - xcenter, y2 - ycenter);
	glEnd();
	resetPosition(xcenter,ycenter);
	setColorRGBA(fill);
}

void cross (int x,int y,int width,int height){
	line(x,y,x+width,y+width);
	line(x+width,y,x,y+height);
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	FILLED SHAPES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void fillCircle(int x, int y, float r,int nbpts){
	setPosition(x,y);
	static const double pi = 3.14159265358979323846;
	double step = 2.*pi/nbpts;
	glBegin(GL_POLYGON);
		for (int i = 0; i < nbpts; i++){
			const double angle = 2.*pi*i/nbpts;
			glVertex2i(r*cos(angle), r*sin(angle));
			glVertex2i(r*cos(angle+step), r*sin(angle+step));
		}
  	glEnd();
	resetPosition(x,y);
}

void fillTriangle(int x1,int y1,int x2,int y2,int x3,int y3){
	int xcenter = (x1+x2+x3)/3;
	int ycenter = (y1+y2+y3)/3;
	setPosition(xcenter,ycenter);
	glBegin(GL_TRIANGLES);
		glVertex2f(x1 - xcenter,y1 - ycenter);
		glVertex2f(x2 - xcenter,y2 - ycenter);
		glVertex2f(x3 - xcenter,y3 - ycenter);
	glEnd();
	resetPosition(xcenter,ycenter);
}

void fillRectangle(int x,int y, int width,int height){
	int halfwidth = width/2;
	int halfheight = height/2;
	int xcenter = x + halfwidth;
	int ycenter = y + halfheight;

	setPosition(xcenter,ycenter);
	glBegin(GL_QUADS);
		glVertex2f(-halfwidth , -halfheight);
		glVertex2f( halfwidth , -halfheight);
		glVertex2f( halfwidth ,  halfheight);
		glVertex2f(-halfwidth ,  halfheight);
	glEnd();
	resetPosition(xcenter,ycenter);
}

void fillSquare(int x,int y,int width){
	fillRectangle(x,y,width,width);
}

void fillRoundedRectangle (int x,int y,int width,int height,float coef,int rnbpts){
	int** points 	= getPointsRoundedRectangle(width,height,coef,rnbpts);
	int* pts_x 		= points[0];
	int* pts_y 		= points[1];
	int halfwidth 	= width/2;
	int halfheight 	= height/2;

	setPosition(x + halfwidth,y + halfheight);

	glBegin(GL_POLYGON);
		for(int i = 0;i < rnbpts-1;i++){
			glVertex2i(pts_x[i] -halfwidth,pts_y[i] -halfheight);
			glVertex2i(pts_x[i+1] -halfwidth,pts_y[i+1] -halfheight);
		}
  	glEnd();

	resetPosition(x + halfwidth,y + halfheight);

	free(pts_x);
	pts_x = NULL;
	free(pts_y);
	pts_y = NULL;
	free(points);
	points = NULL;
}
 
void fillEllipse(int x,int y,int radiusX, int radiusY){
	setPosition(x,y);
	glBegin(GL_POLYGON);
		for(int i=0;i<360;i++){
			float rad = i*3.14159/180.0;
			glVertex2f(cos(rad)*radiusX ,sin(rad)*radiusY);
		}
	glEnd();
	resetPosition(x,y);
}

void fillQuads (int x1,int y1,int x2,int y2,int x3,int y3,int x4,int y4){
	int xcenter = (x1+x2+x3+x4)/4;
	int ycenter = (y1+y2+y3+y4)/4;

	setPosition(xcenter,ycenter);

	glBegin(GL_QUADS);
		glVertex2i(x1-xcenter,y1-ycenter);
		glVertex2i(x2-xcenter,y2-ycenter);
		glVertex2i(x3-xcenter,y3-ycenter);
		glVertex2i(x4-xcenter,y4-ycenter);
	glEnd();

	resetPosition(xcenter,ycenter);
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	STROKED SHAPES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void strokeCircle(int x, int y, float r,int nbpts){
	RGBA fill = getFill();
	setColorRGBA(getStroke());
	setPosition(x,y);
	static const double pi = 3.14159265358979323846;
	double step = 2.*pi/nbpts;
	glBegin(GL_LINE_LOOP);
		for (int i = 0; i < nbpts; i++){
			const double angle = 2.*pi*i/nbpts;
			glVertex2i(r*cos(angle), r*sin(angle));
			glVertex2i(r*cos(angle+step), r*sin(angle+step));
		}
  	glEnd();
	for (int i = 0; i < nbpts; i++){
		const double angle = 2.*pi*i/nbpts;
		dot(r*cos(angle+step), r*sin(angle+step));
	}
	resetPosition(x,y);
	setColorRGBA(fill);
}

void strokeTriangle(int x1,int y1,int x2,int y2,int x3,int y3){
	RGBA fill = getFill();
	setColorRGBA(getStroke());
	int xcenter = (x1+x2+x3)/3;
	int ycenter = (y1+y2+y3)/3;
	setPosition(xcenter,ycenter);
	glBegin(GL_LINE_LOOP);
		glVertex2f(x1 - xcenter,y1 - ycenter);
		glVertex2f(x2 - xcenter,y2 - ycenter);
		glVertex2f(x3 - xcenter,y3 - ycenter);
	glEnd();
	dot(x1 - xcenter,y1 - ycenter);
	dot(x2 - xcenter,y2 - ycenter);
	dot(x3 - xcenter,y3 - ycenter);
	resetPosition(xcenter,ycenter);
	setColorRGBA(fill);
}

void strokeRectangle(int x,int y, int width,int height){
	RGBA fill = getFill();
	setColorRGBA(getStroke());
	int strokew = getStrokeWidth()/2;
	int halfwidth = width/2;
	int halfheight = height/2;
	int xcenter = x + halfwidth;
	int ycenter = y + halfheight;

	setPosition(xcenter,ycenter);
	vertexLine(-halfwidth			,-halfheight+strokew 	, halfwidth			,-halfheight+strokew);
	vertexLine( halfwidth-strokew	,-halfheight			, halfwidth-strokew	, halfheight		);
	vertexLine( halfwidth			, halfheight-strokew	,-halfwidth			, halfheight-strokew);
	vertexLine(-halfwidth+strokew	, halfheight			,-halfwidth+strokew	,-halfheight		);
	resetPosition(xcenter,ycenter);

	setColorRGBA(fill);
}

void strokeSquare(int x,int y,int width){
	strokeRectangle(x,y,width,width);
}

void strokeRoundedRectangle (int x,int y,int width,int height,float coef,int rnbpts){
	int** points 	= getPointsRoundedRectangle(width,height,coef,rnbpts);
	int* pts_x 		= points[0];
	int* pts_y 		= points[1];
	int halfwidth 	= width/2;
	int halfheight 	= height/2;

	RGBA fill = getFill();
	setColorRGBA(getStroke());
	setPosition(x + halfwidth,y + halfheight);

	glBegin(GL_LINE_LOOP);
		for(int i = 0;i < rnbpts-1;i++){
			glVertex2i(pts_x[i] -halfwidth,pts_y[i] -halfheight);
			glVertex2i(pts_x[i+1] -halfwidth,pts_y[i+1] -halfheight);
		}
  	glEnd();
	for(int i = 0;i < rnbpts;i++){
		dot(pts_x[i] -halfwidth,pts_y[i] -halfheight);
	}

	resetPosition(x + halfwidth,y + halfheight);
	setColorRGBA(fill);

	free(pts_x);
	pts_x = NULL;
	free(pts_y);
	pts_y = NULL;
	free(points);
	points = NULL;
}

void strokeEllipse(int x,int y,int radiusX, int radiusY){
	RGBA fill = getFill();
	setColorRGBA(getStroke());
	setPosition(x,y);

	glBegin(GL_LINE_LOOP);
		for(int i=0;i<360;i++){
			float rad = i*3.14159/180.0;
			glVertex2f(cos(rad)*radiusX ,sin(rad)*radiusY);
		}
	glEnd();
	for (int i=0;i<360;i++){
		float rad = i*3.14159/180.0;
		dot(cos(rad)*radiusX , sin(rad)*radiusY);
	}

	resetPosition(x,y);
	setColorRGBA(fill);
}

void strokeQuads (int x1,int y1,int x2,int y2,int x3,int y3,int x4,int y4){
	int xcenter = (x1+x2+x3+x4)/4;
	int ycenter = (y1+y2+y3+y4)/4;

	RGBA fill = getFill();
	setColorRGBA(getStroke());
	setPosition(xcenter,ycenter);

	glBegin(GL_LINE_LOOP);
		glVertex2i(x1-xcenter,y1-ycenter);
		glVertex2i(x2-xcenter,y2-ycenter);
		glVertex2i(x3-xcenter,y3-ycenter);
		glVertex2i(x4-xcenter,y4-ycenter);
	glEnd();
	dot(x1-xcenter,y1-ycenter);
	dot(x2-xcenter,y2-ycenter);
	dot(x3-xcenter,y3-ycenter);
	dot(x4-xcenter,y4-ycenter);

	resetPosition(xcenter,ycenter);
   	setColorRGBA(fill);
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	SHAPES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void circle(int x, int y, float r,int nbpts){
	if(getFill().a != 0){
		fillCircle(x,y,r-getStrokeWidth(),nbpts);
	}
	if(getStrokeWidth() > 0 && getStroke().a != 0){
		strokeCircle(x,y,r-getStrokeWidth()/2,nbpts);
	}
}

void triangle(int x1,int y1,int x2,int y2,int x3,int y3){
	if(getFill().a != 0){
		fillTriangle(x1,y1,x2,y2,x3,y3);
	}
	if(getStrokeWidth() > 0 && getStroke().a != 0){
		strokeTriangle(x1,y1,x2,y2,x3,y3);
	}
}

void rectangle(int x,int y, int width,int height){
	int strokewidth = getStrokeWidth();
	if(getFill().a != 0){
		fillRectangle(x+strokewidth,y+strokewidth,width-strokewidth*2,height-strokewidth*2);
	}
	if(getStrokeWidth() > 0 && getStroke().a != 0){
		strokeRectangle(x,y,width,height);
	}
}

void square(int x,int y,int width){
	rectangle(x,y,width,width);
}

void ellipse(int x,int y,int radiusX, int radiusY){
	if(getFill().a != 0){
		fillEllipse(x,y,radiusX,radiusY);
	}
	if(getStrokeWidth() > 0 && getStroke().a != 0){
		strokeEllipse(x,y,radiusX,radiusY);
	}
}

void quads (int x1,int y1,int x2,int y2,int x3,int y3,int x4,int y4){
	if(getFill().a != 0){
		fillQuads(x1,y1,x2,y2,x3,y3,x4,y4);
	}
	if(getStrokeWidth() > 0 && getStroke().a != 0){
		strokeQuads(x1,y1,x2,y2,x3,y3,x4,y4);
	}
}


/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	SMOOTHED SHAPES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void smoothCircle (int x, int y, float r,int nbpts){
	int strk = getStrokeWidth();
	strokeWidth(2);
	fillCircle	(x,y,r,nbpts);
	strokeCircle(x,y,r,nbpts);
	strokeWidth(strk);
}

void smoothTriangle (int x1,int y1,int x2,int y2,int x3,int y3){
	int strk = getStrokeWidth();
	strokeWidth(2);
	fillTriangle(x1,y1,x2,y2,x3,y3);
	strokeTriangle	(x1,y1,x2,y2,x3,y3);
	strokeWidth(strk);
}

void smoothRectangle(int x,int y,int width,int height){
	int strk = getStrokeWidth();
	strokeWidth(2);
	fillRectangle(x,y,width,height);
	strokeRectangle(x,y,width,height);
	strokeWidth(strk);
}

void smoothSquare (int x,int y,int width){
	int strk = getStrokeWidth();
	strokeWidth(2);
	fillSquare(x,y,width);
	strokeSquare(x,y,width);
	strokeWidth(strk);
}

void roundedRectangle (int x,int y,int width,int height,float coef,int rnbpts){
	if(coef == 0){
		rectangle(x,y,width,height);
	}
	else{
		if(getFill().a != 0){
			fillRoundedRectangle(x,y,width,height,coef,rnbpts);
		}
		if(getStrokeWidth() > 0 && getStroke().a != 0){
			strokeRoundedRectangle(x,y,width,height,coef,rnbpts);
		}
	}
}

void smoothRoundedRectangle(int x,int y,int width,int height,float coef,int rnbpts){
	int strk = getStrokeWidth();
	strokeWidth(2);
	fillRoundedRectangle(x,y,width,height,coef,rnbpts);
	strokeRoundedRectangle(x,y,width,height,coef,rnbpts);
	strokeWidth(strk);
}

void smoothEllipse(int x,int y,int radiusX, int radiusY){
	int strk = getStrokeWidth();
	strokeWidth(2);
	fillEllipse(x,y,radiusX,radiusY);
	strokeEllipse(x,y,radiusX,radiusY);
	strokeWidth(strk);
}

void smoothQuads(int x1,int y1,int x2,int y2,int x3,int y3,int x4,int y4){
	int strk = getStrokeWidth();
	strokeWidth(2);
	fillQuads(x1,y1,x2,y2,x3,y3,x4,y4);
	strokeQuads(x1,y1,x2,y2,x3,y3,x4,y4);
	strokeWidth(strk);
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void clear(unsigned char r,unsigned char g,unsigned char b){
	glClearColor(r/255.f, g/255.f, b/255.f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
}

void clearRGB(RGB color){
	glClearColor(color.r/255.f, color.g/255.f, color.b/255.f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
}

void displayFPS (int nbrefresh){
	//getting the time from January 1st 1970
	glPushMatrix();
	hardResetPosition(0,0);
	double rota = getSelfRotate();
	selfRotate(0);

	struct timeval systime;
	gettimeofday(&systime, NULL);
	double time = (double)systime.tv_sec+(double)systime.tv_usec/1000000.;

	static double t0 = 0;
	if(t0 == 0){
		t0=time*nbrefresh;
	}
	static int t1 = 0;
	static int t2 = 0;

	static double elapsed_time = 0;
	static double render_time = 0;

	elapsed_time += display_delay;

	static double elapsed_time2 = 0;
	static double update_time = 0;

	elapsed_time2 += update_delay;

	double t = time*nbrefresh - t0;
	t2 = t1;
	t1 = (int) t;
	static int cpt = 0;
	cpt ++;
	static int fps = 0;

	if(t1 != t2){
		render_time = elapsed_time/cpt;
		update_time = elapsed_time2/cpt;
		elapsed_time = 0;
		elapsed_time2 = 0;
		fps = cpt*nbrefresh;
		cpt = 0;
	}

	char chfps[30];
	sprintf(chfps,"%d FPS",fps);
	int back = getTextWidth(GLUT_BITMAP_HELVETICA_12,12,chfps); 

	setColor4i(0,0,0,127);
	fillRectangle(LF-100,HF-54,LF,HF);
	color(255,255,255);
	strokeWidth(2);
	bitmapText(GLUT_BITMAP_HELVETICA_12,LF-back,HF-12,chfps);
	char renderunit = ' ';
	if(render_time < 0.01){
		renderunit = 'm';
		if(render_time < 0.00001){
			renderunit = 'u';	//micro
		}
	}
	char updateunit = ' ';
	if(update_time < 0.01){
		updateunit = 'm';
		if(update_time < 0.00001){
			updateunit = 'u';	//micro
		}
	}

	char strscale[100];
	sprintf(strscale,"render: %.2f %cs",render_time*(renderunit == ' ' ? 1 : 1000*(renderunit == 'm' ? 1:1000)),renderunit);
	bitmapText(GLUT_BITMAP_HELVETICA_12,LF-getTextWidth(GLUT_BITMAP_HELVETICA_12,12,strscale),HF-26,strscale);
	sprintf(strscale,"update: %.2f %cs",update_time*(updateunit == ' ' ? 1 : 1000*(updateunit == 'm' ? 1:1000)),updateunit);
	bitmapText(GLUT_BITMAP_HELVETICA_12,LF-getTextWidth(GLUT_BITMAP_HELVETICA_12,12,strscale),HF-40,strscale);
	
	char num[10];
	sprintf(num,"%.f",getTimeVal() - launchtime);
	int elsec = atoi(num);
	int sec = elsec%60;
	int min = (elsec/60)%60;
	int hour = (elsec/3600);
	char strtime[100];
	sprintf(strtime,"runtime: %s%d:%s%d:%s%d",hour < 10 ? "0":"",hour,min < 10 ? "0":"",min,sec < 10 ? "0":"",sec);
	bitmapText(GLUT_BITMAP_HELVETICA_12,LF-getTextWidth(GLUT_BITMAP_HELVETICA_12,12,strtime),HF-54,strtime);

	selfRotate(rota);
	glPopMatrix();
}

//Working but source code commented to remove dependency to sdl-image
int takeScreenshot(const char * filename){
	printf("CIGMA INFO @takeScreenshot : Could not take screenshot, Uncomment the source code in cigma.c at line %d to do so\n The code was commented to remove an SDL-image dependancy\n",__LINE__);
	/*
    GLint viewport[4];
    Uint32 rmask, gmask, bmask, amask;
    SDL_Surface * picture, * finalpicture;

    glGetIntegerv(GL_VIEWPORT, viewport);
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif
    picture = SDL_CreateRGBSurface(SDL_SWSURFACE,viewport[2],viewport[3], 32,rmask, gmask, bmask, amask);
    SDL_LockSurface(picture);
    glReadPixels(viewport[0],viewport[1],viewport[2],viewport[3],GL_RGBA,GL_UNSIGNED_BYTE,picture->pixels);
    SDL_UnlockSurface(picture);

    finalpicture = flipSurface(picture);
    if (SDL_SaveBMP(finalpicture, filename)){
        return -1;
    }
    SDL_FreeSurface(finalpicture);
    SDL_FreeSurface(picture);
    return 0;*/
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	TEXT
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

FontText* new_FontText(char* fontName){
	float fontsize = 64.;
	FontText* this = malloc(sizeof(FontText));
	this->h = fontsize;
	this->name = malloc(sizeof(char)*(strlen(fontName)+1));
	strcpy(this->name,fontName);
	FILE* file = fopen(fontName, "rb");
	if(file != NULL){
		fread(this->ttf_buffer, 1, 1<<20, file);
		stbtt_BakeFontBitmap(this->ttf_buffer,0, fontsize, this->temp_bitmap,512,512, 32,96, this->cdata);
		glGenTextures(1, &(this->ftex));
		glBindTexture(GL_TEXTURE_2D, this->ftex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, 512,512, 0, GL_ALPHA, GL_UNSIGNED_BYTE, this->temp_bitmap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}
	else{
		printf("CIGMA ERROR @new_FontText() : Font file \"%s\" not found\n",fontName);
		return NULL;
	}
	return this;
}
 
void freeFontText(FontText** this){
    if(*this != NULL){
        //TODO IMPLEMENTATION
        free(*this);
        (*this) = NULL;
    }
    else{
        printf("CIGMA ERROR @freeFontText() : Trying to free a NULL font\n");
    }
}

typedef int font_type;
#define FONT_TYPE_NULL 0
#define FONT_TYPE_BITMAP_CHARACTERS 1
#define FONT_TYPE_STROKE_CHARACTERS 2
#define FONT_TYPE_TRUETYPE 3

font_type getFontType (void* font){
	if(font == NULL){
		return FONT_TYPE_NULL;
	}
	if(
		font == GLUT_BITMAP_8_BY_13 || 
		font == GLUT_BITMAP_9_BY_15 || 
		font == GLUT_BITMAP_HELVETICA_10 || 
		font == GLUT_BITMAP_HELVETICA_12 ||
		font == GLUT_BITMAP_HELVETICA_18 ||
		font == GLUT_BITMAP_TIMES_ROMAN_10 ||
		font == GLUT_BITMAP_TIMES_ROMAN_24
	){
		return FONT_TYPE_BITMAP_CHARACTERS;
	}
	else{
		FontText* ft_font = (FontText*) font;
		if(ft_font->name == GLUT_STROKE_MONO_ROMAN || ft_font->name == GLUT_STROKE_ROMAN){
			return FONT_TYPE_STROKE_CHARACTERS;
		}
		else{
			return FONT_TYPE_TRUETYPE;
		}
	}
}

int getTextWidthTruetype (FontText* font,int h,char* str){
	if(font != NULL){
		float xpos = 0;
		float xt = 0;
		float yt = 0;
		while (*str) {
			if (*str >= 32 && *str < 128) {
				stbtt_aligned_quad q;
				stbtt_GetBakedQuad(font->cdata, 512,512, *str-32, &xt,&yt,&q,1);//1=opengl & d3d10+,0=d3d9
				xpos = q.x1;
			}
			++str;
		}
		return (int) (xpos*1. *h/font->h) ;
	}
	return 0;
}

int getTextWidthBitmap (void* font,char* str){
	int sum = 0;
	for(int i=0;i<strlen(str);i++){
		sum+= glutBitmapWidth(font,str[i]);
	}
	return sum;
}

int getTextWidthStroked (FontText* font,int h,char* str){
	return glutStrokeLength(font->name,(const unsigned char*)str)*h*1./120;
}

int getTextWidth (void* font,int h, char* format,...){
	if(format == NULL){
		printf("CIGMA ERROR @getTextWidth() : Trying to measure NULL text\n");
	}
	else{
		va_list arg;
		va_start (arg, format);
		char ret[1000];
		vsprintf (ret, format, arg);
		va_end (arg);

		switch(getFontType(font)){
			case FONT_TYPE_NULL:
				printf("CIGMA ERROR @getTextWidth() : Trying to measure \"%s\" with a NULL font\n",ret);
			break;

			case FONT_TYPE_BITMAP_CHARACTERS:
				return getTextWidthBitmap(font,ret);
			break;

			case FONT_TYPE_STROKE_CHARACTERS:
				return getTextWidthStroked(font,h,ret);
			break;

			case FONT_TYPE_TRUETYPE:
				return getTextWidthTruetype(font,h,ret);
			break;
		}
	}
	return 0;
}

int getTextHeight (void* font){
	switch(getFontType(font)){
		case FONT_TYPE_NULL:
			printf("CIGMA ERROR @getTextHeight() : Trying to measure a NULL font\n");
		break;

		case FONT_TYPE_BITMAP_CHARACTERS:
			if(font == GLUT_BITMAP_HELVETICA_10 || font== GLUT_BITMAP_TIMES_ROMAN_10){
				return 10;
			}
			if(font == GLUT_BITMAP_HELVETICA_12){
				return 12;
			}
			if(font == GLUT_BITMAP_8_BY_13){
				return 13;
			}
			if(font == GLUT_BITMAP_9_BY_15){
				return 15;
			}
			if(font == GLUT_BITMAP_HELVETICA_18){
				return 18;
			}
			if(font == GLUT_BITMAP_TIMES_ROMAN_24){
				return 24;
			}
		break;

		case FONT_TYPE_STROKE_CHARACTERS:
			return ((FontText*)font)->h;
		break;

		case FONT_TYPE_TRUETYPE:
			return ((FontText*)font)->h;
		break;
	}
	return 0;
}

//retourne la position x en pixel de chaque charactere de la chaine 
int* getCharPosition (FontText* font,int h,char* text,...){
	if(text == NULL || strlen(text) <= 0){
		printf("CIGMA ERROR @getCharPosition() : Trying to measure NULL text\n");
	}
	else{
		int* pos = malloc(sizeof(int)*strlen(text));

		va_list arg;
		va_start (arg, text);
		char ret[1000];
		vsprintf (ret, text, arg);
		va_end (arg);

		char* subtext = calloc(strlen(text)+1,sizeof(char));
		for(int i=0;i<strlen(text);i++){
			switch(getFontType(font)){
				case FONT_TYPE_BITMAP_CHARACTERS:
					pos[i] = getTextWidthBitmap(font,subtext);
				break;

				case FONT_TYPE_STROKE_CHARACTERS:
					pos[i] = getTextWidthStroked(font,h,subtext);
				break;

				case FONT_TYPE_TRUETYPE:
					pos[i] = getTextWidthTruetype(font,h,subtext);
				break;
			}
			subtext[i] = text[i];
		}
		free(subtext);
		subtext = NULL;
		return pos;
	}
	return NULL;
}

/*
POSSIBLE FONTS :
	GLUT_BITMAP_8_BY_13
	GLUT_BITMAP_9_BY_15
	GLUT_BITMAP_HELVETICA_10
	GLUT_BITMAP_HELVETICA_12
	GLUT_BITMAP_HELVETICA_18
	GLUT_BITMAP_TIMES_ROMAN_10
	GLUT_BITMAP_TIMES_ROMAN_24
 */
void bitmapText(void * font,int x,int y, char *format,...){
	va_list arg;
	va_start (arg, format);
	char ret[1000];
	vsprintf (ret, format, arg);
	va_end (arg);
	glRasterPos2i(x,y);

	for(int i=0;i < strlen(ret);i++){
		glutBitmapCharacter(font, ret[i] );
	}
}

void strokedText(char *text, float x, float y,int h,FontText* font){
	float scale = h*1./120;
	glPushMatrix();
	glTranslatef(x, y, 0);
	glScalef(scale,scale,scale);
	for(int i=0;i<strlen(text);i++){
		glutStrokeCharacter((void*)font->name, text[i]);
	}
	glPopMatrix();
}

void truetypeText(char* text,FontText* ft_font,int x,int y,int h){
	int text_halfwidth = getTextWidth(ft_font,h,text)/2;
	int text_halfheight = h/2;
	float scale = h*1./ft_font->h;

	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	int xcenter = x + text_halfwidth;
	int ycenter = y + text_halfheight;
	setPosition(xcenter,ycenter);
	glTranslatef(-text_halfwidth,-text_halfheight,0); //move to text
	glScalef(scale,scale,scale);
	glBindTexture(GL_TEXTURE_2D, ft_font->ftex);
	glBegin(GL_QUADS);
	float xt = 0;
	float yt = 0;
	while (*text) {
		if (*text >= 32 && *text < 128) {
			stbtt_aligned_quad q;
			stbtt_GetBakedQuad(ft_font->cdata, 512,512, *text-32, &xt,&yt,&q,1);//1=opengl & d3d10+,0=d3d9
			glTexCoord2f(q.s0,q.t1); glVertex2f(q.x0,q.y0);
			glTexCoord2f(q.s1,q.t1); glVertex2f(q.x1,q.y0);
			glTexCoord2f(q.s1,q.t0); glVertex2f(q.x1,q.y1);
			glTexCoord2f(q.s0,q.t0); glVertex2f(q.x0,q.y1);
			//printf("%c %f,%f,%f,%f\n",text[0],q.x0,q.y0,q.x1,q.y1);
		}
		++text;
	}
	glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

void text(void *castfont, int x, int y, int h, char* format,...){
    if(format == NULL){
        printf("CIGMA ERROR @writeText() : Trying to write NULL text\n");
    }
    else{
		va_list arg;
		va_start (arg, format);
		char ret[1000];
		vsprintf (ret, format, arg);
		va_end (arg);
		
		RGBA fill = getFill();
		setColorRGBA(getStroke());

		switch(getFontType(castfont)){
			case FONT_TYPE_NULL:
				printf("CIGMA ERROR @writeText() : Trying to write \"%s\" with a NULL font\n",format);
			break;
			
			case FONT_TYPE_BITMAP_CHARACTERS:
				bitmapText(castfont,x,y,ret);
			break;

			case FONT_TYPE_STROKE_CHARACTERS:
				strokedText(ret,x,y,h,(FontText*)castfont);
			break;

			case FONT_TYPE_TRUETYPE:
				truetypeText(ret,(FontText*)castfont,x,y,h);
			break;
		}

		setColorRGBA(fill);
    }
}

void* copyFont (void* font){
	switch(getFontType(font)){
		case FONT_TYPE_NULL:
			printf("CIGMA ERROR @copyFont() : Trying to copy a NULL font\n");
		break;
			
		case FONT_TYPE_BITMAP_CHARACTERS:
			return font;
		break;

		case FONT_TYPE_STROKE_CHARACTERS:
			return makeVectorialFont(((FontText*)font)->name,((FontText*)font)->h);
		break;

		case FONT_TYPE_TRUETYPE:
			return new_FontText(((FontText*)font)->name);
		break;
	}
	return NULL;
}

void fitTextToRectangle (void *font, int x, int y,int width,int height,char* format,...){
	va_list arg;
	va_start (arg, format);
	char ret[1000];
	vsprintf (ret, format, arg);
	va_end (arg);

	int size;
	for(size=1;getTextWidth(font,size,ret)< width;size++);
	if(size >= height){
		size = height - 1;
	}
	size--;
	int str_x = x + width/2 - getTextWidth(font,size,ret)/2;
	int str_y = y + height/2 - size/2;
	text(font,str_x,str_y,size,ret);
}


FontText* makeVectorialFont (void* font,int size){
	FontText* f = malloc(sizeof(FontText));
	f->name = (void*)GLUT_STROKE_ROMAN;
	f->h = size;
	return f;
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	SOUND
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void playSound(Sound* self){
	sfMusic_play(self->data);
}

void stopSound(Sound* self){
	sfMusic_stop(self->data);
}

void setVolumeSound(Sound* self,float volume){
	self->volume = volume;
	sfMusic_setVolume(self->data,volume);
}

void pauseSound (Sound* self){
	sfMusic_pause(self->data);
}

float getPositionSound (Sound* self){
	sfTime time = sfMusic_getPlayingOffset(self->data);
	return sfTime_asSeconds(time);
}

void setLoopSound (Sound* self,bool loop){
	sfMusic_setLoop(self->data,loop);
	self->loop = loop;
}

void setPositionSound (Sound* self,float position){
	sfTime time;
	time.microseconds = position*1000000;
	sfMusic_setPlayingOffset(self->data,time);
}

bool isNULLSound (Sound* self){
	return self->data == NULL;
}

Sound new_Sound (char* filename){
	Sound sound;
	sound.data = sfMusic_createFromFile(filename);
	sfTime time = sfMusic_getDuration(sound.data);
	sound.duration = sfTime_asSeconds(time);
	sound.volume = sfMusic_getVolume(sound.data);
	sound.loop = false;

	sound.play = playSound;
	sound.stop = stopSound;
	sound.setVolume = setVolumeSound;
	sound.pause = pauseSound;
	sound.getPosition = getPositionSound;
	sound.setLoop = setLoopSound;
	sound.setPosition = setPositionSound;
	sound.isNULL = isNULLSound;

	return sound;
}

void freeSound (Sound* sound){
	if(!sound->isNULL(sound)){
		sfMusic_destroy(sound->data);
		sound->data = NULL;
		sound->duration = 0;
		sound->volume = 0;
		sound->loop = 0;
	}
}


/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	IMAGE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

Image* new_Image (char* filename,bool antialiasing){
    GLuint glID;
    Image* ret;
	bool useMipMap = antialiasing;

    int x,y,n;
    unsigned char *data = stbi_load(filename, &x, &y, &n, 0);
    if (data == NULL){
        printf("ERROR @new_Image() : FILE %s COULD NOT BE LOADED\n",filename);
        return NULL;
	}
    ret = malloc(sizeof(Image));
    glGenTextures(1, &glID);
    glBindTexture(GL_TEXTURE_2D, glID);

    if (useMipMap){
        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, x,y, GL_RGBA,GL_UNSIGNED_BYTE,data);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    }
    else{
        glTexImage2D(GL_TEXTURE_2D, 0, 1, x,y, 0, GL_RGBA,GL_UNSIGNED_BYTE,data);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

	ret->width = x;
	ret->height = y;
    ret->static_height = y;
    ret->static_width = x;

    //free
    ret->zoom = 1;
	ret->donneesRGB = glID;
	ret->color = COLOR_RGBA_WHITE;
	return ret;
}

void image (Image* I,int x,int y){
	RGBA fill = getFill();
	fillRGBA(I->color);
	int halfwidth = I->static_width/2;
	int halfheight = I->static_height/2;
	int xcenter = x + halfwidth;
	int ycenter = y + halfheight;
	setPosition(xcenter,ycenter);

    glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, I->donneesRGB);
    glBegin(GL_QUADS);
    glTexCoord2d(0,1);  glVertex2i(-halfwidth,-halfheight);
    glTexCoord2d(0,0);  glVertex2i(-halfwidth, halfheight);
    glTexCoord2d(1,0);  glVertex2i( halfwidth, halfheight);
    glTexCoord2d(1,1);  glVertex2i( halfwidth,-halfheight);
    glEnd();
    glDisable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);

	resetPosition(xcenter,ycenter);
	fillRGBA(fill);
}

void imageCustom (Image* I,int x,int y,int width,int height){
	RGBA fill = getFill();
	fillRGBA(I->color);
	int halfwidth = width/2;
	int halfheight = height/2;
	int xcenter = x + halfwidth;
	int ycenter = y + halfheight;
	setPosition(xcenter,ycenter);

    glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, I->donneesRGB);
    glBegin(GL_QUADS);
    glTexCoord2d(0,1);  glVertex2i(-halfwidth,-halfheight);
    glTexCoord2d(0,0);  glVertex2i(-halfwidth, halfheight);
    glTexCoord2d(1,0);  glVertex2i( halfwidth, halfheight);
    glTexCoord2d(1,1);  glVertex2i( halfwidth,-halfheight);
    glEnd();
    glDisable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);

	resetPosition(xcenter,ycenter);
	fillRGBA(fill);
}

//fits the image in a rectangle without breaking the aspect ratio
void fitImageToRectangle (Image* img,int max_width,int max_height){
	float fact_x = max_width*1./img->static_width;
	float fact_y = max_height*1./img->static_height;
	float fact = fact_x < fact_y ? fact_x : fact_y;
	img->static_width *= fact;
	img->static_height *= fact;
}

void freeImage(Image** img){
	if(img != NULL){
		glDeleteTextures(1,(const GLuint*)&((*img)->donneesRGB));
		free(*img);
		*img = NULL;
	}
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	VIDEOS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/



void Video_play (Video* vid);
void Video_update (Video* vid);
void Video_show (Video* vid,int x,int y);
void Video_delete (Video* vid);

static void * lockfct(void *data, void **p_pixels);
static void unlockfct(void *data, void *id, void * const *p_pixels);

/**
 * @brief Creates a new video from a file or an URL using libVLC
 * @param path path to a video file or an URL
 * @param width width of the video
 * @param height height of the video
 * @return Video 
 */
Video createVideo (char* path,int width,int height,bool sound){
	Video vid;
	vid.path = malloc(sizeof(char)*(strlen(path)+1));
	vid.width = width;
	vid.height = height;
	vid.sdlStruct = malloc(sizeof(SdlVideoData));
	vid.state = 0;
	vid.play = Video_play;
	vid.update = Video_update;
	vid.show = Video_show;
	vid.delete = Video_delete;

	strcpy(vid.path,path);

	// Init Texture
	glGenTextures(1, &(vid.textureId));
	glBindTexture(GL_TEXTURE_2D, vid.textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Init SDL_surface
	vid.sdlStruct->sdlSurface = SDL_CreateRGBSurface(SDL_SWSURFACE, width, height, 16, 0xf800, 0x07e0, 0x001f, 0);
	vid.sdlStruct->sdlMutex = SDL_CreateMutex();

	// Init LibVLC + media player
	const char *vlc_argv[] = { 
		"--no-xlib", /* tell VLC to not use Xlib */
		"--no-audio", /* skip any audio track */
	};
	int vlc_argc = 1;
	if(!sound){
		vlc_argc= 2;
	}
	libvlc_instance_t * libVlcInstance = libvlc_new(vlc_argc, vlc_argv);
	libvlc_media_t * media;
	if(str_startsWith(path,"URL:")){
		media = libvlc_media_new_location(libVlcInstance, path+4);
	}
	else{
		if(fileExist(path))
			media = libvlc_media_new_path(libVlcInstance, path);
		else{
			printf("CIGMA ERROR @new_Video : video '%s' not found\n",path);
			vid.mediaPlayer = NULL;
			return vid;
		}
	}
	vid.mediaPlayer = libvlc_media_player_new_from_media(media);
	libvlc_media_release(media);
	libvlc_release(libVlcInstance);

	libvlc_video_set_callbacks(vid.mediaPlayer, lockfct, unlockfct, NULL, vid.sdlStruct);
	libvlc_video_set_format(vid.mediaPlayer, "RV16", width, height, width*2);

	return vid;
}

/**
 * @brief Creates a new video from a file or an URL using libVLC
 * @param path path to a video file or an URL
 * @param width width of the video
 * @param height height of the video
 * @return Video 
 */
Video new_Video (char* path,int width,int height){
	return createVideo(path,width,height,true);
}

/**
 * @brief Creates a new video from a file or an URL using libVLC
 * @param path path to a video file or an URL
 * @param width width of the video
 * @param height height of the video
 * @return Video 
 */
Video new_Video_NoSound (char* path,int width,int height){
	return createVideo(path,width,height,false);
}

/**
 * @brief starts the video
 * @param vid video to start
 */
void Video_play (Video* vid){
	libvlc_media_player_play(vid->mediaPlayer);
	vid->state = libvlc_media_player_get_state(vid->mediaPlayer);
}

/**
 * @brief updates a video
 * @param vid video to update
 */
void Video_update (Video* vid){
	vid->state = libvlc_media_player_get_state(vid->mediaPlayer);
}

/**
 * @brief displays the video
 * @param vid video to display
 * @param x x-position to display the video
 * @param y y-position to display the video
 */
void Video_show (Video* vid,int x,int y){
	glEnable(GL_TEXTURE_2D);
	void * rawData = (void *) malloc(vid->width * vid->height * 4);
	Uint8 * pixelSource;
	Uint8 * pixelDestination = (Uint8 *) rawData;
	Uint32 pix;

	for (unsigned int i = vid->height; i > 0; i--) {
		for (unsigned int j = 0; j < vid->width; j++) {
			pixelSource = (Uint8 *) vid->sdlStruct->sdlSurface->pixels + (i-1) * vid->sdlStruct->sdlSurface->pitch + j * 2;
			pix = *(Uint16 *) pixelSource;
			SDL_GetRGBA(pix, vid->sdlStruct->sdlSurface->format, &(pixelDestination[2]), &(pixelDestination[1]), &(pixelDestination[0]), &(pixelDestination[3]));
			pixelDestination += 4;
		}
	}

	// Building the texture
	glBindTexture(GL_TEXTURE_2D, vid->textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, vid->width, vid->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (Uint8 *) rawData);
	free(rawData);

	int halfwidth = vid->width/2;
	int halfheight = vid->height/2;
	setPosition(x+halfwidth,y+halfheight);
	glBegin(GL_QUADS);
		glTexCoord2d(0,0);  glVertex2i(-halfwidth,-halfheight);
		glTexCoord2d(0,1);  glVertex2i(-halfwidth, halfheight);
		glTexCoord2d(1,1);  glVertex2i( halfwidth, halfheight);
		glTexCoord2d(1,0);  glVertex2i( halfwidth,-halfheight);
	glEnd();
	resetPosition(x+halfwidth,y+halfheight);

	glDisable(GL_TEXTURE_2D);

}

/**
 * @brief frees the memory allocated to a video
 * @param vid video to free
 */
void Video_delete (Video* vid){
	libvlc_media_player_release(vid->mediaPlayer);
	SDL_DestroyMutex(vid->sdlStruct->sdlMutex);
	SDL_FreeSurface(vid->sdlStruct->sdlSurface);
}

/**
 * @brief lock callback for libvlc
 * @param data data from the SdlVideoData structure
 * @param p_pixels pixels of the next frame
 * @return void* 
 */
static void * lockfct(void *data, void **p_pixels) {
	SdlVideoData sdlStruct =*( (SdlVideoData*) data );
	SDL_LockMutex(sdlStruct.sdlMutex);
	SDL_LockSurface(sdlStruct.sdlSurface);
	*p_pixels = sdlStruct.sdlSurface->pixels;
	return NULL;
}

/**
 * @brief unlock callback for libvlc
 * @param data data from the SdlVideoData structure
 * @param id id of the video
 * @param p_pixels pixels of the next frame
 */
static void unlockfct(void *data, void *id, void * const *p_pixels) {
	SdlVideoData sdlStruct =*( (SdlVideoData*) data );
	SDL_UnlockSurface(sdlStruct.sdlSurface);
	SDL_UnlockMutex(sdlStruct.sdlMutex);
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	3D SHAPES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

void rectangle3D(float posx, float posy, float posz, float lx, float ly, float lz) {
	glPushMatrix();

	glTranslatef(posx, posy, posz);
	glScalef(1,ly/lx,lz/lx);
	glutSolidCube(lx);

	glPopMatrix();
}

void drawRepere(float unitaire) {
    //tracé du repere x,y,z de meme vecteur unitaire
    glColor3f(1, 0, 0);
    glBegin(GL_LINES);
    glVertex3f(0, 0, 0);
    glVertex3f(unitaire, 0, 0);
    glEnd();
    glColor3f(0, 1, 0);
    glBegin(GL_LINES);
    glVertex3f(0, 0, 0);
    glVertex3f(0, unitaire, 0);
    glEnd();
    glColor3f(0, 0, 1);
    glBegin(GL_LINES);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, unitaire);
    glEnd();
}
 
void drawGrid(int rayon, float step) {
    glPushMatrix();
    glTranslatef(0, -1, 0);
 
    glBegin(GL_LINES);
    for (GLfloat i = -rayon; i <= rayon; i += step) {
        glVertex3f(i, 0, rayon);
        glVertex3f(i, 0, -rayon);
        glVertex3f(rayon, 0, i);
        glVertex3f(-rayon, 0, i);
    }
    glEnd();
 
    glPopMatrix();
}
 
void drawSphere(float posx, float posy, float posz, float rayon, float prec, GLenum style) {
    glPushMatrix();
    glTranslatef(posx, posy, posz);
 
    GLUquadric* params = gluNewQuadric();
    gluQuadricTexture(params, GLU_TRUE);
    gluQuadricDrawStyle(params, style);
    gluSphere(params, rayon, prec, prec);
    gluDeleteQuadric(params);
   
    glPopMatrix();
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	OBJ 3D Models
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

GLuint loadObj(char *fname){
	GLuint ret;
    FILE *fp;
    int read;
    GLfloat x, y, z;
    char ch;
    ret = glGenLists(1);
    fp=fopen(fname,"r");
    if (!fp){
        printf("can't open file %s\n", fname);
        exit(1);
    }
    glPointSize(3.0);
    glNewList(ret, GL_COMPILE);{
        glPushMatrix();
        glBegin(GL_LINES);
        while(!(feof(fp))){
            read=fscanf(fp,"%c %f %f %f",&ch,&x,&y,&z);
            if(read==4&&ch=='v'){
                glVertex3f(x,y,z);
            }
        }
        glEnd();
    }
    glPopMatrix();
    glEndList();
    fclose(fp);
	return ret;
}

void drawModel(GLuint model,float size){
    glColor3f(1.0,0.23,0.27);
    glScalef(size,size,size);
    glCallList(model);
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	CAMERAS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/


void Camera2D_update(Camera2D* cam);
void Camera2D_apply (Camera2D* cam);
void Camera2D_reset (Camera2D* cam);

Camera2D new_GrabCamera2D (){
    Camera2D cam;
    cam.grabbed     = false;
    cam.x           = 0;
    cam.y           = 0;
    cam.zoom        = 1;
    cam.rotation_x  = 0;
    cam.rotation_y  = 0;

    cam.offsetx     = 0;
	cam.offsety     = 0;
	cam.oldAS       = 0;
	cam.oldOS       = 0;

    cam.update      = Camera2D_update;
    cam.reset       = Camera2D_reset;
    cam.apply       = Camera2D_apply;
    return cam;
}

void Camera2D_update(Camera2D* cam){
	if(cam->grabbed && (cam->oldAS != AS || cam->oldOS != OS)){
		cam->offsetx = AS - cam->oldAS;
		cam->offsety = OS - cam->oldOS;
		cam->oldAS = AS;
		cam->oldOS = OS;
		cam->x += cam->offsetx;
		cam->y += cam->offsety;
	}
	if(handleSouris && mouseState == MOUSE_LEFT_DOWN){
		cam->grabbed = true;
		cam->oldAS = AS;
		cam->oldOS = OS;
	}
	if(handleSouris && mouseState == MOUSE_LEFT_UP){
		cam->grabbed = false;
	}
}

void Camera2D_apply (Camera2D* cam){
    glTranslated(cam->x,cam->y,0);
    glScalef(cam->zoom,cam->zoom,0);
}

void Camera2D_reset (Camera2D* cam){
    glScalef(1./cam->zoom,1./cam->zoom,0);
    glTranslated(-cam->x,-cam->y,0);
}

void CameraFPS_update(CameraFPS* cam);
void CameraFPS_apply (CameraFPS* cam);
void CameraFPS_reset (CameraFPS* cam);

CameraFPS new_CameraFPS (){
    CameraFPS cam;
    cam.x           = 0;
    cam.y           = 0;
    cam.z           = 10;
    cam.zoom        = 2;
    cam.lx  = 0;
    cam.ly  = 0;
    cam.lz  = 10;
    cam.warping = false;
    cam.rotSpeed = 0.002;
    cam.speed = 0.1;
    cam.angle1 + -5000;
    cam.mouseCaptured = true;

    cam.offsetx     = 0;
	cam.offsety     = 0;
	cam.oldAS       = 0;
	cam.oldOS       = 0;

    cam.update      = CameraFPS_update;
    cam.reset       = CameraFPS_reset;
    cam.apply       = CameraFPS_apply;
    return cam;
}



void CameraFPS_update(CameraFPS* cam){

    //movement
    ifkeypressed('z'){
        cam->x += cam->lx * cam->speed;
        cam->y += cam->ly * cam->speed;
        cam->z += cam->lz * cam->speed;
    }
    ifkeypressed('s'){
        cam->x -= cam->lx * cam->speed;
        cam->y -= cam->ly * cam->speed;
        cam->z -= cam->lz * cam->speed;
    }
    ifkeypressed('q'){
        cam->x += cam->lz * cam->speed;
        cam->z -= cam->lx * cam->speed;
    }
    ifkeypressed('d'){
        cam->x -= cam->lz * cam->speed;
        cam->z += cam->lx * cam->speed;
    }

    ifkeypressed('a'){
        cam->mouseCaptured = !cam->mouseCaptured;
    }


    if(cam->warping){
        cam->warping = false;
        return;
    }
    int dx = AS - LF/2;
    int dy = -(OS-glutGet(GLUT_WINDOW_HEIGHT)) - HF/2;
    cam->angle = cam->angle+dx*cam->rotSpeed;
    cam->angle1 = cam->angle1-dy*cam->rotSpeed;
    cam->lx=sin(cam->angle1)*sin(cam->angle);
    cam->ly=cos(cam->angle1);
    cam->lz=-sin(cam->angle1)*cos(cam->angle);
    if(cam->mouseCaptured){
        cam->warping = true;
        glutWarpPointer(LF/2,HF/2);
    }
    
    ifscrollup(){
        cam->zoom*= 1.1;
    }
    ifscrolldown(){
        cam->zoom*= 0.9;
    }
}

void CameraFPS_apply (CameraFPS* cam){
    glLoadIdentity();
    gluLookAt(cam->x, cam->y, cam->z,cam->x+cam->lx, cam->y+cam->ly, cam->z+cam->lz,0,1,0);
}

void CameraFPS_reset (CameraFPS* cam){
}


void Camera3D_update(Camera3D* cam);
void Camera3D_apply (Camera3D* cam);
void Camera3D_reset (Camera3D* cam);

Camera3D new_GrabCamera3D (){
    Camera3D cam;
    cam.grabbed     = false;
    cam.x           = 0;
    cam.y           = 0;
    cam.z           = 0;
    cam.zoom        = 2;
    cam.rotation_x  = 0;
    cam.rotation_y  = 0;
    cam.rotation_z  = 0;

    cam.offsetx     = 0;
	cam.offsety     = 0;
	cam.oldAS       = 0;
	cam.oldOS       = 0;

    cam.update      = Camera3D_update;
    cam.reset       = Camera3D_reset;
    cam.apply       = Camera3D_apply;
    return cam;
}

void Camera3D_update(Camera3D* cam){
	if(cam->grabbed && (cam->oldAS != AS || cam->oldOS != OS)){
		cam->offsetx = AS - cam->oldAS;
		cam->offsety = OS - cam->oldOS;
		cam->oldAS = AS;
		cam->oldOS = OS;
		cam->x -= cam->offsetx;
		cam->y += cam->offsety;
	}
	if(handleSouris && mouseState == MOUSE_LEFT_DOWN){
		cam->grabbed = true;
		cam->oldAS = AS;
		cam->oldOS = OS;
	}
	if(handleSouris && mouseState == MOUSE_LEFT_UP){
		cam->grabbed = false;
	}
    ifscrollup(){
        cam->zoom*= 1.1;
    }
    ifscrolldown(){
        cam->zoom*= 0.9;
    }
}

void Camera3D_updateRotation(Camera3D* cam){
	if(cam->grabbed && (cam->oldAS != AS || cam->oldOS != OS)){
		cam->offsetx = AS - cam->oldAS;
		cam->offsety = OS - cam->oldOS;
		cam->oldAS = AS;
		cam->oldOS = OS;
        float factor = 0.5;
		cam->rotation_z -= cam->offsetx*factor;
		cam->rotation_x += cam->offsety*factor;
	}
	if(handleSouris && mouseState == MOUSE_LEFT_DOWN){
		cam->grabbed = true;
		cam->oldAS = AS;
		cam->oldOS = OS;
	}
	if(handleSouris && mouseState == MOUSE_LEFT_UP){
		cam->grabbed = false;
	}
}

void Camera3D_apply (Camera3D* cam){
    glTranslated(cam->x,cam->y,cam->z);
    glRotatef(cam->rotation_x,1,0,0);
    glRotatef(cam->rotation_y,0,1,0);
    glRotatef(cam->rotation_z,0,0,1);
    glScalef(cam->zoom,cam->zoom,0);
}

void Camera3D_reset (Camera3D* cam){
    glScalef(1./cam->zoom,1./cam->zoom,0);
    glRotatef(cam->rotation_z,0,0,1);
    glRotatef(cam->rotation_y,0,1,0);
    glRotatef(cam->rotation_x,1,0,0);
    glTranslated(-cam->x,-cam->y,0);
}

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
																	TOOLS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

// donnes ARGB en format long du logo par defaut de Cigma
#define CIGMA_ICON_32x32 \
	         0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 3009517565, 3479279871,          0,\
	         0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239,\
	         0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,  845255668, 3462502140, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4279846106,\
	         0,          0,          0,          0,          0,          0,          0,          0, 1566282236, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4280504286, 4278200013,\
	         0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4280899296, 4278200013, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 2741081851,          0,          0,          0,          0,          0,          0,          0,\
	         0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,  778148095,          0,          0,          0,          0, 4284586239, 4284586239, 2439092221,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,\
	         0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 1834783225,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,\
	         0, 2220790777, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239,  492935423,          0,          0,          0,          0,          0,          0,          0,          0,          0,\
	         0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,  610375935, 4284586239, 4284586239, 4284586239, 1231132927,          0,          0,          0,          0,          0,          0,          0,          0,\
	 406343570, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,   16777216,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 1063360767,          0,          0,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239,  509646588,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 2137102591, 2137102591, 2137102591, 2137102591, 1096915199,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 2791414015,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 1063360767, 2137102591, 2137102591, 2137102591, 2137102591, 2137102591,          0,          0,          0,          0,\
	4281425893, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 2137102591, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,\
	4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,\
	2130716365, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 1096914682,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,\
	         0, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,  610375935,          0,          0,          0,          0,\
	         0, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 1549899515,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,  962697212, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,\
	         0,          0, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 3137349068,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,  778146801,          0,          0,          0,          0,          0,\
	         0,          0, 2550146507, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278200013, 1107305671,          0,          0,          0,          0,          0,          0,          0,          0,          0, 2590087165, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,\
	         0,          0,          0, 4278200013, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278200013, 4278200013, 4278200013, 4278200013, 3187680715, 2130716365, 2130716365, 2130716365, 2130716365, 2130716365, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 2130716365, 2130716365, 2130716365, 2130716365, 2130716365, 2130716365, 2130716365,\
	         0,          0,          0,          0, 4278200013, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278924242, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4279780057, 4284586239, 4284586239, 4284586239, 4284586239, 4282940402, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0,          0,          0, 3137349068, 4278200013, 4280833505, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4281360358, 4279977691, 4283862009, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0,          0,          0,          0,          0, 4278200013, 4278200013, 4281360358, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 2164270537, 4278200013, 4278200013, 4279187153, 4281360358, 4280701405, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013

#define CIGMA_ICON_32x32_REVERSED \
			 0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 2164270537, 4278200013, 4278200013, 4279187153, 4281360358, 4280701405, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0,          0,          0,          0,          0, 4278200013, 4278200013, 4281360358, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0,          0,          0, 3137349068, 4278200013, 4280833505, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4281360358, 4279977691, 4283862009, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0,          0, 4278200013, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278924242, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4279780057, 4284586239, 4284586239, 4284586239, 4284586239, 4282940402, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0, 4278200013, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278200013, 4278200013, 4278200013, 4278200013, 3187680715, 2130716365, 2130716365, 2130716365, 2130716365, 2130716365, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 2130716365, 2130716365, 2130716365, 2130716365, 2130716365, 2130716365, 2130716365,\
	         0,          0, 2550146507, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278200013, 1107305671,          0,          0,          0,          0,          0,          0,          0,          0,          0, 2590087165, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,\
	         0,          0, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 3137349068,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,  778146801,          0,          0,          0,          0,          0,\
	         0, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 1549899515,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,  962697212, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,\
	         0, 4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,  610375935,          0,          0,          0,          0,\
	2130716365, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 1096914682,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,\
	4278200013, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,\
	4281425893, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 2137102591, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 2791414015,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 1063360767, 2137102591, 2137102591, 2137102591, 2137102591, 2137102591,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239,  509646588,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 2137102591, 2137102591, 2137102591, 2137102591, 1096915199,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,\
	4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,\
	 406343570, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,   16777216,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 1063360767,          0,          0,          0,          0,          0,          0,          0,\
	         0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,  610375935, 4284586239, 4284586239, 4284586239, 1231132927,          0,          0,          0,          0,          0,          0,          0,          0,\
	         0, 2220790777, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239,  492935423,          0,          0,          0,          0,          0,          0,          0,          0,          0,\
	         0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 1834783225,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,\
	         0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239,  778148095,          0,          0,          0,          0, 4284586239, 4284586239, 2439092221,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,\
	         0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 2741081851,          0,          0,          0,          0,          0,          0,          0,\
	         0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4280899296, 4278200013, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4278200013, 4278200013, 4278200013,\
	         0,          0,          0,          0,          0,          0,          0,          0, 1566282236, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4280504286, 4278200013,\
	         0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,  845255668, 3462502140, 4284586239, 4284586239, 4284586239, 4284586239, 4284586239, 4279846106,\
	         0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 4284586239, 4284586239, 4284586239, 4284586239,\
	         0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0,          0, 3009517565, 3479279871,          0


int** getPointsRoundedRectangle(int width,int height,float coef,int rnbpts){
	int** ret = malloc(sizeof(int*)*2);
	if(rnbpts > 150){
		rnbpts = 150;
	}
	if(rnbpts < 17){
		rnbpts = 17;
	}
	if(coef > height*1./width * 2){
		coef = height*1./width*2;
	}

	int x2 = width;
	int y2 = height;

	int x3 = width/2;
	int y3 = height/2;
	int nbpts = rnbpts/4;
	float scale = coef*x3*1./2;

	int* pts_x = malloc(sizeof(int)*rnbpts);
	int* pts_y = malloc(sizeof(int)*rnbpts);

	//Definition d'une forme a bords arrondis
	pts_x[0] = x3;
	pts_y[0] = y2;
	for(int i= 1; i< nbpts;i++){
		float x,y;
		x = (i-1)*1./(nbpts-3);
		y = sqrt(1 - x*x);
		pts_x[i] = x2-scale + x*scale;
		pts_y[i] = y2-scale + y*scale;
	}
	pts_x[nbpts-1] = x2;
	pts_y[nbpts-1] = y3;

	// Symetrie par l axe X
	int cpt = rnbpts/4 - 1;
	for(int i=rnbpts/4;i<rnbpts/2;i++){
		if(cpt < 0)
			cpt = 0;
		pts_x[i] = pts_x[cpt];
		pts_y[i] = 2*y3 - pts_y[cpt];
		cpt--;
	}

	//Symetrie par l'axe Y
	int cpt2 = rnbpts/2 - 1;
	for(int i=rnbpts/2;i<rnbpts;i++){
		pts_x[i] = 2*x3- pts_x[cpt2];
		pts_y[i] = pts_y[cpt2];
		cpt2--;
	}
	ret[0] = pts_x;
	ret[1] = pts_y;

	return ret;
}

double getSystemTime(void){
	struct timeval time;
	gettimeofday(&time, NULL);
	return (double)time.tv_sec+(double)time.tv_usec/1000000.;
}

unsigned long * getCigmaIcon32 (){
	unsigned long icon32[] = {32,32,CIGMA_ICON_32x32};
	unsigned long *ret = malloc(sizeof(unsigned long)*(2+32*32));
	for(int i=0;i<2+32*32;i++){
		ret[i] = icon32[i];
	}
	return ret;
}

unsigned long * getCigmaIcon32Reversed (){
	unsigned long icon2[] = {CIGMA_ICON_32x32_REVERSED};
	unsigned long *ret = malloc(sizeof(unsigned long)*(32*32));
	for(int i=0;i<32*32;i++){
		ret[i] = icon2[i];
	}
	return ret;
}

Image* textureFromLongRGBA(unsigned long* data,int width,int height){
	GLuint texture;
	unsigned int *imgdata = malloc(sizeof(unsigned int)*width*height);
	for(int i=0;i<width*height;i++){
		unsigned int a = ( data[i] & 0b11111111000000000000000000000000 ) >> 8*3;
		imgdata[i] = (data[i] << 8) | a;	//decalage du byte 'a'
	}

	glGenTextures( 1, &texture );
	glBindTexture( GL_TEXTURE_2D, texture );

	glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, imgdata);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

	Image* ret = malloc(sizeof(Image));
	ret->color = new_RGBA(255,255,255,255);
	ret->donneesRGB = texture;
	ret->height = height;
	ret->static_height = height;
	ret->static_width = width;
	ret->width = width;
	ret->zoom = 1;

	return ret;
}
